-- Entité représentant un jeu vide
-- Utilise une forme d'héritage simple: game qui inclus game
-- ****************************************

-- CE JEU EST VIDE ET EST NÉCESSAIRE AU MENU DE CHOIX DU JEU

--- Création d'un jeu
-- @return un pseudo objet game
function createGame ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createGame")"
  local lGame = {}

  lGame.fontSizeFactor = 1.0
  lGame.name = "DEFAULT"
  lGame.title = "DEFAULT"
  lGame.helpText = {"A quel jeu souhaitez-vous jouer ?"}
  
  return lGame
end -- createGame
