-- Entité représentant un donjon généré aléatoirement
-- ****************************************
-- affiche les portes dans la minimap
DUNGEON_DEBUG_SHOOWDOORS = true

-- valeurs par défaut
DUNGEON_ROOM_COUNT = 20
DUNGEON_COLS = 9
DUNGEON_ROWN = 6
DUNGEON_MINIMPAP_CELLWIDTH = 34
DUNGEON_MINIMPAP_CELLHEIGHT = 13
DUNGEON_MINIMPAP_SPACING = 5

--- Crée un pseudo objet de type donjon aléatoire
-- @return un pseudo objet dungeon
-- @param pRoomCount (OPTIONNEL) nombre de pièces du donjon.
-- @param pEntity (OPTIONNEL) entité à laquelle est liée l'objet.
-- @param pCols (OPTIONNEL) nombre de colonnes.
-- @param pRows (OPTIONNEL) nombre de lignes.
-- @return pseudo objet de type donjon
function createDungeon (pRoomCount, pEntity, pCols, pRows)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createDungeon ", pRoomCount, pEntity, pCols, pRows)

  local lDungeon = {}

  -- entité à laquelle est lié l'objet
  lDungeon.entity = pEntity
  -- nombre de colonnes
  lDungeon.cols = 0
  -- nombre de lignes
  lDungeon.rows = 0
  -- position X en pixel (minimap)
  lDungeon.xMiniMap = 0
  -- position Y en pixel (minimap)
  lDungeon.yMiniMap = 0
  -- largeur en pixel d'une cellule de la minimap
  lDungeon.cellWidthMiniMap = 0
  -- hauteur en pixel d'une cellule de la minimap
  lDungeon.cellHeightMiniMap = 0
  -- nombre de pièces
  lDungeon.roomCount = pRoomCount
  -- pièce de départ
  lDungeon.startRoom = nil
  -- pièce courante
  lDungeon.currentRoom = nil
  -- totalité du pièces donjon
  lDungeon.grid = {}
  --  pièces donjon non vide et utilisables
  lDungeon.rooms = {}

  --- Initialise l'objet
  -- @param pRoomCount (OPTIONNEL) nombre de pièces du donjon. Si absent, DUNGEON_ROOM_COUNT sera utilisé.
  -- @param pEntity (OPTIONNEL) entité à laquelle est liée l'objet.
  -- @param pCols (OPTIONNEL) nombre de colonnes. Si absent, DUNGEON_WIDTH sera utilisé.
  -- @param pRows (OPTIONNEL) nombre de lignes. Si absent, DUNGEON_HEIGHT sera utilisé.
  -- @param pXMiniMap (OPTIONNEL) position X en pixel de la minimap. Si absent, 0 sera utilisé.
  -- @param pYMiniMap (OPTIONNEL) position Y en pixel de la minimap. Si absent, 0 sera utilisé.
  -- @param pCellWidthMiniMap (OPTIONNEL) largeur en pixel d'une cellule de la minimap. Si absent, DUNGEON_CELLWIDTH sera utilisé.
  -- @param pCellHeightMiniMap (OPTIONNEL) hauteur en pixel d'une cellule de la minimap. Si absent, DUNGEON_CELLHEIGHT sera utilisé.
  function lDungeon.initialize (pRoomCount, pEntity, pCols, pRows, pXMiniMap, pYMiniMap, pCellWidthMiniMap, pCellHeightMiniMap)
    debugFunctionEnter("dungeon.initialize ",pRoomCount, pEntity, pCols, pRows, pXMiniMap, pYMiniMap, pCols, pRows, pCellWidthMiniMap, pCellHeightMiniMap)
    if (pRoomCount == nil) then pRoomCount = DUNGEON_ROOM_COUNT end
    if (pCols == nil) then pCols = DUNGEON_COLS end
    if (pRows == nil) then pRows = DUNGEON_ROWN end
    if (pXMiniMap == nil) then pXMiniMap = 0 end
    if (pYMiniMap == nil) then pYMiniMap = 0 end
    if (pCellWidthMiniMap == nil) then pCellWidthMiniMap = DUNGEON_MINIMPAP_CELLWIDTH end
    if (pCellHeightMiniMap == nil) then pCellHeightMiniMap = DUNGEON_MINIMPAP_CELLHEIGHT end

    lDungeon.entity = pEntity
    lDungeon.roomCount = pRoomCount
    lDungeon.xMiniMap = pXMiniMap
    lDungeon.yMiniMap = pYMiniMap
    lDungeon.cols = pCols
    lDungeon.rows = pRows
    lDungeon.cellWidthMiniMap = pCellWidthMiniMap
    lDungeon.cellHeightMiniMap = pCellHeightMiniMap

    -- initialise le donjon avec des salles fermées et vides
    local l, c
    for l = 1, lDungeon.rows do
      lDungeon.grid[l] = {}
      for c = 1, lDungeon.cols  do
        lDungeon.grid[l][c] = createRoom(l, c)
      end
    end

    -- initialisation de la salle de départ
    local startCol = math.random(1, lDungeon.cols)
    local startRow = math.random(1, lDungeon.rows)
    local startRoom = lDungeon.grid[startRow][startCol]
    startRoom.status = ROOM_STATUS_OPEN
    startRoom.row = startRow
    startRoom.col = startCol
    startRoom.isStart = true

    lDungeon.startRoom = startRoom

    -- liste des pièces du donjon
    lDungeon.rooms = {}
    startRoom.tableIndex = #lDungeon.rooms + 1
    lDungeon.rooms[startRoom.tableIndex] = startRoom

    -- génère le donjon
    -- *******************
    local selectedRoomNumber, selectedRoom
    local newRoom
    local newRoomDirection
    local newRoomRow, selectedRoomRow
    local roomFound = false
    local roomsCreatedCount = #lDungeon.rooms

    -- boucle sur le nombre de salles à créer
    while (roomsCreatedCount < lDungeon.roomCount) do
      roomFound = false
      -- sélectionne une salle dans la liste
      selectedRoomNumber = math.random(1, roomsCreatedCount)
      selectedRoom = lDungeon.rooms[selectedRoomNumber]
      selectedRoomRow = selectedRoom.row
      selectedRoomCol = selectedRoom.col
      -- on choisi une direction parmi les 4 possibles à partir de la salle sélectionnée
      newRoomDirection = math.random(1, 4)

      --debugMessage(newRoomDirection, selectedRoomRow, selectedRoomCol)

      -- on vérifie si la place est possible et si la place est libre
      if (newRoomDirection == 1 and selectedRoomRow > 1) then
        -- vers le haut
        newRoom = lDungeon.grid[selectedRoomRow - 1][selectedRoomCol]
        if (newRoom.status == ROOM_STATUS_CLOSE) then
          -- OK, la ligne au dessus existe et la salle n'est pas encore ouverte
          selectedRoom.doorStatus[LAST_EXT_UP] = DOOR_STATUS_OPEN
          newRoom.doorStatus[LAST_EXT_DOWN] = DOOR_STATUS_OPEN
          roomFound = true
        end
      elseif (newRoomDirection == 2 and selectedRoomCol < lDungeon.cols) then
        -- vers la droite
        newRoom = lDungeon.grid[selectedRoomRow][selectedRoomCol + 1]
        if (newRoom.status == ROOM_STATUS_CLOSE) then
          -- OK, la colonne à droite existe et la salle n'est pas encore ouverte
          selectedRoom.doorStatus[LAST_EXT_RIGHT] = DOOR_STATUS_OPEN
          newRoom.doorStatus[LAST_EXT_LEFT] = DOOR_STATUS_OPEN
          roomFound = true
        end
      elseif (newRoomDirection == 3 and selectedRoomRow < lDungeon.rows) then
        -- vers le bas
        newRoom = lDungeon.grid[selectedRoomRow + 1][selectedRoomCol]
        if (newRoom.status == ROOM_STATUS_CLOSE) then
          -- OK, la ligne au dessous existe et la salle n'est pas encore ouverte
          selectedRoom.doorStatus[LAST_EXT_DOWN] = DOOR_STATUS_OPEN
          newRoom.doorStatus[LAST_EXT_UP] = DOOR_STATUS_OPEN
          roomFound = true
        end
      elseif (newRoomDirection == 4 and selectedRoomCol > 1) then
        -- vers la gauche
        newRoom = lDungeon.grid[selectedRoomRow][selectedRoomCol - 1]
        if (newRoom.status == ROOM_STATUS_CLOSE) then
          -- OK, la colonne à gauche existe et la la salle n'est pas encore ouverte
          selectedRoom.doorStatus[LAST_EXT_LEFT] = DOOR_STATUS_OPEN
          newRoom.doorStatus[LAST_EXT_RIGHT] = DOOR_STATUS_OPEN
          roomFound = true
        end
      else
        -- aucune salle ne correspond, on reboucle pour tirer une autre salle de départ
        debugMessage("aucune salle ne correspond")
      end -- if (newRoomDirection == 1) then

      -- OK, on a trouvé une nouvelle salle, on l'ajoute à la liste
      if (roomFound) then
        newRoom.status = ROOM_STATUS_OPEN
        newRoom.tableIndex = roomsCreatedCount + 1
        lDungeon.rooms[newRoom.tableIndex] = newRoom
      end
      roomsCreatedCount = #lDungeon.rooms
    end -- while
  end -- dungeon.initialize

  -- dessine le donjon dans la minimap
  function lDungeon.drawMiniMap ()
    ---- debugFunctionEnterNL("dungeon.drawMiniMap") -- ATTENTION cet appel peut remplir le log
    local color, door
    local posX, posY
    local l, c
    for l = 1, lDungeon.rows do
      for c = 1, lDungeon.cols do
        -- dessine la salle
        local room = lDungeon.grid[l][c]
        posX = lDungeon.xMiniMap + (lDungeon.cellWidthMiniMap + DUNGEON_MINIMPAP_SPACING) * (c - 1)
        posY = lDungeon.yMiniMap + (lDungeon.cellHeightMiniMap + DUNGEON_MINIMPAP_SPACING) * (l - 1)
        room.drawMiniMap(posX, posY, lDungeon.cellWidthMiniMap, lDungeon.cellHeightMiniMap)
      end
    end
  end -- dungeon.draw

  --- Définit la salle courante (celle ou se trouve le joueur)
  -- @param pRoom Salle à définir.
  function lDungeon.setCurrentRoom (pRoom)
    -- debugFunctionEnter("gameMap.setCurrentRoom ", pRoom)
    local cRoom = lDungeon.currentRoom
    if (cRoom ~= nil) then
      cRoom.isCurrent = false
      cRoom.deleteDoors() -- important pour éviter de remplir la liste de sprites
    end
    pRoom.isCurrent = true

    -- on associe un pseudo objet "porte" à chaque porte existant dans la salle
    pRoom.createDoors()
    lDungeon.currentRoom = pRoom
  end -- gameMap.setCurrentRoom

  -- initialisation par défaut
  lDungeon.initialize(pRoomCount, pEntity, pCols, pRows)

  return lDungeon
 end -- createDungeon
