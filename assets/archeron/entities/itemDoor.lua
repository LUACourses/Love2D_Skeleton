-- Entité représentant une porte
-- Utilise une forme d'héritage simple: door qui inclus item qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "door" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_ITEM_EXIT_DOOR = SPRITE_TYPE_ITEM_EXIT..SEP_NEXT..SPRITE_TYPE_DETAIL -- une porte qui s'ouvre et qui se ferme (si tout a été collecté)
SPRITE_TYPE_ITEM_EXIT_DOOR_UP = SPRITE_TYPE_ITEM_EXIT_DOOR..LAST_EXT_UP -- en haut
SPRITE_TYPE_ITEM_EXIT_DOOR_RIGHT = SPRITE_TYPE_ITEM_EXIT_DOOR..LAST_EXT_RIGHT -- à droite
SPRITE_TYPE_ITEM_EXIT_DOOR_DOWN = SPRITE_TYPE_ITEM_EXIT_DOOR..LAST_EXT_DOWN-- en bas
SPRITE_TYPE_ITEM_EXIT_DOOR_LEFT = SPRITE_TYPE_ITEM_EXIT_DOOR..LAST_EXT_LEFT -- à gauche

-- nombre de pixel nécessaires pour la détection de collision
DOOR_COLLISON_OFFSET = 2 

--- Création d'un objet Door
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_ITEMS_IMAGES.."/door.png" sera utilisée.
-- @param pSubType (OPTIONNEL) sous-type de porte (voir les constantes SPRITE_TYPE_ITEM_EXIT_DOORXXX). Si absent, SPRITE_TYPE_ITEM_EXIT_DOOR_RIGHT sera utilisée.
-- @return un pseudo objet door
function createDoor (pX, pY, pImage, pSubType)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createDoor ", pX, pY, pImage, pSubType)
  if (pSubType == nil) then pSubType = SPRITE_TYPE_ITEM_EXIT_DOOR_RIGHT end
  if (pImage == nil) then pImage = FOLDER_ITEMS_IMAGES.."/door.png" end

  local lDoor = createItem(pSubType, pImage, pX, pY, nil, 1, 1)

  lDoor.points = 0
  lDoor.mustDestroyedWithFX = false
  lDoor.mustDestroyedWithSound = false
  lDoor.mustDestroyedOnCollision = false

  -- attributs spécifiques à une porte
  -- ******************************
  -- La porte est elle ouverte ?
  lDoor.isOpen = false
  -- rotation à appliquer pour l'affichage de la porte
  lDoor.rotation = 0

  --- Initialise l'objet
  function lDoor.initialize ()
    debugFunctionEnter("door.initialize ")

    -- la position de la porte dépend de son type
    local posX, posY, rotation
    if (lDoor.type == SPRITE_TYPE_ITEM_EXIT_DOOR_UP) then
      posX = (map.width - lDoor.width) / 2 -- centrée en X
      posY = map.tileheight * 2 - lDoor.height + DOOR_COLLISON_OFFSET-- sur la 2e ligne
      rotation = 0
    elseif (lDoor.type == SPRITE_TYPE_ITEM_EXIT_DOOR_RIGHT) then
      posX = map.width - map.tilewidth * 2 - DOOR_COLLISON_OFFSET -- avant dernière colonne
      posY = (map.height - lDoor.height) / 2 -- centrée en Y
      rotation = 90
    elseif (lDoor.type == SPRITE_TYPE_ITEM_EXIT_DOOR_DOWN) then
      posX = (map.width - lDoor.width) / 2 -- centrée en X
      posY = map.height - map.tileheight * 2  - DOOR_COLLISON_OFFSET-- avant dernière ligne
      rotation = 180
    elseif (lDoor.type == SPRITE_TYPE_ITEM_EXIT_DOOR_LEFT) then
      posX = map.tilewidth * 2 - lDoor.width + DOOR_COLLISON_OFFSET -- 2e colonne
      posY = (map.height - lDoor.height) / 2 -- centrée en Y
      rotation = 270
    end
    lDoor.x = posX
    lDoor.y = posY
    lDoor.rotation = rotation

    -- ajoute les animations
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lDoor.addAnimation(ANIM_NAME_STATUS_CLOSE, FOLDER_ITEMS_ANIMS.."/door_close", ".png")
    lDoor.addAnimation(ANIM_NAME_STATUS_OPEN, FOLDER_ITEMS_ANIMS.."/door_open", ".png")
    lDoor.playAnimation(ANIM_NAME_STATUS_CLOSE)
    lDoor.isOpen = false

    -- définit les hotSpots pour les collisions
    lDoor.createCollisionBox(COLLISION_MODEL_BOX4)

    lDoor.updateInitialValues() -- important
  end -- door.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --- affiche l'objet
  lDoor.itemDraw = lDoor.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lDoor.draw ()
  ---- debugFunctionEnterNL("door.draw") -- ATTENTION  cet appel peut remplir le log
  -- affiche l'objet comme un sprite normal
   lDoor.itemDraw()
  end -- door.draw

  --[[ à décommenter pour des comportements spécifiques
  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lDoor.itemDestroy = lDoor.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lDoor.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
  debugFunctionEnter("door.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

  -- la destruction effective se fait dans la classe parent
  lDoor.itemDestroy()
  end -- door.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lDoor.itemUpdate = lDoor.update
  function lDoor.update (pDt)
  ---- debugFunctionEnterNL("door.update ", pDT) -- ATTENTION cet appel peut remplir le log
  lDoor.itemUpdate(pDt)

  end -- door.update
  ]]

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lDoor.itemCollide = lDoor.collide
  function lDoor.collide ()
    ---- debugFunctionEnterNL("door.collide") -- ATTENTION cet appel peut remplir le log
    lDoor.itemCollide(pDt)

    if (not lDoor.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    local collisionType = SPRITE_COLLISION_NONE

    -- on vérifie si la porte ne collisionne pas avec le joueur
    if (lDoor.collideWithSprite(player) ~= SPRITE_COLLISION_NONE) then
      -- oui, elle collisionne avec le joueur
      collisionType = SPRITE_COLLISION_PLAYER
      if (lDoor.isOpen) then
        -- si elle est ouverte, on change de salle
        map.changeRoom(lDoor.type)
      end
    end -- if (lDoor.collideWithSprite(player) ~= SPRITE_COLLISION_NONE)
    return collisionType
  end -- door.collide

  -- ouvre la porte
  function lDoor.open ()
    lDoor.playAnimation(ANIM_NAME_STATUS_OPEN)
    lDoor.isOpen = true
  end

  -- ferme la porte
  function lDoor.close ()
    lDoor.playAnimation(ANIM_NAME_STATUS_CLOSE)
    lDoor.isOpen = false
  end

  -- initialisation par défaut
  lDoor.initialize()

  return lDoor
end -- createDoor
