-- Entité représentant un pnj en mode guidé
-- Utilise une forme d'héritage simple: npcGuided qui inclus npc qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "npcGuided" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_NPC_GUIDED = SPRITE_TYPE_NPC..NEXT_EXT_MOVEGUIDED -- pnj se déplaçant en suivant des directions positionnées dans la carte (par exemple un blob)
SPRITE_TYPE_NPC_GUIDED1 = SPRITE_TYPE_NPC_GUIDED..LAST_EXT_1 -- pnj guidé n°1, par défaut, marchant sur le sol avec changements de direction droite et gauche

--- Création d'un objet npcGuided
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids du npc. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_NPCS_IMAGES.."/npcGuided.png" sera utilisée.
-- @param pSubType (OPTIONNEL) sous-type de pnj (voir les constantes SPRITE_TYPE_NPC_GUIDEDXXX). Si absent, SPRITE_TYPE_NPC_GUIDED1 sera utilisée.
-- @return un pseudo objet npcGuided
function createNpcGuided (pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createNpcGuided ", pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
  if (pSubType == nil) then pSubType = SPRITE_TYPE_NPC_GUIDED1 end

  local detailIndex = getLastSuffix(pSubType)

  if (pImage == nil) then pImage = FOLDER_NPCS_IMAGES.."/"..SPRITE_TYPE_DETAIL..detailIndex..".png" end

  local lNpcGuided = createNPC(pSubType, pImage, pX, pY, pWeight, pLife, pEnergy)

  --- Initialise l'objet
  function lNpcGuided.initialize ()
    debugFunctionEnter("npcGuided.initialize")

    local detailIndex = getLastSuffix(lNpcGuided.type)

    local actionDelaiWithDtFactor = game.getDelaiDifficultyFactor()
    if (lNpcGuided.type == SPRITE_TYPE_NPC_GUIDED1) then
      lNpcGuided.speed = 75 * game.getSpeedDifficultyFactor()
      lNpcGuided.actionDelai = {
        min = 5 * actionDelaiWithDtFactor,
        max = 10 * actionDelaiWithDtFactor
      }
    else
      logMessage("lNpcGuided.initialize:le type '"..lNpcGuided.type .. " est invalide")
      return
    end

    lNpcGuided.canCollideWithTile = true
    lNpcGuided.navPath = createNavPath(NAVPATH_TYPE_RIGHT, lNpcGuided)

    lNpcGuided.addAnimation(ANIM_NAME_MOVE_WALK, FOLDER_NPCS_ANIMS.."/"..SPRITE_TYPE_DETAIL..detailIndex.."/walk", ".png")
    lNpcGuided.playAnimation(ANIM_NAME_MOVE_WALK)

    -- définit les hotSpots pour les collisions
    lNpcGuided.createCollisionBox(COLLISION_MODEL_BOX4)

    lNpcGuided.updateInitialValues() -- important
  end -- npcGuided.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lNpcGuided.npcDraw = lNpcGuided.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcGuided.draw ()
  ---- debugFunctionEnterNL("npcGuided.draw") -- ATTENTION  cet appel peut remplir le log
  -- affiche l'objet comme un sprite normal
   lNpcGuided.npcDraw()
  end -- npcGuided.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lNpcGuided.npcDestroy = lNpcGuided.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcGuided.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
  debugFunctionEnter("npcGuided.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

  -- la destruction effective se fait dans la classe parent
  lNpcGuided.npcDestroy()
  end -- npcGuided.destroy
  ]]

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcGuided.npcUpdate = lNpcGuided.update
  function lNpcGuided.update (pDt)
    ---- debugFunctionEnterNL("npcGuided.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lNpcGuided.npcUpdate(pDt)

    local mathAbs = math.abs -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    if (lNpcGuided.checkIfActive()) then
      -- mouvement spécifiques (autre que dans le CDD) pour les sprites qui se déplacent en suivant des directions
      if (lNpcGuided.direction == SPRITE_MOVE_RIGHT) then
        lNpcGuided.vX = mathAbs(lNpcGuided.initialValues.speed)
        lNpcGuided.vY = 0
      elseif (lNpcGuided.direction == SPRITE_MOVE_LEFT) then
        lNpcGuided.vX = -mathAbs(lNpcGuided.initialValues.speed)
        lNpcGuided.vY = 0
      end
      if (lNpcGuided.direction == SPRITE_MOVE_UP) then
        lNpcGuided.vX = 0
        lNpcGuided.vY = -mathAbs(lNpcGuided.initialValues.speed)
      elseif (lNpcGuided.direction == SPRITE_MOVE_DOWN) then
        lNpcGuided.vX = 0
        lNpcGuided.vY = mathAbs(lNpcGuided.initialValues.speed)
      end
    end

  end -- npcGuided.update

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcGuided.npcCollide = lNpcGuided.collide
  function lNpcGuided.collide ()
    ---- debugFunctionEnterNL("npcGuided.collide") -- ATTENTION cet appel peut remplir le log
    local collisionType = lNpcGuided.npcCollide(pDt)

    if (not lNpcGuided.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    if (lNpcGuided.checkIfCanCollideWithTiles()) then
      -- Tuile sous le sprite
      local idOverlap = map.getTileAt(lNpcGuided.x + map.tilewidth / 2, lNpcGuided.y + map.tileheight - 1)
      local tileType = nil
      if (map.tiles ~= nil and map.tiles[idOverlap] ~= nil) then tileType = map.tiles[idOverlap].type else tileType = TILE_SPECIAL_EMPTY end

      -- MOUVEMENT POUR LES SPRITE DE TYPE "GUIDED"
      -- ******************************
      if (tileType == TILE_TYPE_GO_RIGHT or (lNpcGuided.direction == SPRITE_MOVE_LEFT and lNpcGuided.collideWithTileLeft() ~= SPRITE_COLLISION_NONE)) then
        -- tuiles pour aller vers la droite ou collision à gauche avec une tuile solide
        lNpcGuided.direction = SPRITE_MOVE_RIGHT
        lNpcGuided.currentAnimation.flipedToRight = false
        lNpcGuided.x = lNpcGuided.x + OFFSET_WHEN_BOUNCE
        collisionType = SPRITE_COLLISION_TILE_SOLID_LEFT
      elseif (tileType == TILE_TYPE_GO_LEFT or (lNpcGuided.direction == SPRITE_MOVE_RIGHT and lNpcGuided.collideWithTileRight() ~= SPRITE_COLLISION_NONE)) then
        -- tuiles pour aller vers la gauche ou collision à droite avec une tuile solide
        lNpcGuided.direction = SPRITE_MOVE_LEFT
        lNpcGuided.currentAnimation.flipedToRight = true
        lNpcGuided.x = lNpcGuided.x - OFFSET_WHEN_BOUNCE
        collisionType = SPRITE_COLLISION_TILE_SOLID_RIGHT
      elseif (tileType == TILE_TYPE_GO_DOWN or (lNpcGuided.direction == SPRITE_MOVE_UP and lNpcGuided.collideWithTileTop() ~= SPRITE_COLLISION_NONE)) then
        -- tuiles pour aller vers le bas ou collision en haut avec une tuile solide
        lNpcGuided.direction = SPRITE_MOVE_DOWN
        lNpcGuided.currentAnimation.flipedToRight = true
        lNpcGuided.y = lNpcGuided.y + OFFSET_WHEN_BOUNCE
        collisionType = SPRITE_COLLISION_TILE_SOLID_TOP
      elseif (tileType == TILE_TYPE_GO_UP or (lNpcGuided.direction == SPRITE_MOVE_DOWN and lNpcGuided.collideWithTileBottom() ~= SPRITE_COLLISION_NONE)) then
        -- tuiles pour aller vers le haut ou collision en bas avec une tuile solide
        lNpcGuided.direction = SPRITE_MOVE_UP
        lNpcGuided.currentAnimation.flipedToRight = false
        lNpcGuided.y = lNpcGuided.y - OFFSET_WHEN_BOUNCE
        collisionType = SPRITE_COLLISION_TILE_SOLID_BOTTOM
      end
    end -- if (lNPC.checkIfCanCollideWithTiles()) then

    return collisionType
  end -- npcGuided.collide

  --- le PNJ effectue une action (tire un projectile)
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcGuided.npcAction = lNpcGuided.action
  function lNpcGuided.action ()
    -- debugFunctionEnter("npc.action")
    lNpcGuided.npcAction()

    if (not lNpcGuided.checkIfCanMakeAction()) then return end
    local lProjectile
    -- COMPORTEMENT SPÉCIFIQUE: tire vers le bas
    -- ******************************
    -- rappel:      createProjectile(pType               , pImage                                            ,pX,pY,pLife,pEnergy,pDamages,pSpeed)
    lProjectile = createProjectile(SPRITE_TYPE_PROJ_NPC, FOLDER_PROJECTILES_IMAGES.."/projectile_npc_1.png", 0, 0, 1, 1, 1, 200)
    if (lNpcGuided.direction == SPRITE_MOVE_RIGHT) then
      lProjectile.navPath = createNavPath(NAVPATH_TYPE_RIGHT, lProjectile, lNpcGuided)
    else
      lProjectile.navPath = createNavPath(NAVPATH_TYPE_LEFT, lProjectile, lNpcGuided)
    end
    lProjectile.timeToLive = 5
    lProjectile.gravity = 2
    lProjectile.weight = 10
    lProjectile.mustDestroyedOnCollision = true
    lProjectile.canCollideWithTile = true
    lProjectile.mustDestroyedWithFX = false
    lProjectile.mustDestroyedWithSound = false
  end

  -- initialisation par défaut
  lNpcGuided.initialize()

  return lNpcGuided
end -- createNpcGuided
