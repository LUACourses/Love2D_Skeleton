-- Entité représentant une salle dans un donjon
-- ****************************************

-- différents états
ROOM_STATUS = "roomStatus"
ROOM_STATUS_OPEN = ROOM_STATUS..SEP_LAST.."Open"
ROOM_STATUS_CLOSE = ROOM_STATUS..SEP_LAST.."Close"
ROOM_STATUS_START= ROOM_STATUS..SEP_LAST.."Start"
ROOM_STATUS_CURRENT = ROOM_STATUS..SEP_LAST.."Current"

DOOR_STATUS_CLOSE = "dungeonDoorClose"
DOOR_STATUS_OPEN = "dungeonDoorOpen"

-- couleurs pour l'affichage des salles dans la minimap
ROOM_COLORS = {}
ROOM_COLORS[ROOM_STATUS_OPEN] = {255, 255, 255, 255}
ROOM_COLORS[ROOM_STATUS_CLOSE] = {128, 128, 128, 255}
ROOM_COLORS[ROOM_STATUS_START] = {128, 128, 255, 255}
ROOM_COLORS[ROOM_STATUS_CURRENT] = {128, 255, 128, 255}

--- Crée un pseudo objet de type pièce dans un donjon
-- @param pRow Ligne associée à cette salle.
-- @param pCol Colonne associée à cette salle.
-- @return un pseudo objet room
function createRoom (pRow, pCol)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createRoom ", pRow, pCol)
  local lRoom = {}
  -- Status
  lRoom.status = ROOM_STATUS_CLOSE
  -- Ligne (du donjon) associée à cette salle.
  lRoom.row = pRow
  -- Colonne (du donjon) associée à cette salle
  lRoom.col = pCol
  -- est-ce la salle de départ ?
  lRoom.isStart = false
  -- est-ce la salle courante ?
  lRoom.isCurrent = false
  -- état des 4 portes de la salle
  lRoom.doorStatus = {}
  -- les 4 portes de la salle. Important: cette liste est vide tant que la salle n'est pas la salle courante
  lRoom.doors = {}

  --- Initialise l'objet
  function lRoom.initialize ()
    debugFunctionEnter("room.initialize ")
    --lRoom.isStart = false -- important: ne pas modifier car cette info doit perdurer même si la porte est réinitialisée
    lRoom.isCurrent = false
    lRoom.doors = {}
    -- toutes les portes sont fermée au départ
    lRoom.doorStatus[LAST_EXT_UP] = DOOR_STATUS_CLOSE
    lRoom.doorStatus[LAST_EXT_RIGHT] = DOOR_STATUS_CLOSE
    lRoom.doorStatus[LAST_EXT_DOWN] = DOOR_STATUS_CLOSE
    lRoom.doorStatus[LAST_EXT_LEFT] = DOOR_STATUS_CLOSE
  end -- room.initialize

    -- dessine la salle (courante)
  function lRoom.draw ()
    ---- debugFunctionEnterNL("room.draw")

    -- dessine chaque porte
    for _, door in pairs(lRoom.doors) do
      door.draw()
    end -- for
  end

  -- dessine la salle dans la minimap
  function lRoom.drawMiniMap (pX, pY, pDrawWidth, pDrawHeight)
    ---- debugFunctionEnterNL("room.drawMiniMap ", pX, pY, pDrawWidth, pDrawHeight)-- ATTENTION cet appel peut remplir le log
    if (lRoom.isStart) then
      color = ROOM_COLORS[ROOM_STATUS_START]
    elseif (lRoom.isCurrent) then
      color = ROOM_COLORS[ROOM_STATUS_CURRENT]
    else
      color = ROOM_COLORS[lRoom.status]
    end

    love.graphics.setColor(color)
    love.graphics.rectangle ("fill", pX, pY, pDrawWidth, pDrawHeight)

    -- dessine les portes
    if (DUNGEON_DEBUG_SHOOWDOORS) then
      love.graphics.setColor({255, 0, 0, 255})
      if (lRoom.doorStatus[LAST_EXT_UP] == DOOR_STATUS_OPEN) then love.graphics.rectangle ("fill", pX + pDrawWidth / 2 - 2, pY - 2, 4, 6) end
      if (lRoom.doorStatus[LAST_EXT_RIGHT] == DOOR_STATUS_OPEN) then love.graphics.rectangle ("fill", pX + pDrawWidth - 2, pY + pDrawHeight / 2 - 2, 6, 4) end
      if (lRoom.doorStatus[LAST_EXT_DOWN] == DOOR_STATUS_OPEN) then love.graphics.rectangle ("fill", pX + pDrawWidth / 2 - 2, pY + pDrawHeight - 2, 4, 6) end
      if (lRoom.doorStatus[LAST_EXT_LEFT] == DOOR_STATUS_OPEN) then love.graphics.rectangle ("fill", pX - 2, pY + pDrawHeight / 2 - 2, 6, 4) end
    end
    love.graphics.setColor({255, 255, 255, 255})
  end

  -- crée les portes de la salle (courante)
  function lRoom.createDoors ()
    debugFunctionEnterNL("room.createDoors")

    lRoom.deleteDoors() -- important pour éviter de remplir la liste de sprites

    -- boucle sur les status et créée les portes associées
    local key, door
    for key, doorStatus in pairs(lRoom.doorStatus) do
      -- rappel: function createDoor (pX, pY, pImage, pSubType)
      door = createDoor(nil, nil, nil, SPRITE_TYPE_ITEM_EXIT_DOOR..key)
      if (doorStatus == DOOR_STATUS_OPEN) then door.open() else door.close() end
      lRoom.doors[#lRoom.doors + 1] = door
    end -- for
  end -- room.createDoors

  -- efface les portes de la salle (courante)
  function lRoom.deleteDoors ()
    debugFunctionEnterNL("room.deleteDoors")
    local i
    for i = 1, #lRoom.doors do
      lRoom.doors[i].destroy(false, false, false)
    end -- for
  end -- room.deleteDoors

  -- initialisation par défaut
  lRoom.initialize()

  return lRoom
end -- createRoom