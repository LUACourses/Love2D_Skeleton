-- fichier décrivant les éléments de base associés à un jeu
-- NOTE: TOUS LES FICHIERS MANIFEST SONT AUTOMATIQUEMENT INCLUS

-- tableau contenant le nom associé à chaque jeu présent dans le framework, indexé par l'ordre alphabétique des dossiers des jeux dans le dossier assets. 
GAME_NAMES[#GAME_NAMES + 1] = "archeron"
-- tableau contenant le manifeste associé à chaque jeu présent dans le framework, indexé par NOM DE JEU
GAME_MANIFESTS[GAME_NAMES[#GAME_NAMES]] = {  
  -- nom du jeu
  name = GAME_NAMES[#GAME_NAMES],
  -- titre du jeu
  title = "Rise of Archeron",
  -- dossier contenant les fichiers spécifiques à chaque jeu
  folder = GAME_NAMES[#GAME_NAMES],
  -- TYPE DE JEU
  -- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
  gametype = GAME_TYPE_SIDE..GAME_TYPE_PLATEFORMER..SEP_LAST..GAME_NAMES[#GAME_NAMES]
}