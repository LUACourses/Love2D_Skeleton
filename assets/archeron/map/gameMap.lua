-- Fonctions pour la map propre du type SIDE_PLATEFORMER
-- Utilise une forme d'héritage simpSle: gameMap qui inclus map
-- ****************************************

-- taille par défaut des tuiles de la map
MAP_TILE_WIDTH = 64
MAP_TILE_HEIGHT = 64

--- Crée un pseudo objet de type gameMap
-- @param pLevel (OPTIONNEL) niveau de jeu associé à la map.
-- @return un pseudo objet gameMap
function createGameMap (pLevel)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createGameMap ", pLevel)

  local lGameMap = createMap (pLevel)

  -- Override des fonctions de la map
  -- ******************************

  -- Charge et initialise la map
  -- @param pLevel (OPTIONNEL) niveau de jeu associé à la map. Si absent, 1 sera utilisé.
  function lGameMap.initialize (pLevel)
    debugFunctionEnter("gameMap.initialize ", pLevel)
    if (pLevel == nil) then pLevel = 1 end
    lGameMap.mapIsOK = false

    -- numéro du niveau
    lGameMap.level = pLevel
    lGameMap.playerStart = {} -- permet de centrer le joueur au départ

    -- attributs spécifiques à cette map
    -- ******************************

    -- objets disponibles dans le niveau
    lGameMap.items = {}
    -- nombre d'éléments à collecter dans le niveau (utile pour ouvrir la porte)
    lGameMap.collectableCount = {0}
    -- le donjon
    -- rappel:        createDungeon (pRoomCount, pEntity, pCols, pRows)
    lGameMap.dungeon = createDungeon()
    -- rappel: lDungeon.initialize (pRoomCount, pEntity , pCols, pRows, pXMiniMap, pYMiniMap, pCellWidthMiniMap, pCellHeightMiniMap)
    lGameMap.dungeon.initialize    (20        , lGameMap, 9    , 6    , 5        , 5        , 25               , 15)

    -- charge les fichiers liés au niveau de la map
    if (love.filesystem.exists (FOLDER_MAP_LEVELS)) then

      -- si on a déjà chargé un niveau, efface toutes les entités
      resetEntities()

      -- CHARGE les images des tuiles
      if (love.filesystem.exists (FOLDER_MAP_IMAGES_TILES)) then
        lGameMap.tiles["0"] = {image = "", type = TILE_SPECIAL_EMPTY}
        lGameMap.tiles["P"] = {image = "", type = TILE_SPECIAL_PLAYER_START}
        lGameMap.tiles["1"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile1.png"), type = TILE_TYPE_SOLID}
        if (lGameMap.tiles["1"].image ~= nil) then
          lGameMap.tilewidth, lGameMap.tileheight = lGameMap.tiles["1"].image:getDimensions()
          lGameMap.type = MAP_TYPE_TILED
          lGameMap.mapIsOK = true
        else
          logMessage("gameMap.initialize:le fichier '"..FOLDER_MAP_IMAGES_TILES.."/tile1.png' n'a pas été trouvé")
        end
      else
        logMessage("gameMap.initialize:le dossier '"..FOLDER_MAP_IMAGES_TILES.."' contenant les images des tuiles n'a pas été trouvé")
      end
    else
      logMessage("gameMap.initialize:le dossier '"..FOLDER_MAP_LEVELS.."' contenant la définitions des niveaux n'a pas été trouvé")
    end -- if (love.filesystem.exists (FOLDER_MAP_LEVELS)) then

    assertEqualQuit(lGameMap.grid, nil, "lGameMap.draw:lGameMap.grid", true)
    assertEqualQuit(lGameMap.tiles, nil, "lGameMap.draw:lGameMap.tiles", true)

    return lGameMap.mapIsOK
  end -- gameMap.initialize

  --- Crée et positionne les contenus (objets, PNJs...) dans la map
  -- @param pDt Delta time
  --[[ à décommenter pour des comportements spécifiques
  function lGameMap.update (pDt)
  ---- debugFunctionEnterNL("gameMap.update ", pDt) -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  end
  ]]

  --- affiche la map
  function lGameMap.draw ()
    ---- debugFunctionEnterNL("gameMap.draw") -- ATTENTION cet appel peut remplir le log
    lGameMap.mapIsOK = true

    local l, c, char, drawX, drawY
    local stringSub = string.sub -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe
    for l = 1, #lGameMap.grid do
      for c = 1, #lGameMap.grid[l] do
        char = stringSub(lGameMap.grid[l], c, c)
        if (char ~= nil) then
          if (lGameMap.tiles ~= nil and lGameMap.tiles[char] ~= nil and not lGameMap.isInvisible(char) and lGameMap.tiles[char].image ~= "") then
            drawX, drawY = lGameMap.mapToPixel(c, l)
            HUD.drawImage(lGameMap.tiles[char].image, drawX, drawY)
          end
        end
      end
    end

    local dungeon = lGameMap.dungeon
    if (dungeon ~= nil) then
      -- dessine la minimap
      dungeon.drawMiniMap()

      -- dessine la salle ou se trouve le joueur
      local cRoom = dungeon.currentRoom
      if (cRoom ~= nil) then cRoom.draw() end

    end

    return lGameMap.mapIsOK
  end -- gameMap.draw

  --- Crée et positionne les contenus (objets, PNJs...) dans la map pour un niveau (de difficulté) donné
  -- @param pLevel (OPTIONNEL) niveau (de difficulté). Si absent, 1 sera utilisée.
  function lGameMap.createLevelContent (pLevel)
    debugFunctionEnter("gameMap.createLevelContent ", pLevel)
    lGameMap.mapIsOK = true

    -- on définit la salle courante comme étant celle de départ
    lGameMap.dungeon.setCurrentRoom(lGameMap.dungeon.startRoom)

    -- ANALYSE la map pour placer les éléments
    -- ******************************
    if (lGameMap.type == MAP_TYPE_TILED) then
      local filename, line
      filename = FOLDER_MAP_LEVELS.."/level"..tostring(lGameMap.level) .. ".txt"

      lGameMap.grid = resourceManager.getTextFile(filename)

      if (lGameMap.grid ~= nil) then
        -- Analyse de la map
        local l, c, char, objet, key, posX, posY
        for l = 1, #lGameMap.grid do
          for c = 1, #lGameMap.grid[1] do
            key = l.."##"..c
            char = string.sub(lGameMap.grid[l], c, c)
            posX, posY = lGameMap.mapToPixel(c, l)
            if (char == TILE_SPECIAL_PLAYER_START) then
              -- point de départ du joueur
              lGameMap.playerStart.col = c
              lGameMap.playerStart.line = l
            elseif (char == TILE_SPECIAL_COIN) then
              --[[ TODO: à remplacer par collectable du donjon
              -- ajout d'une piece
              object = createCoin(posX, posY)
              lGameMap.items[key] = {type = char, content = object, col = c, line = l}
              lGameMap.addCollectableCount(1, 1)
              ]]
            elseif (char == TILE_SPECIAL_DOOR) then
              --[[ TODO: à remplacer par une sortie du donjon
              -- ajout d'une porte
              object = createDoor(posX, posY)
              lGameMap.items[key] = {type = char, content = object, col = c, line = l}
              ]]
            elseif (char == TILE_SPECIAL_BLOB) then
              --[[ TODO: à remplacer par une PNJ du donjon
              object = createNpcGuided(posX, posY, 1, 1, 1)
              object.mustDisplayEnergy = false
              ]]
            end
          end
        end
      else
        logMessage("gameMap.initialize:le fichier "..filename.." n'a pas été trouvé")
        -- plus de niveaux à charger, la partie est gagnée
        winGame()
        lGameMap.mapIsOK = false
      end -- if (love.filesystem.exists(filename)) then
    end -- if (lGameMap.type == MAP_TYPE_TILED) then

    return lGameMap.mapIsOK
  end -- gameMap.createLevelContent

  -- Fonctions spécifiques à cette map
  -- ******************************

  -- Retourne une info sur la map, affichée dans l'écran de jeu
  -- @param pParam (OPTIONNEL) paramètre générique.
  -- @return le contenu à afficher
  function lGameMap.getHudInfo (pParam)
    -- debugFunctionEnter("gameMap.getHudInfo ", pParam)
    return ""
  end

  -- Retourne des infos de debug info sur la map, affichée dans la zone de debug
  -- @return le contenu à afficher
  function lGameMap.getDebugInfo ()
    -- debugFunctionEnter("map.getDebugInfo ")
    -- par défaut, on ne retourne rien
    local content, room
    room = lGameMap.dungeon.currentRoom
    if (room == nil) then
      content = "non définie"
    else
      content = room.col..","..room.row..":"..room.status
    end
    content = "ROOM:" .. content
    return content
  end

  --- Ajoute une valeur au compteur des collectibles
  -- @param pLevel Index des collectable concernés
  -- @param pValue Valeur à ajouter
  function lGameMap.addCollectableCount (pLevel, pValue)
    -- debugFunctionEnter("gameMap.addCollectableCount ", pLevel, pValue)
    if (lGameMap.collectableCount == nil or lGameMap.collectableCount[pLevel] == nil) then return end
    lGameMap.collectableCount[pLevel] = lGameMap.collectableCount[pLevel] + pValue
  end -- lGameMap.addCollectableCount

  --- Change de salle
  -- @param pDoorType type de porte qui a été franchie.
  function lGameMap.changeRoom (pDoorType)
    debugFunctionEnter("gameMap.changeRoom ", pDoorType)
    debugMessage("CHANGEROOM ", pDoorType)
    local dungeon = lGameMap.dungeon
    local newRoom, room, col, row
    local spawnX = player.x
    local spawnY = player.y
    room = dungeon.currentRoom
    col = room.col
    row = room.row
    -- par quelle porte est on arrivé dans la salle ?
    -- on détermine la coordonnées de la nouvelle salle et la position du joueur
    if (pDoorType == SPRITE_TYPE_ITEM_EXIT_DOOR_UP) then
      -- salle du dessus
      row = row - 1
      -- le joueur apparait avec le même X et un Y sur l'avant dernière ligne
      spawnY = map.height - map.tileheight * 2 - player.height - DOOR_COLLISON_OFFSET * 2
    elseif (pDoorType == SPRITE_TYPE_ITEM_EXIT_DOOR_RIGHT) then
      -- salle de droite
      col = col + 1
      -- le joueur apparait avec un X sur la 2e colonne et avec le même Y 
      spawnX = map.tilewidth * 2 + DOOR_COLLISON_OFFSET * 2
    elseif (pDoorType == SPRITE_TYPE_ITEM_EXIT_DOOR_DOWN) then
      -- salle du dessous
      row = row + 1
      -- le joueur apparait avec le même X et un Y sur la 2e ligne
      spawnY = map.tileheight * 2 + DOOR_COLLISON_OFFSET * 2 
    elseif (pDoorType == SPRITE_TYPE_ITEM_EXIT_DOOR_LEFT) then
      -- salle de gauche
      col = col - 1
      -- le joueur apparait avec un X sur sur l'avant dernière colonne et avec le même Y 
      spawnX = map.width - map.tilewidth * 2 - player.width - DOOR_COLLISON_OFFSET * 2 
    end

    -- on récupère la nouvelle salle 
    newRoom = dungeon.grid[row][col]
    if (newRoom ~= nil) then
      dungeon.setCurrentRoom (newRoom)
      player.spawn(spawnX, spawnY)
      -- TODO: déplacer le joueur en face de la porte opposée
    end
  end

  -- initialisation par défaut
  lGameMap.initialize(pLevel)

  return lGameMap
end -- createGameMap
