- Ce dossier contient les tous les éléments liés à la carte et au niveaux du jeu (map).

- Il contient un sous-dossier images contenant les fichiers utilisés pour les tuiles (tiles) ou les ensembles de tuiles (tileset).

- Il contient un sous-dossier levels contenant la description de chaque niveau, par exemple sous la forme de fichier texte level<numero>.txt ou bien d'un fichier level<numero>.lua exporté depuis l'application tiled

- Il contient un fichier gameMap.lua, permettant de créer ou/ou de surcharger les fonctions utilisées par l'objet map afin d'adapter l'objet au type de jeu