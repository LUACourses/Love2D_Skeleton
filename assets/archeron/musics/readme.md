- Ce dossier contient les musiques du jeu. 

- Les musiques par défaut qui peuvent être automatiquement chargées à chaque écrans de jeu doivent être présentes dans ce dossier sous la forme screen<name>.mp3
  voici une liste non exhaustives des noms de fichier possibles pouvant être chargé automatiquement. 
    screenEnd.mp3
    screenStart.mp3 
    screenStats.mp3

- Les musiques par défaut qui peuvent être automatiquement chargées à chaque niveau de jeu doivent être présentes dans ce dossier sous la forme mmLevel<numero>.mp3
  voici une liste non exhaustives des noms de fichier possibles pouvant être chargé automatiquement.
    mmLevel1.mp3     
    mmLevel2.mp3     
    ...