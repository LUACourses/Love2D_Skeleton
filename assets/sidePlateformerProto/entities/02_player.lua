-- Entité représentant le joueur
-- Utilise une forme d'héritage simpSle:  player qui inclus character qui inclus sprite
-- ****************************************

-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "player" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_CHARACTER_PLAYER = SPRITE_TYPE_CHARACTER..SEP_NEXT..SPRITE_TYPE_DETAIL -- tous les joueurs
SPRITE_TYPE_CHARACTER_PLAYER1 = SPRITE_TYPE_CHARACTER_PLAYER..LAST_EXT_1 -- le joueur courant

--- Création du joueur
-- @return un pseudo objet player
function createPlayer ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createPlayer")"

  local lPlayer = createCharacter (
    SPRITE_TYPE_CHARACTER_PLAYER1, -- pType
    0, -- pX
    0, -- pY
    nil, -- pWidth (calculé automatiquement en fonction de la map)
    nil, -- pHeight (calculé automatiquement en fonction de la map)
    FOLDER_PLAYER_IMAGES.."/player.png", -- pImage
    -600, -- pVelocityJump
    600, -- pSpeed
    200, -- pFriction
    1500, -- pGravity
    200, -- pVelocityMax. NOTE: en augmentant ce paramètre on diminue la limite de vélocité et on augmente l'inertie du mouvement (par exemple on rend le sol moins adhérent)
    1, -- pWeight. NOTE: en augmentant ce paramètre on augmente la vitesse de chute et diminue la hauteur des sauts
    nil, -- pLifeImage
    1, -- pLevel, valeur restaurée par restorePlayer
    3, -- pLife
    0, -- pSCore, valeur restaurée par restorePlayer
    true, -- pUseFriction
    false, -- pMoveWithMouse
    true, -- pMoveWithKeys
    nil, -- pColor (par défaut)
    0, -- pvX
    0, -- pvY
    true, -- pCanCollideWithTile
    true, -- pCanJump
    false, -- pUseVelocity (ajoute de l'inertie au mouvement)
    1, -- pEnergy
    true -- pMustRespawnIfDestroyed
  )

  --- Initialise l'objet
  function lPlayer.initialize ()
    debugFunctionEnter("player.initialize")
    lPlayer.fallLimit = 6
    lPlayer.fallDamage = 0
    lPlayer.direction = SPRITE_MOVE_RIGHT

    -- Ajoute les animations du joueur (si les images nécessaires sont présentes)
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lPlayer.addAnimation(ANIM_NAME_MOVE_IDLE, FOLDER_PLAYER_ANIMS.."/idle", ".png")
    lPlayer.addAnimation(ANIM_NAME_MOVE_RUN, FOLDER_PLAYER_ANIMS.."/run", ".png")
    lPlayer.addAnimation(ANIM_NAME_MOVE_CLIMB, FOLDER_PLAYER_ANIMS.."/climb", ".png")
    lPlayer.addAnimation(ANIM_NAME_MOVE_CLIMB_IDLE, FOLDER_PLAYER_ANIMS.."/climb_idle", ".png")
    lPlayer.addAnimation(ANIM_NAME_MOVE_FALL, FOLDER_PLAYER_ANIMS.."/fall", ".png")
    lPlayer.addAnimation(ANIM_NAME_MOVE_JUMP, FOLDER_PLAYER_ANIMS.."/jump", ".png")
    lPlayer.addAnimation(ANIM_NAME_STATUS_LOSE_LIFE, FOLDER_PLAYER_ANIMS.."/loselife", ".png", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_NORMAL, lPlayer.checkLife)
    lPlayer.addAnimation(ANIM_NAME_STATUS_DESTROY, FOLDER_PLAYER_ANIMS.."/destroy", ".png", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lPlayer.clean)
    lPlayer.playAnimation(ANIM_NAME_MOVE_IDLE)

  end -- player.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lPlayer.characterDraw = lPlayer.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lPlayer.draw ()
    ---- debugFunctionEnterNL("player.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
    lPlayer.characterDraw()
  end -- player.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lPlayer.characterDestroy = lPlayer.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lPlayer.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("player.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)
    -- la destruction effective se fait dans la classe parent
    lPlayer.characterDestroy()
  end -- player.destroy
  ]]

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lPlayer.spriteUpdate = lPlayer.update
  function lPlayer.update (pDt)
    ---- debugFunctionEnterNL("player.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lPlayer.spriteUpdate(pDt)

    if (lPlayer.status == SPRITE_STATUS_DESTROYING or lPlayer.status == SPRITE_STATUS_DESTROYED) then return end

    -- par défaut, adapte la direction du sprite en fonction de son état
    if (lPlayer.isJumping) then
      lPlayer.direction = SPRITE_MOVE_UP
    elseif (lPlayer.isFalling) then
      lPlayer.direction = SPRITE_MOVE_DOWN
    end
    if (lPlayer.currentAnimation.flipedToRight) then
      lPlayer.direction = SPRITE_MOVE_LEFT
    else
      lPlayer.direction = SPRITE_MOVE_RIGHT
    end

    -- déplacement avec le clavier
    if (appState.currentScreen.name == SCREEN_PLAY) then
      if (lPlayer.moveWithKeys and lPlayer.checkIfActive()) then

        -- déplacement avec tuiles et collision possible, les déplacements sont en fonction des tuiles autour du joueur
        -- NOTE un plateformer fonctionne toujours avec tuiles et collisions
        -- ******************************
        -- Id des Tuiles sous le joueur
        local tx = math.floor(lPlayer.x)
        local ty = math.floor(lPlayer.y)
        local ty2 = math.floor(lPlayer.y + map.tileheight)
        local idUnder = map.getTileAt(tx, ty)
        local idOverlap = map.getTileAt(tx, ty2)

        -- Vérifie si le joueur a fini de sauter ou s'il est sur une échelle
        if (lPlayer.isJumping and (lPlayer.collideWithTileBottom() ~= SPRITE_COLLISION_NONE or map.isLadder(idUnder))) then
          lPlayer.isJumping = false
          lPlayer.playSound("Standing")
          lPlayer.updateLastStanding(true)
          lPlayer.alignOnLineTile()
        end
        
        -- Vérifie si le joueur est sur une échelle
        local isOnLadder = map.isLadder(idUnder) or map.isLadder(idOverlap)

        if (not map.isLadder(idOverlap) and map.isLadder(idUnder)) then
          lPlayer.updateLastStanding(true)
        end
        --if (map.isLadder(idOverlap) and map.isLadder(idUnder)) then
        --createAnimationName = ANIM_NAME_MOVE_CLIMB_IDLE
        --end

        -- Touches droite et gauche du Clavier
        -- rappel: on ne tient pas compte de la vélocité max si elle est inférieure à 0
        if (love.keyboard.isDown(lPlayer.keys.moveRight) or love.keyboard.isDown(lPlayer.keys.moveRightAlt)) then
          lPlayer.vX = lPlayer.vX + lPlayer.speed * pDt
          if (lPlayer.velocityMax >= 0 and lPlayer.vX > lPlayer.velocityMax) then lPlayer.vX = lPlayer.velocityMax end
          lPlayer.currentAnimation.flipedToRight = false
        end
        if (love.keyboard.isDown(lPlayer.keys.moveLeft) or love.keyboard.isDown(lPlayer.keys.moveLeftAlt)) then
          lPlayer.vX = lPlayer.vX - lPlayer.speed * pDt
          if (lPlayer.velocityMax >= 0 and lPlayer.vX < -lPlayer.velocityMax) then lPlayer.vX = -lPlayer.velocityMax end
          lPlayer.currentAnimation.flipedToRight = true
        end

        -- Touches haut : Sauter
        if ((love.keyboard.isDown(lPlayer.keys.moveUp) or love.keyboard.isDown(lPlayer.keys.moveUpAlt)) and lPlayer.isStanding and lPlayer.canJump and not map.isLadder(idOverlap)) then
          lPlayer.jump()
        end

        -- Vérifie si le joueur grimpe
        if (isOnLadder and not isJumping) then
          lPlayer.gravity = 0
          lPlayer.vY = 0
          lPlayer.canJump = false
          lPlayer.isClimbing = true
        end

        -- Touches haut : Monter à l'échelle
        if ((love.keyboard.isDown(lPlayer.keys.moveUp) or love.keyboard.isDown(lPlayer.keys.moveUpAlt)) and isOnLadder and not lPlayer.isJumping) then
          lPlayer.vY = (-math.floor(lPlayer.initialValues.speed / 4)) -- on grimpe 2 fois moins vite que la vitesse normale
          lPlayer.isClimbing = true
          lPlayer.direction = SPRITE_MOVE_UP
        end

         -- Touches bas : Descendre de l'échelle
        if ((love.keyboard.isDown(lPlayer.keys.moveDown) or love.keyboard.isDown(lPlayer.keys.moveDownAlt)) and isOnLadder) then
          lPlayer.vY = (math.floor(lPlayer.initialValues.speed / 4)) -- on descend 2 fois moins vite que la vitesse normale
          lPlayer.isClimbing = true
          lPlayer.direction = SPRITE_MOVE_DOWN
        end

        -- Vérifie si le joueur ne grimpe plus
        if (not isOnLadder and lPlayer.gravity == 0 and not lPlayer.isJumping) then
          lPlayer.gravity = lPlayer.initialValues.gravity
        end

        -- Vérifie si le joueur est prêt pour le saut suivant ?
        if (not (love.keyboard.isDown(lPlayer.keys.moveUp) and not love.keyboard.isDown(lPlayer.keys.moveUpAlt)) and not lPlayer.canJump and lPlayer.isStanding) then
          lPlayer.canJump = true
        end

        -- doit-on appliquer la friction ?
        if (lPlayer.useFriction) then
          -- Calcul des vélocités
          -- ******************************
          -- prise en compte de la friction de l'environnement et du poids
          local frictionAndWeight = lPlayer.weight * lPlayer.friction * pDt
          if (lPlayer.vX > 0) then
            lPlayer.vX = lPlayer.vX - frictionAndWeight
          elseif (lPlayer.vX < 0) then
            lPlayer.vX = lPlayer.vX + frictionAndWeight
          end
          if (lPlayer.vY > 0) then
            lPlayer.vY = lPlayer.vY - frictionAndWeight
          elseif (lPlayer.vY < 0) then
            lPlayer.vY = lPlayer.vY + frictionAndWeight
          end
          if (math.abs(lPlayer.vX) < 0.01) then lPlayer.vX = 0 end
          if (math.abs(lPlayer.vY) < 0.01) then lPlayer.vY = 0 end
        end -- if (useFriction) then
      end --if (lPlayer.moveWithKeys and lPlayer..checkIfActive()) then
    end -- if (appState.currentScreen.name == SCREEN_PLAY) then
  end -- player.update

  --- Le joueur effectue une action (ie. tire un projectile)
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lPlayer.characterAction = lPlayer.action
  function lPlayer.action ()
    -- debugFunctionEnter("player.action")
    lPlayer.characterAction()

    if (not lPlayer.checkIfCanMakeAction()) then return end
    local lProjectile

    -- rappel:    createProjectile(pType                    , pImage                                               ,pX,pY,pLife,pEnergy,pDamages,pSpeed)
    lProjectile = createProjectile(SPRITE_TYPE_PROJ_PLAYER, FOLDER_PROJECTILES_IMAGES.."/projectile_player_1.png", 0, 0, 1, 1, 1, 200)
    lProjectile.navPath = createNavPath(NAVPATH_TYPE_TOWARD_GRAVITY, lProjectile, lPlayer)

    lProjectile.timeToLive = 5
    lProjectile.gravity = 2
    lProjectile.weight = 10
    lProjectile.mustDestroyedOnCollision = true
    lProjectile.canCollideWithTile = true
    lProjectile.mustDestroyedWithFX = false
    lProjectile.mustDestroyedWithSound = false

    -- les sons et les animations doivent être joués dans le cet objet et pas dans son parent sinon les noms de fichiers chargés automatiquement ne seront pas les bons (liés au type de sprite)
    lPlayer.playSound("Action")
    lPlayer.playAnimation(ANIM_NAME_ACT_ACTION1)
  end -- character.action

  --- Le joueur saute
  function lPlayer.jump ()
    debugFunctionEnter("character.jump")
    lPlayer.isJumping = true
    lPlayer.updateLastStanding(false)
    lPlayer.canJump = false
    lPlayer.gravity = lPlayer.initialValues.gravity
    lPlayer.vY = lPlayer.velocityJump
    lPlayer.playSound("Jump")
    lPlayer.playAnimation(ANIM_NAME_MOVE_JUMP)
  end -- character.jump

  -- initialisation par défaut
  lPlayer.initialize()

  return lPlayer
end -- createPlayer
