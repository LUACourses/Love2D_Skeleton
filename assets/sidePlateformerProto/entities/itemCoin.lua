-- Entité représentant une pièce
-- Utilise une forme d'héritage simple: coin qui inclus item qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "coin" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_ITEM_COLLECT_COIN = SPRITE_TYPE_ITEM_COLLECT..SEP_NEXT..SPRITE_TYPE_DETAIL -- une pièce de monnaie
SPRITE_TYPE_ITEM_COLLECT_COIN1 = SPRITE_TYPE_ITEM_COLLECT_COIN..LAST_EXT_1 -- le 1er type de pièce de monnaie

--- Création d'un objet coin
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_ITEMS_IMAGES.."/coin.png" sera utilisée.
-- @return un pseudo objet coin
function createCoin (pX, pY, pImage)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createCoin ", pX, pY, pImage)

  if (pImage == nil) then pImage = FOLDER_ITEMS_IMAGES.."/coin.png" end

  local lCoin = createItem(SPRITE_TYPE_ITEM_COLLECT_COIN1, pImage, pX, pY, nil, 1, 1)

  --- Initialise l'objet
  function lCoin.initialize ()
    debugFunctionEnter("coin.initialize")

    -- ajoute les animations
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lCoin.addAnimation(ANIM_NAME_MOVE_IDLE, FOLDER_ITEMS_ANIMS.."/coin_idle", ".png")
    lCoin.playAnimation(ANIM_NAME_MOVE_IDLE)

    -- définit les hotSpots pour les collisions
    lCoin.createCollisionBox(COLLISION_MODEL_BOX4)

    lCoin.updateInitialValues() -- important
  end -- coin.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lCoin.itemDraw = lCoin.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lCoin.draw ()
  ---- debugFunctionEnterNL("coin.draw") -- ATTENTION  cet appel peut remplir le log
  -- affiche l'objet comme un sprite normal
   lCoin.itemDraw()
  end -- coin.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lCoin.itemDestroy = lCoin.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lCoin.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
  debugFunctionEnter("coin.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

  -- la destruction effective se fait dans la classe parent
  lCoin.itemDestroy()
  end -- coin.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lCoin.itemUpdate = lCoin.update
  function lCoin.update (pDt)
  ---- debugFunctionEnterNL("coin.update ", pDT) -- ATTENTION cet appel peut remplir le log
  lCoin.itemUpdate(pDt)
  end -- coin.update
  ]]

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lCoin.itemCollide = lCoin.collide
  function lCoin.collide ()
    ---- debugFunctionEnterNL("coin.collide") -- ATTENTION cet appel peut remplir le log
    lCoin.itemCollide(pDt)

    if (not lCoin.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    local collisionType = SPRITE_COLLISION_NONE
    -- on vérifie la pièce ne collisionne pas avec le joueur
    if (lCoin.collideWithSprite(player) ~= SPRITE_COLLISION_NONE) then
      collisionType = SPRITE_COLLISION_PLAYER
      -- oui, elle collisionne avec le joueur
      lCoin.destroy()
      map.addCollectableCount(1, -1)

      -- on vérifie si toutes les pièces ont été collectées
      if (map.collectableCount[1] <= 0) then
        -- oui, le niveau est ouvert
        -- on parcours tous les sprites pour trouver et ouvrir les portes
        local indexSprite, sprite
        for indexSprite = #entities.sprites, 1, -1 do
          sprite = entities.sprites[indexSprite]
          if (sprite.open ~= nil) then sprite.open() end -- seule les items de type "door" ont cette fonction
        end
      end
    end -- if (lCoin.collideWithSprite(player) ~= SPRITE_COLLISION_NONE)

    return collisionType
  end -- coin.collide

  -- initialisation par défaut
  lCoin.initialize()

  return lCoin
end -- createCoin
