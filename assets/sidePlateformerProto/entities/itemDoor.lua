-- Entité représentant une porte
-- Utilise une forme d'héritage simple: door qui inclus item qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "door" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_ITEM_EXIT_DOOR = SPRITE_TYPE_ITEM_EXIT..SEP_NEXT..SPRITE_TYPE_DETAIL -- une porte qui s'ouvre et qui se ferme (si tout a été collecté)
SPRITE_TYPE_ITEM_EXIT_DOOR1 = SPRITE_TYPE_ITEM_EXIT_DOOR..LAST_EXT_1 -- le 1er type de porte

--- Création d'un objet Door
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_ITEMS_IMAGES.."/door.png" sera utilisée.
-- @return un pseudo objet door
function createDoor (pX, pY, pImage)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createDoor ", pX, pY, pImage)

  if (pImage == nil) then pImage = FOLDER_ITEMS_IMAGES.."/door.png" end

  local lDoor = createItem(SPRITE_TYPE_ITEM_EXIT_DOOR1, pImage, pX, pY, nil, 1, 1)

  lDoor.points = 0
  lDoor.mustDestroyedWithFX = false
  lDoor.mustDestroyedWithSound = false
  lDoor.mustDestroyedOnCollision = false
  
  -- attributs spécifiques à une porte
  -- ******************************
  -- La porte est elle ouverte ?
  lDoor.isOpen = false

  --- Initialise l'objet
  function lDoor.initialize ()
    debugFunctionEnter("door.initialize ")

    -- ajoute les animations
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lDoor.addAnimation(ANIM_NAME_STATUS_CLOSE, FOLDER_ITEMS_ANIMS.."/door_close", ".png")
    lDoor.addAnimation(ANIM_NAME_STATUS_OPEN, FOLDER_ITEMS_ANIMS.."/door_open", ".png")
    lDoor.playAnimation(ANIM_NAME_STATUS_CLOSE)
    lDoor.isOpen = false

    -- définit les hotSpots pour les collisions
    lDoor.createCollisionBox(COLLISION_MODEL_BOX4)

    lDoor.updateInitialValues() -- important
  end -- door.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lDoor.itemDraw = lDoor.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lDoor.draw ()
  ---- debugFunctionEnterNL("door.draw") -- ATTENTION  cet appel peut remplir le log
  -- affiche l'objet comme un sprite normal
   lDoor.itemDraw()
  end -- door.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lDoor.itemDestroy = lDoor.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lDoor.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
  debugFunctionEnter("door.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

  -- la destruction effective se fait dans la classe parent
  lDoor.itemDestroy()
  end -- door.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lDoor.itemUpdate = lDoor.update
  function lDoor.update (pDt)
  ---- debugFunctionEnterNL("door.update ", pDT) -- ATTENTION cet appel peut remplir le log
  lDoor.itemUpdate(pDt)

  end -- door.update
  ]]

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lDoor.itemCollide = lDoor.collide
  function lDoor.collide ()
    ---- debugFunctionEnterNL("door.collide") -- ATTENTION cet appel peut remplir le log
    lDoor.itemCollide(pDt)

    if (not lDoor.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    local collisionType = SPRITE_COLLISION_NONE

    -- on vérifie si la porte ne collisionne pas avec le joueur
    if (lDoor.collideWithSprite(player) ~= SPRITE_COLLISION_NONE) then
      -- oui, elle collisionne avec le joueur
      collisionType = SPRITE_COLLISION_PLAYER
      if (lDoor.isOpen) then
        -- si elle est ouverte, on passe au niveau suivant
        nextLevel()
      end
    end -- if (lDoor.collideWithSprite(player) ~= SPRITE_COLLISION_NONE)
    return collisionType
  end -- door.collide

  -- ouvre la porte
  function lDoor.open ()
    lDoor.playAnimation(ANIM_NAME_STATUS_OPEN)
    lDoor.isOpen = true
  end

  -- ferme la porte
  function lDoor.close ()
    lDoor.playAnimation(ANIM_NAME_STATUS_CLOSE)
    lDoor.isOpen = false
  end

  -- initialisation par défaut
  lDoor.initialize()

  return lDoor
end -- createDoor
