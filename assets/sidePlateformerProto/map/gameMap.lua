-- Fonctions pour la map propre du type SIDE_PLATEFORMER
-- Utilise une forme d'héritage simpSle: gameMap qui inclus map
-- ****************************************

--- Crée un pseudo objet de type gameMap
-- @param pLevel (OPTIONNEL) numéro de la map (niveau de jeu) associé à la map
-- @return un pseudo objet gameMap
function createGameMap (pLevel)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("pLevel ", pLevel)

  local lGameMap = createMap (pLevel)

  -- Override des fonctions de la map
  -- ******************************

  -- Charge et initialise  la map
  -- @param pLevel (OPTIONNEL) niveau de jeu associé à la map. Si absent, 1 sera utilisé.
  function lGameMap.initialize (pLevel)
    debugFunctionEnter("gameMap.initialize ", pLevel)
    if (pLevel == nil) then pLevel = 1 end
    lGameMap.mapIsOK = false
    lGameMap.level = pLevel
    
    -- TODO: extraire le nom depuis le fichier décrivant la map
    lGameMap.names = {
      "The beginning",
      "Welcome in hell"
    }

    -- attributs spécifiques à cette map
    -- ******************************
    
    -- objets disponibles dans le niveau
    lGameMap.items = {}
    -- nombre d'éléments à collecter dans le niveau (utile pour ouvrir la porte)
    lGameMap.collectableCount = {0}

    -- charge les fichiers liés au niveau de la map
    if (love.filesystem.exists (FOLDER_MAP_LEVELS)) then

      -- IMPORTANT: si on a déjà chargé un niveau, efface toutes les entités
      resetEntities()

      -- charge les images des tuiles
      if (love.filesystem.exists (FOLDER_MAP_IMAGES_TILES)) then
        lGameMap.tiles["0"] = {image = "", type = TILE_SPECIAL_EMPTY}
        lGameMap.tiles["P"] = {image = "", type = TILE_SPECIAL_PLAYER_START}
        lGameMap.tiles["1"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile1.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["2"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile2.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["3"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile3.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["4"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile4.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["5"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile5.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["="] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile=.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["["] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile[.png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["]"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile].png"), type = TILE_TYPE_SOLID}
        lGameMap.tiles["H"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tileH.png"), type = TILE_TYPE_LADDER}
        lGameMap.tiles["#"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile#.png"), type = TILE_TYPE_LADDER}
        lGameMap.tiles["g"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tileg.png"), type = TILE_TYPE_JUMPTHROUGH}
        lGameMap.tiles[">"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile-arrow-right.png"), type = TILE_TYPE_GO_RIGHT}
        lGameMap.tiles["<"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile-arrow-left.png"), type = TILE_TYPE_GO_LEFT}
        lGameMap.tiles["^"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile-arrow-up.png"), type = TILE_TYPE_GO_UP}
        lGameMap.tiles["`"] = {image = resourceManager.getImage(FOLDER_MAP_IMAGES_TILES.."/tile-arrow-down.png"), type = TILE_TYPE_GO_DOWN}

        if (lGameMap.tiles["1"].image ~= nil) then
          lGameMap.tilewidth, lGameMap.tileheight = lGameMap.tiles["1"].image:getDimensions()
          lGameMap.cols = math.floor(viewport.getWidth() / lGameMap.tilewidth)
          lGameMap.rows = math.floor(viewport.getHeight() / lGameMap.tileheight)
          lGameMap.type = MAP_TYPE_TILED
          lGameMap.mapIsOK = true
        else
          logMessage("gameMap.initialize:le fichier '"..FOLDER_MAP_IMAGES_TILES.."/tile1.png' n'a pas été trouvé")
        end
      else
        logMessage("gameMap.initialize:le dossier '"..FOLDER_MAP_IMAGES_TILES.."' contenant les images des tuiles n'a pas été trouvé")
      end
    else
      logMessage("gameMap.initialize:le dossier '"..FOLDER_MAP_LEVELS.."' contenant la définitions des niveaux n'a pas été trouvé")
    end -- if (love.filesystem.exists (FOLDER_MAP_LEVELS)) then

    assertEqualQuit(lGameMap.grid, nil, "lGameMap.draw:lGameMap.grid", true)
    assertEqualQuit(lGameMap.tiles, nil, "lGameMap.draw:lGameMap.tiles", true)

    return lGameMap.mapIsOK
  end -- gameMap.initialize

  --- Crée et positionne les contenus (objets, PNJs...) dans la map
  -- @param pDt Delta time
  --[[ à décommenter pour des comportements spécifiques
  function lGameMap.update (pDt)
  ---- debugFunctionEnterNL("gameMap.update ", pDt) -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  end
  ]]

  --- affiche la map
  function lGameMap.draw ()
    ---- debugFunctionEnterNL("lGameMap.draw") -- ATTENTION cet appel peut remplir le log
    lGameMap.mapIsOK = true

    if (lGameMap.type == MAP_TYPE_TILED) then

      local l, c, char, drawX, drawY
      local stringSub = string.sub -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe
      for l = 1, #lGameMap.grid do
        for c = 1, #lGameMap.grid[l] do
          char = stringSub(lGameMap.grid[l], c, c)
          if (char ~= nil) then
            if (lGameMap.tiles ~= nil and lGameMap.tiles[char] ~= nil and not lGameMap.isInvisible(char) and lGameMap.tiles[char].image ~= "") then
              drawX, drawY = lGameMap.mapToPixel(c, l)
              HUD.drawImage(lGameMap.tiles[char].image, drawX, drawY)
            end
          end
        end
      end
    end -- if (lGameMap.type == MAP_TYPE_TILED) then
    
    return lGameMap.mapIsOK
  end -- lGameMap.draw

  --- Crée et positionne les contenus (objets, PNJs...) dans la map pour un niveau (de difficulté) donné
  -- @param pLevel (OPTIONNEL) niveau (de difficulté). Si absent, 1 sera utilisée.
  function lGameMap.createLevelContent (pLevel)
    debugFunctionEnter("gameMap.createLevelContent ", pLevel)
    lGameMap.mapIsOK = true

    -- ANALYSE la map pour placer les éléments
    -- ******************************
    if (lGameMap.type == MAP_TYPE_TILED) then
      local filename, line
      filename = FOLDER_MAP_LEVELS.."/level"..tostring(lGameMap.level) .. ".txt"

      lGameMap.grid = resourceManager.getTextFile(filename)

      if (lGameMap.grid ~= nil) then
        -- Analyse de la map
        local l, c, char, objet, key, posX, posY
        for l = 1, #lGameMap.grid do
          for c = 1, #lGameMap.grid[1] do
            key = l.."##"..c
            char = string.sub(lGameMap.grid[l], c, c)
            posX, posY = lGameMap.mapToPixel(c, l)
            if (char == TILE_SPECIAL_PLAYER_START) then
              -- point de départ du joueur
              lGameMap.playerStart.col = c
              lGameMap.playerStart.line = l
            elseif (char == TILE_SPECIAL_COIN) then
              -- ajout d'une piece
              object = createCoin(posX, posY)
              lGameMap.items[key] = {type = char, content = object, col = c, line = l}
              lGameMap.addCollectableCount(1, 1)
            elseif (char == TILE_SPECIAL_DOOR) then
              -- ajout d'une porte
              object = createDoor(posX, posY)
              lGameMap.items[key] = {type = char, content = object, col = c, line = l}
            elseif (char == TILE_SPECIAL_BLOB) then
              object = createNpcGuided(posX, posY, 1, 1, 1)
              object.mustDisplayEnergy = false
            end
          end
        end
      else
        logMessage("gameMap.initialize:le fichier "..filename.." n'a pas été trouvé")
        -- plus de niveaux à charger, la partie est gagnée
        winGame()
        lGameMap.mapIsOK = false
      end -- if (love.filesystem.exists(filename)) then
    end -- if (lGameMap.type == MAP_TYPE_TILED) then
    
    return lGameMap.mapIsOK
  end -- gameMap.createLevelContent

  -- Fonctions spécifiques à cette map
  -- ******************************

  --- Ajoute une valeur au compteur des collectibles
  -- @param pLevel Index des collectable concernés
  -- @param pValue Valeur à ajouter
  function lGameMap.addCollectableCount (pLevel, pValue)
    if (lGameMap.collectableCount == nil or lGameMap.collectableCount[pLevel] == nil) then return end
    lGameMap.collectableCount[pLevel] = lGameMap.collectableCount[pLevel] + pValue
  end -- lGameMap.addCollectableCount

  -- initialisation par défaut
  lGameMap.initialize(pLevel)

  return lGameMap
end -- createGameMap
