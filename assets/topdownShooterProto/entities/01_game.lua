-- Entité représentant un jeu
-- Utilise une forme d'héritage simple: game qui inclus game
-- ****************************************

-- NOTE: SOUS-TYPE DE JEU doit être défini dans contants.lua car il est utilisé pour la définition des dossiers et avant l'auto load des modules.

--- Création d'un jeu
-- @return un pseudo objet game
function createGame ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createGame")"

  local lGame = createPrototype()

  -- nom du jeu (on utilise les infos du manifest pour éviter les doublons)
  lGame.name = GAME_NAMES[settings.gameIndex]
  -- titre du jeu  (on utilise les infos du manifest pour éviter les doublons)
  lGame.title = GAME_MANIFESTS[lGame.name].title
  -- nombre de niveaux à atteindre pour gagner le jeu
  lGame.levelToWin = 3
  -- nombre de points à marquer pour passer un niveau
  lGame.pointForLevel = 0

  -- texte d'aide affiché sur l'écran de départ
  lGame.helpText = {
    "Détruisez tous vos ennemis et restez en vie",
    "Atteignez le niveau "..lGame.levelToWin.." pour gagner",
    "",
    "ZQSD ou bien les flèches du clavier pour se déplacer",
    "Espace pour tirer",
    "Appuyer sur ESC pour quitter"
  }

  -- coefficient de taille pour les polices utilisée sur les écrans (voir HUD)
  lGame.fontSizeFactor = 1

  --- Initialise l'objet
  function lGame.initialize ()
    debugFunctionEnter("game.initialize")

    -- Redéfinit certains paramètres de configuration en fonction du jeu choisi
    love.window.setTitle(lGame.title)
    love.window.setMode(1200, 900)
  end --game.initialize

  --- Initialise le prototype
  function lGame.startGame ()
    debugFunctionEnter("game.startGame")

    debugMessage ("LANCEMENT DE "..lGame.title.." ("..lGame.name..")")

    -- init de la camera, qui en se déplaçant automatiquement va permettre un scrolling
    camera.initialize(0, 60) -- avance de 1 pixel en Y
  end -- game.startGame

  -- initialisation par défaut
  lGame.initialize()

  return lGame
end -- createGame
