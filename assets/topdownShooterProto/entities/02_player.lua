-- Entité représentant le joueur
-- Utilise une forme d'héritage simpSle:  player qui inclus character qui inclus sprite
-- ****************************************

-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "player" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_CHARACTER_PLAYER = SPRITE_TYPE_CHARACTER..SEP_NEXT..SPRITE_TYPE_DETAIL -- tous les joueurs
SPRITE_TYPE_CHARACTER_PLAYER1 = SPRITE_TYPE_CHARACTER_PLAYER..LAST_EXT_1 -- le joueur courant

--- Création du joueur
-- @return un pseudo objet player
function createPlayer ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createPlayer")"

  local lPlayer = createCharacter (
    SPRITE_TYPE_CHARACTER_PLAYER1, -- pType
    0, -- pX
    0, -- pY
    nil, -- pWidth (calculé automatiquement en fonction de la map)
    nil, -- pHeight (calculé automatiquement en fonction de la map)
    FOLDER_PLAYER_IMAGES.."/player.png", -- pImage
    0, -- pVelocityJump
    700, -- pSpeed
    200, -- pFriction
    0, -- pGravity
    300, -- pVelocityMax. NOTE: en augmentant ce paramètre on diminue la limite de vélocité et on augmente l'inertie du mouvement (par exemple on rend le sol moins adhérent)
    1, -- pWeight. NOTE: en augmentant ce paramètre on augmente la vitesse de chute et diminue la hauteur des sauts
    nil, -- pLifeImage
    1, -- pLevel, valeur restaurée par restorePlayer
    3, -- pLife
    0, -- pSCore, valeur restaurée par restorePlayer
    true, -- pUseFriction
    false, -- pMoveWithMouse
    nil, -- pMoveWithKeys
    nil, -- pColor (par défaut)
    0, -- pvX
    0, -- pvY
    false, -- pCanCollideWithTile
    false, -- pCanJump
    true, -- pUseVelocity (ajoute de l'inertie au mouvement)
    4, -- pEnergy
    true -- pMustRespawnIfDestroyed
  )

  --- Initialise l'objet
  function lPlayer.initialize ()
    debugFunctionEnter("player.initialize")

    lPlayer.direction = SPRITE_MOVE_UP
    -- Ajoute les animations du joueur (si les images nécessaires sont présentes)
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lPlayer.addAnimation(ANIM_NAME_MOVE_IDLE, FOLDER_PLAYER_ANIMS.."/idle", ".png")
    lPlayer.addAnimation(ANIM_NAME_ACT_ACTION1, FOLDER_PLAYER_ANIMS.."/action1", ".png", nil, false, ANIM_NAME_MOVE_IDLE)
    lPlayer.addAnimation(ANIM_NAME_STATUS_LOSE_LIFE, FOLDER_PLAYER_ANIMS.."/loselife", ".png", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_NORMAL, lPlayer.checkLife)
    lPlayer.addAnimation(ANIM_NAME_STATUS_DESTROY, FOLDER_PLAYER_ANIMS.."/destroy", ".png", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lPlayer.clean)
    lPlayer.addAnimation(ANIM_NAME_STATUS_SPAWN, FOLDER_PLAYER_ANIMS.."/spawn", ".png", nil, false, ANIM_NAME_MOVE_IDLE)
    lPlayer.playAnimation(ANIM_NAME_MOVE_IDLE)
  end -- player.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lPlayer.characterDraw = lPlayer.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lPlayer.draw ()
    ---- debugFunctionEnterNL("player.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
    lPlayer.characterDraw()
  end -- player.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lPlayer.characterDestroy = lPlayer.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
    function lPlayer.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("player.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)
    -- la destruction effective se fait dans la classe parent
    lPlayer.characterDestroy()
  end -- player.destroy
  ]]

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lPlayer.spriteUpdate = lPlayer.update
  function lPlayer.update (pDt)
    ---- debugFunctionEnterNL("player.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lPlayer.spriteUpdate(pDt)

    if (lPlayer.status == SPRITE_STATUS_DESTROYING or lPlayer.status == SPRITE_STATUS_DESTROYED) then return end

    -- déplacement avec le clavier
    if (appState.currentScreen.name == SCREEN_PLAY) then
      if (lPlayer.moveWithKeys and lPlayer.checkIfActive()) then
        -- map sans tuile ou sans collision avec les tuiles, les déplacements sont "normaux", sans contraintes spécifiques
        -- ******************************
        -- doit-on utiliser les vélocité (inertie) ?
        if (lPlayer.useVelocity) then
          -- oui, déplacements avec utilisation des vélocités
          if (love.keyboard.isDown(lPlayer.keys.moveRight) or love.keyboard.isDown(lPlayer.keys.moveRightAlt)) then
            lPlayer.vX = (lPlayer.vX + lPlayer.speed * pDt)
            lPlayer.direction = SPRITE_MOVE_RIGHT
          end
          if (love.keyboard.isDown(lPlayer.keys.moveLeft) or love.keyboard.isDown(lPlayer.keys.moveLeftAlt)) then
            lPlayer.vX = (lPlayer.vX - lPlayer.speed * pDt)
            lPlayer.direction = SPRITE_MOVE_LEFT
          end
          if (love.keyboard.isDown(lPlayer.keys.moveUp) or love.keyboard.isDown(lPlayer.keys.moveUpAlt)) then
            lPlayer.vY = (lPlayer.vY - lPlayer.speed * pDt)
            lPlayer.direction = SPRITE_MOVE_UP
          end
          if (love.keyboard.isDown(lPlayer.keys.moveDown) or love.keyboard.isDown(lPlayer.keys.moveDownAlt)) then
            lPlayer.vY = (lPlayer.vY + lPlayer.speed * pDt)
            lPlayer.direction = SPRITE_MOVE_DOWN
          end

          if (lPlayer.velocityMax >= 0) then
            -- rappel: on ne tient pas compte de la vélocité max si elle est inférieure à 0
            -- limite la vélocité en X
            if (lPlayer.vX < -lPlayer.velocityMax) then
              lPlayer.vX = -lPlayer.velocityMax
            end
            if (lPlayer.vX > lPlayer.velocityMax) then
              lPlayer.vX = lPlayer.velocityMax
            end
            -- limite la vélocité en Y
            if (lPlayer.vY < -lPlayer.velocityMax) then
              lPlayer.vY = -lPlayer.velocityMax
            end
            if (lPlayer.vY > lPlayer.velocityMax) then
              lPlayer.vY = lPlayer.velocityMax
            end
          end
        else
          -- non, déplacements sans utilisation des vélocités (direct)
          local xMin, yMin, xMax, yMax = lPlayer.getScreenLimits()
          if (love.keyboard.isDown(lPlayer.keys.moveRight) or love.keyboard.isDown(lPlayer.keys.moveRightAlt) and lPlayer.x < xMax) then
            lPlayer.x = lPlayer.x + lPlayer.speed * pDt
            lPlayer.direction = SPRITE_MOVE_RIGHT
          elseif (love.keyboard.isDown(lPlayer.keys.moveLeft) or love.keyboard.isDown(lPlayer.keys.moveLeftAlt) and lPlayer.x > xMin) then
            lPlayer.x = lPlayer.x - lPlayer.speed * pDt
            lPlayer.direction = SPRITE_MOVE_LEFT
          end
          if (love.keyboard.isDown(lPlayer.keys.moveUp) or love.keyboard.isDown(lPlayer.keys.moveUpAlt) and lPlayer.y > yMin) then
            lPlayer.y = lPlayer.y - lPlayer.speed * pDt
            lPlayer.direction = SPRITE_MOVE_UP
          elseif (love.keyboard.isDown(lPlayer.keys.moveDown) or love.keyboard.isDown(lPlayer.keys.moveDownAlt) and lPlayer.y < yMax) then
            lPlayer.y = lPlayer.y + lPlayer.speed * pDt
            lPlayer.direction = SPRITE_MOVE_DOWN
          end
        end

      end --if (lPlayer.moveWithKeys and lPlayer..checkIfActive()) then
    end -- if (appState.currentScreen.name == SCREEN_PLAY) then
  end -- player.update

  --- Le joueur effectue une action (ie. tire un projectile)
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lPlayer.characterAction = lPlayer.action
  function lPlayer.action ()
    -- debugFunctionEnter("player.action")
    lPlayer.characterAction()

    if (not lPlayer.checkIfCanMakeAction()) then return end
    local lProjectile

    -- rappel:    createProjectile(pType                    , pImage                                               ,pX,pY,pLife,pEnergy,pDamages,pSpeed)
    lProjectile = createProjectile(SPRITE_TYPE_PROJ_PLAYER, FOLDER_PROJECTILES_IMAGES.."/projectile_player_1.png", 0, 0, 1, 1, 1, 200)
    lProjectile.navPath = createNavPath(NAVPATH_TYPE_UP, lProjectile, lPlayer)

    -- les sons et les animations doivent être joués dans le cet objet et pas dans son parent sinon les noms de fichiers chargés automatiquement ne seront pas les bons (liés au type de sprite)
    lPlayer.playSound("Action")
    lPlayer.playAnimation(ANIM_NAME_ACT_ACTION1)
  end -- character.action

  -- initialisation par défaut
  lPlayer.initialize()

  return lPlayer
end -- createPlayer
