-- Entité représentant un vaisseau qui rebondit sur les bords de l'écran
-- Utilise une forme d'héritage simple: npcBounce qui inclus npc qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "npcBounce" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_NPC_BOUNCE = SPRITE_TYPE_NPC..NEXT_EXT_MOVEBOUNCE -- pnj avec déplacement avec rebond sur les bord de l'écran (par exemple un vaisseau)
SPRITE_TYPE_NPC_BOUNCE1 = SPRITE_TYPE_NPC_BOUNCE..LAST_EXT_1 -- pnj avec rebond n°1, par défaut rebondi en bas de l'écran

--- Création d'un objet npcBounce
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids du npc. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_NPCS_IMAGES.."/npcBounce.png" sera utilisée.
-- @param pSubType (OPTIONNEL) sous-type de pnj (voir les constantes SPRITE_TYPE_NPC_BOUNCE_XXX). Si absent, SPRITE_TYPE_NPC_BOUNCE1 sera utilisée.
-- @return un pseudo objet npcBounce
function createNpcBounce (pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
    -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
    debugFunctionEnter("createNpcBounce ", pX, pY, pWeight, pLife, pEnergy, pImage)
    if (pSubType == nil) then pSubType = SPRITE_TYPE_NPC_BOUNCE1 end
    local detailIndex = getLastSuffix(pSubType)

    if (pImage == nil) then pImage = FOLDER_NPCS_IMAGES.."/"..SPRITE_TYPE_DETAIL..detailIndex..".png" end

    local lNpcBounce = createNPC(pSubType, pImage, pX, pY, pWeight, pLife, pEnergy)

    --- Initialise l'objet
    function lNpcBounce.initialize ()
        debugFunctionEnter("npcBounce.initialize")

        local detailIndex = getLastSuffix(lNpcBounce.type)
        local actionDelaiWithDtFactor = game.getDelaiDifficultyFactor()

        lNpcBounce.speed = 125 * game.getSpeedDifficultyFactor()
        lNpcBounce.mustDestroyedAtBottom = false
        lNpcBounce.mustDestroyedAtTop = false
        lNpcBounce.mustDestroyedAtLeft = false
        lNpcBounce.mustDestroyedAtRight = false
        lNpcBounce.actionDelai = {
            min = 2 * actionDelaiWithDtFactor,
            max = 3 * actionDelaiWithDtFactor
        }
        lNpcBounce.actionTimer = lNpcBounce.actionDelai.max or 1 * actionDelaiWithDtFactor
        if (math.random(1, 2) < 1) then lNpcBounce.navPath = createNavPath(NAVPATH_TYPE_RIGHTDOWN, lNpcBounce) else lNpcBounce.navPath = createNavPath(NAVPATH_TYPE_LEFTDOWN, lNpcBounce) end

        -- ajoute les animations
        -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
        --lNpcBounce.addAnimation(ANIM_NAME_MOVE_WALK, FOLDER_NPCS_ANIMS.."/npcBounce1/walk", ".png")
        --lNpcBounce.playAnimation(ANIM_NAME_MOVE_WALK)

        -- définit les hotSpots pour les collisions
        lNpcBounce.createCollisionBox(COLLISION_MODEL_BOX4)

        lNpcBounce.updateInitialValues() -- important
    end -- npcBounce.initialize

    -- Override des fonctions du sprite
    -- ******************************

    --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lNpcBounce.npcDraw = lNpcBounce.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcBounce.draw ()
    ---- debugFunctionEnterNL("npcBounce.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
     lNpcBounce.npcDraw()
  end -- npcBounce.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lNpcBounce.npcDestroy = lNpcBounce.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcBounce.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("npcBounce.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

    -- la destruction effective se fait dans la classe parent
    lNpcBounce.npcDestroy()
  end -- npcBounce.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcBounce.npcUpdate = lNpcBounce.update
  function lNpcBounce.update (pDt)
    ---- debugFunctionEnterNL("npcBounce.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lNpcBounce.npcUpdate(pDt)

  end -- npcBounce.update

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcBounce.npcCollide = lNpcBounce.collide
  function lNpcBounce.collide ()
    ---- debugFunctionEnterNL("npcBounce.collide") -- ATTENTION cet appel peut remplir le log
    local collisionType = lNpcBounce.npcCollide(pDt)

    if (not lNpcBounce.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    return collisionType
  end -- npcBounce.collide
  ]]

    --- le PNJ effectue une action (tire un projectile)
    -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
    lNpcBounce.npcAction = lNpcBounce.action
    function lNpcBounce.action ()
        -- debugFunctionEnter("npc.action")
        lNpcBounce.npcAction()

        if (not lNpcBounce.checkIfCanMakeAction()) then return end
        local lProjectile
        if (lNpcBounce.type == SPRITE_TYPE_NPC_BOUNCE1) then
            -- COMPORTEMENT SPÉCIFIQUE: le projectile est dirigé vers le player
            -- ******************************
            lProjectile = createProjectile(SPRITE_TYPE_PROJ_NPC, FOLDER_PROJECTILES_IMAGES.."/projectile_npc_3.png", 0, 0, 1, 1, 2, 260)
            lProjectile.navPath = createNavPath(NAVPATH_TYPE_FROMTO, lProjectile, lNpcBounce, player)
        end
    end

    -- initialisation par défaut
    lNpcBounce.initialize()

    return lNpcBounce
end -- createDoor
