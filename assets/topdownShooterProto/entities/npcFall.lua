-- Entité représentant un vaisseau qui descend du haut de l'écran
-- Utilise une forme d'héritage simple: npcFall qui inclus npc qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "npcFall" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_NPC_FALL = SPRITE_TYPE_NPC..NEXT_EXT_MOVEFALL -- pnj avec déplacement vers le bas sans rebond (par exemple un vaisseau)
SPRITE_TYPE_NPC_FALL1 = SPRITE_TYPE_NPC_FALL..LAST_EXT_1 -- pnj avec déplacement vers le bas  n°1, par défaut détruit en bas de l'écran
SPRITE_TYPE_NPC_FALL2 = SPRITE_TYPE_NPC_FALL..LAST_EXT_2 -- pnj avec déplacement vers le bas  n°2, par défaut détruit en bas de l'écran

--- Création d'un objet npcFall
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids du npc. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_NPCS_IMAGES.."/npcFall.png" sera utilisée.
-- @param pSubType (OPTIONNEL) sous-type de pnj (voir les constantes SPRITE_TYPE_NPC_FALL_XXX). Si absent, SPRITE_TYPE_NPC_FALL1 sera utilisée.

-- @return un pseudo objet npcFall
function createNpcFall (pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
    -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
    debugFunctionEnter("createNpcFall ", pX, pY, pWeight, pLife, pEnergy, pImage)
    if (pSubType == nil) then pSubType = SPRITE_TYPE_NPC_FALL1 end
    local detailIndex = getLastSuffix(pSubType)

    if (pImage == nil) then pImage = FOLDER_NPCS_IMAGES.."/"..SPRITE_TYPE_DETAIL..detailIndex..".png" end

    local lNpcFall = createNPC(pSubType, pImage, pX, pY, pWeight, pLife, pEnergy)

    --- Initialise l'objet
    function lNpcFall.initialize ()
        debugFunctionEnter("npcFall.initialize")

        local detailIndex = getLastSuffix(lNpcFall.type)
        local actionDelaiWithDtFactor = game.getDelaiDifficultyFactor()

        if (lNpcFall.type == SPRITE_TYPE_NPC_FALL1) then
            lNpcFall.speed = 75 * game.getSpeedDifficultyFactor()
            lNpcFall.actionDelai = {
                min = 5 * actionDelaiWithDtFactor,
                max = 10 * actionDelaiWithDtFactor
            }

        elseif (lNpcFall.type == SPRITE_TYPE_NPC_FALL2) then
            lNpcFall.speed = 125 * game.getSpeedDifficultyFactor()
            lNpcFall.actionDelai = {
                min = 4 * actionDelaiWithDtFactor,
                max = 6 * actionDelaiWithDtFactor
            }
        else
            logMessage("lNpcFall.initialize:le type '"..lNpcFall.type .. " est invalide")
            return
        end

        lNpcFall.actionTimer = lNpcFall.actionDelai.max or 1 * actionDelaiWithDtFactor
        lNpcFall.navPath = createNavPath(NAVPATH_TYPE_DOWN, lNpcFall)

        -- ajoute les animations
        -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
        --lNpcFall.addAnimation(ANIM_NAME_MOVE_WALK, FOLDER_NPCS_ANIMS.."/npcFall1/walk", ".png")
        --lNpcFall.playAnimation(ANIM_NAME_MOVE_WALK)

        -- définit les hotSpots pour les collisions
        lNpcFall.createCollisionBox(COLLISION_MODEL_BOX4)

        lNpcFall.updateInitialValues() -- important
    end -- npcFall.initialize

    -- Override des fonctions du sprite
    -- ******************************

    --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lNpcFall.npcDraw = lNpcFall.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcFall.draw ()
    ---- debugFunctionEnterNL("npcFall.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
     lNpcFall.npcDraw()
  end -- npcFall.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lNpcFall.npcDestroy = lNpcFall.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcFall.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("npcFall.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

    -- la destruction effective se fait dans la classe parent
    lNpcFall.npcDestroy()
  end -- npcFall.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcFall.npcUpdate = lNpcFall.update
  function lNpcFall.update (pDt)
    ---- debugFunctionEnterNL("npcFall.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lNpcFall.npcUpdate(pDt)

  end -- npcFall.update

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcFall.npcCollide = lNpcFall.collide
  function lNpcFall.collide ()
    ---- debugFunctionEnterNL("npcFall.collide") -- ATTENTION cet appel peut remplir le log
    local collisionType = lNpcFall.npcCollide(pDt)

    if (not lNpcFall.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    return collisionType
  end -- npcFall.collide
  ]]

    --- le PNJ effectue une action (tire un projectile)
    -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
    lNpcFall.npcAction = lNpcFall.action
    function lNpcFall.action ()
        -- debugFunctionEnter("npc.action")
        lNpcFall.npcAction()

        if (not lNpcFall.checkIfCanMakeAction()) then return end
        local lProjectile
        if (lNpcFall.type == SPRITE_TYPE_NPC_FALL1) then
            -- COMPORTEMENT SPÉCIFIQUE: tire vers le bas
            -- ******************************
            -- rappel:    createProjectile(pType               , pImage                                            ,pX,pY,pLife,pEnergy,pDamages,pSpeed)
            lProjectile = createProjectile(SPRITE_TYPE_PROJ_NPC, FOLDER_PROJECTILES_IMAGES.."/projectile_npc_1.png", 0, 0, 1, 1, 1, 200)
            lProjectile.navPath = createNavPath(NAVPATH_TYPE_DOWN, lProjectile, lNpcFall, nil)

        elseif (lNpcFall.type == SPRITE_TYPE_NPC_FALL2) then
            -- COMPORTEMENT SPÉCIFIQUE: tire vers le bas
            -- ******************************
            -- rappel:    createProjectile(pType               , pImage                                            ,pX,pY,pLife,pEnergy,pDamages,pSpeed)
            lProjectile = createProjectile(SPRITE_TYPE_PROJ_NPC, FOLDER_PROJECTILES_IMAGES.."/projectile_npc_2.png", 0, 0, 1, 1, 2, 200)
            lProjectile.navPath = createNavPath(NAVPATH_TYPE_DOWN, lProjectile, lNpcFall, nil)
        end
    end

    -- initialisation par défaut
    lNpcFall.initialize()

    return lNpcFall
end -- createDoor
