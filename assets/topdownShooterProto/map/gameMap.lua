-- Fonctions pour le map propre du type TOPDOWN_SHOOTER
-- Utilise une forme d'héritage simpSle: gameMap qui inclus map
-- ****************************************

--- Crée un pseudo objet de type gameMap
-- @param pLevel (OPTIONNEL) niveau de jeu associé à la map.
-- @return un pseudo objet gameMap
function createGameMap (pLevel)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("pLevel ", pLevel)

  local lGameMap = createMap (pLevel)

  -- Override des fonctions de la map
  -- ******************************

  -- Charge et initialise  la map
  -- @param pLevel (OPTIONNEL) niveau de jeu associé à la map. Si absent, 1 sera utilisé.
  function lGameMap.initialize (pLevel)
    debugFunctionEnter("gameMap.initialize ", pLevel)
    if (pLevel == nil) then pLevel = 1 end
    lGameMap.mapIsOK = false

    lGameMap.level = pLevel
    -- TODO: extraire le nom depuis le fichier décrivant la map
    lGameMap.names = {
      "The beginning",
      "Welcome in hell"
    }

    -- attributs spécifiques à cette map
    -- ******************************

    -- charge les fichiers liés au niveau de la map
    local filename = FOLDER_MAP_LEVELS.."/level"..pLevel..NO_AUTOLOAD_STRING
    if (love.filesystem.exists(filename..".lua")) then

      -- IMPORTANT: si on a déjà chargé un niveau, efface toutes les entités
      resetEntities()

      -- charge le fichier levelXXX.lua généré par TILED
      -- NOTE: la plus part des infos de la map sont contenues dans ce fichier
      local mapFile = require(filename)
      -- merge la table avec le contenu du fichier
      table.merge(lGameMap, mapFile)
      lGameMap.cols = math.floor(viewport.getWidth() / lGameMap.tilewidth)
      lGameMap.rows = math.floor(viewport.getHeight() / lGameMap.tileheight)
      -- position de départ du joueur centrée et en bas par défaut
      lGameMap.playerStart = {
        col = math.floor(lGameMap.cols / 2),
        line = lGameMap.rows - 1
      }

      filename = FOLDER_MAP_IMAGES_TILES.."/"..lGameMap.tilesets[1].image

      if (love.filesystem.isFile(filename)) then
        lGameMap.type = MAP_TYPE_TILED
        lGameMap.mapIsOK = true
      else
        logMessage("gameMap.initialize:le fichier '"..filename.." n'a pas été trouvé")
      end
    else
      logMessage("gameMap.initialize:le fichier "..filename.." n'a pas été trouvé")
      winGame()
    end -- if (love.filesystem.exists(filename)) then
    return lGameMap.mapIsOK
  end -- gameMap.initialize

  --- Crée et positionne les contenus (objets, PNJs...) dans la map
  -- @param pDt Delta time
  --[[ à décommenter pour des comportements spécifiques
  function lGameMap.update (pDt)
    ---- debugFunctionEnterNL("gameMap.update ", pDt) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end
  ]]

  --- affiche la map
  function lGameMap.draw ()
    ---- debugFunctionEnterNL("gameMap.draw") -- ATTENTION cet appel peut remplir le log
    lGameMap.mapIsOK = true

    local l, c, indexTile, x, y, indexGrid, lgrid, tileSheet, indexLayer, indexTileSheet, quad, w, h, filename
    local mapScale = 1
    indexLayer = 1
    indexTileSheet = 1
    w = lGameMap.tilewidth
    h = lGameMap.tileheight
    lgrid = lGameMap.layers[indexLayer].data
    fileName = FOLDER_MAP_IMAGES_TILES.."/"..lGameMap.tilesets[indexTileSheet].image
    tileSheet = resourceManager.getImage(fileName)
    if (tileSheet ~= nil) then
      -- pour le scrolling, on parcours la map à l'envers
      x = camera.x
      y = camera.y - h -- carte hors de l'écran au départ

      indexGrid = 1
      for l = lGameMap.height, 1, -1 do
        for c = 1, lGameMap.width do
          indexGrid = (l - 1) * lGameMap.width + c
          indexTile = lgrid[indexGrid]
          if (indexTile ~= nil and tonumber(indexTile) > 0) then
            quad = love.graphics.newQuad((indexTile - 1) * w, 0, w, h, tileSheet:getDimensions())
            love.graphics.draw(tileSheet, quad, x, y, 0, mapScale, mapScale)
          end
          x = x + w * mapScale
        end
        x = camera.x
        y = y - h * mapScale
      end
    else
      -- TODO: action quand le tileset introuvable
      logMessage("fichier tileset "..fileName.." introuvable")
      lGameMap.mapIsOK = false
    end
    return lGameMap.mapIsOK
  end -- gameMap.draw

  --- Crée et positionne les contenus (objets, PNJs...) dans la map pour un niveau (de difficulté) donné
  -- @param pLevel (OPTIONNEL) niveau (de difficulté). Si absent, 1 sera utilisée.
  function lGameMap.createLevelContent (pLevel)
    debugFunctionEnter("gameMap.createLevelContent ", pLevel)
    if (pLevel == nil) then pLevel = 1 end
    local enemy
    local cMin, cMax, lMin, lMax, index
    cMin = 2
    lMin = 2
    cMax = lGameMap.width / 2 + 2 -- seule la partie de la largeur de la carte est affichée
    lMax = lGameMap.height - 1
    local enemyTypeCount = 4
    local difficultyFactor = 1.2
    local enemyCount = {}
    lGameMap.mapIsOK = true

    -- nombre d'ennemis de chaque catégorie
    for index = 1, enemyTypeCount do
      local tmpVal = enemyTypeCount * difficultyFactor - index + difficultyFactor
      enemyCount[index] = math.floor(pLevel * tmpVal + tmpVal * difficultyFactor)
      -- enemyCount[index] = 2
      -- debugMessage("createLevelContent: "..enemyCount[index].." PNJ de type "..index)
    end
    if (true) then -- mettre à false pour test uniquement
      -- on génère les enemy en fonction du niveau du joueur
      -- TODO: repartir plus d'ennemis plus coriaces en fin de niveau et les plus faible au début
      for index = 1, enemyCount[1] do
        enemy = createNpcFall(0, 0, 1, 1, 1, nil, SPRITE_TYPE_NPC_FALL1)
        cRand = math.random(cMin, cMax)
        lRand = math.random(lMin, lMax)
        enemy.spawnToMap(cRand, -lRand)
        tmpX, tmpY = lGameMap.mapToPixel(cRand, -lRand)
        -- debugMessage("createLevelContent: "..enemy.type.." at "..cRand, -lRand, tmpX, tmpY)
      end

      for index = 1, enemyCount[2] do
        enemy = createNpcFall(0, 0, 1, 1, 2, nil, SPRITE_TYPE_NPC_FALL2)
        cRand = math.random(cMin, cMax)
        lRand = math.random(lMin, lMax)
        enemy.spawnToMap(cRand, -lRand)
        tmpX, tmpY = lGameMap.mapToPixel(cRand, -lRand)
        -- debugMessage("createLevelContent: "..enemy.type.." at "..cRand, -lRand, tmpX, tmpY)
      end

      for index = 1, enemyCount[3] do
        enemy = createNpcBounce(0, 0, 1, 1, 3)
        cRand = math.random(cMin, cMax)
        lRand = math.random(lMin, lMax)
        enemy.spawnToMap(cRand, -lRand)
        tmpX, tmpY = lGameMap.mapToPixel(cRand, -lRand)
        -- debugMessage("createLevelContent: "..enemy.type.." at "..cRand, -lRand, tmpX, tmpY)
      end

      for index = 1, enemyCount[4] do
        enemy = createNpcFixed(0, 0, 1, 1, 5)
        cRand = math.random(cMin, cMax)
        lRand = math.random(lMin, lMax)
        enemy.spawnToMap(cRand, -lRand)
        tmpX, tmpY = lGameMap.mapToPixel(cRand, -lRand)
        -- debugMessage("createLevelContent: "..enemy.type.." at "..cRand, -lRand, tmpX, tmpY)
      end

      -- le BOSS
      enemy = createNpcBossBounce(0, 0, 1, 1, 50)
      cRand = math.random(cMin, cMax)
      lRand = lMax + 1
      enemy.spawnToMap(cRand, -lRand) -- fin du niveau
      tmpX, tmpY = lGameMap.mapToPixel(cRand, -lRand)
      -- debugMessage("createLevelContent: "..enemy.type.." at "..cRand, -lRand, tmpX, tmpY)
      -- enemy.spawnToMap(2, 1) -- pour test
    else
      -- pour tests
      --enemy = createNpcFixed(0, 0, 1, 1, 3)
      --enemy.spawnToMap(2, 1)
      --enemy = createNpcFall(0, 0, 1, 1, 10, nil, SPRITE_TYPE_NPC_FALL2)
      --enemy.spawnToMap(10, 1)
      --enemy = createNpcBounce(0, 0, 1, 1, 10, nil, SPRITE_TYPE_NPC_BOUNCE1)
      --enemy.spawnToMap(16, 3)
      enemy = createNpcBossBounce(0, 0, 1, 1, 1)
      enemy.spawnToMap(10, 1)
    end
    return lGameMap.mapIsOK
  end -- gameMap.createLevelContent

  -- Fonctions spécifiques à cette map
  -- ******************************

  -- initialisation par défaut
  lGameMap.initialize(pLevel)

  return lGameMap
end -- createGameMap
