- Ce dossier contient les sons du jeu.

- Les sons par défaut qui peuvent être automatiquement chargés pour les animations DOIVENT ÊTRE PLACÉS dans le dossier de chaque animation.

- Les sons par défaut qui peuvent être automatiquement chargés pour les sprites doivent être présents dans ce dossier sous la forme <TYPE_DE _SPRITE><Action>.mp3
 	voici une liste non exhaustives des noms de fichier possibles pouvant être chargé automatiquement. Pour plus d'infos, voir la fonction lSprite.loadDefaultSounds ()
		playerAction.mp3
		playerAction2.mp3
		playerDestroy.mp3
		playerLoseEnergy.mp3
		playerPickup.mp3
		playerWinEnergie.mp3
		npcAction.mp3
		npcAction2.mp3
		npcDestroy.mp3
		npcLoseEnergy.mp3
		npcLoseLife.mp3
		npcPickup.mp3
		npcWinEnergie.mp3
		projectileDestroy.mp3
		projectileLoseLife.mp3
		...

- Si certains fichiers audio sont présents, ils sont automatiquement utilisés par le framework. Pour plus d'infos, voir le fichier main.lua.
  En voici la liste:
		nextLevel.mp3 -- le joueur passe au niveau suivant
		menuChange.mp3 -- navigation dans le menu d'un écran
		menuValid.mp3 -- validation du choix dans le menu d'un écran