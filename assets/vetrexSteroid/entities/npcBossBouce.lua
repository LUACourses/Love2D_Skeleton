-- Entité représentant un vaisseau de type boss qui rebondit sur les bords de l'écran
-- Utilise une forme d'héritage simple: npcBossBounce qui inclus npc qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "npcBossBounce" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_NPC_BOUNCEBOSS = SPRITE_TYPE_NPC_BOSS..NEXT_EXT_MOVEBOUNCE -- un PNJ boss avec déplacement avec rebond, passage au niveau suivant si détruit
SPRITE_TYPE_NPC_BOUNCEBOSS1 = SPRITE_TYPE_NPC_BOUNCEBOSS..LAST_EXT_1 -- le PNJ boss n°1, par défaut rebondi en bas de l'écran

--- Création d'un objet npcBossBounce
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids du npc. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_NPCS_IMAGES.."/npcBossBounce.png" sera utilisée.
-- @param pSubType (OPTIONNEL) sous-type de pnj (voir les constantes SPRITE_TYPE_NPC_FIXED_XXX). Si absent, SPRITE_TYPE_NPC_FIXED1 sera utilisée.

-- @return un pseudo objet npcBossBounce
function createNpcBossBounce (pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createNpcBossBounce ", pX, pY, pWeight, pLife, pEnergy, pImage)
  if (pSubType == nil) then pSubType = SPRITE_TYPE_NPC_BOUNCEBOSS1 end
  local detailIndex = getLastSuffix(pSubType)

  if (pImage == nil) then pImage = FOLDER_NPCS_IMAGES.."/"..SPRITE_TYPE_DETAIL..detailIndex..".ptxt" end

  local lNpcBossBounce = createNPC(pSubType, pImage, pX, pY, pWeight, pLife, pEnergy)

  --- Initialise l'objet
  function lNpcBossBounce.initialize ()
    debugFunctionEnter("npcBossBounce.initialize")

    local detailIndex = getLastSuffix(lNpcBossBounce.type)
    local actionDelaiWithDtFactor = game.getDelaiDifficultyFactor()
    lNpcBossBounce.rotation = -90
    lNpcBossBounce.color = {217, 0, 0, 255}
    lNpcBossBounce.mustDisplayEnergy = false

    lNpcBossBounce.speed = 100 * game.getSpeedDifficultyFactor()
    lNpcBossBounce.mustDestroyedAtBottom = false
    lNpcBossBounce.mustDestroyedAtTop = false
    lNpcBossBounce.mustDestroyedAtLeft = false
    lNpcBossBounce.mustDestroyedAtRight = false
    lNpcBossBounce.mustChangeLevelIfDestroyed = true -- important sinon le niveau ne se finira pas
    lNpcBossBounce.destroyDelay = 3
    lNpcBossBounce.lineWidth = 3
    lNpcBossBounce.actionDelai = {
      min = 8 * actionDelaiWithDtFactor,
      max = 12 * actionDelaiWithDtFactor
    }

    lNpcBossBounce.actionTimer = lNpcBossBounce.actionDelai.max or 1 * actionDelaiWithDtFactor
    if (math.random(1, 2) < 1) then lNpcBossBounce.navPath = createNavPath(NAVPATH_TYPE_RIGHTDOWN, lNpcBossBounce) else lNpcBossBounce.navPath = createNavPath(NAVPATH_TYPE_LEFTDOWN, lNpcBossBounce) end

    -- ajoute les animations
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    --lNpcBossBounce.addAnimation(ANIM_NAME_MOVE_WALK, FOLDER_NPCS_ANIMS.."/npcBossBounce1/walk", ".png")
    --lNpcBossBounce.playAnimation(ANIM_NAME_MOVE_WALK)

    -- définit les hotSpots pour les collisions
    lNpcBossBounce.createCollisionBox(COLLISION_MODEL_BOX4)

    lNpcBossBounce.updateInitialValues() -- important
  end -- npcBossBounce.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lNpcBossBounce.npcDraw = lNpcBossBounce.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcBossBounce.draw ()
    ---- debugFunctionEnterNL("npcBossBounce.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
     lNpcBossBounce.npcDraw()
  end -- npcBossBounce.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lNpcBossBounce.npcDestroy = lNpcBossBounce.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcBossBounce.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("npcBossBounce.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

    -- la destruction effective se fait dans la classe parent
    lNpcBossBounce.npcDestroy()
  end -- npcBossBounce.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcBossBounce.npcUpdate = lNpcBossBounce.update
  function lNpcBossBounce.update (pDt)
    ---- debugFunctionEnterNL("npcBossBounce.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lNpcBossBounce.npcUpdate(pDt)

  end -- npcBossBounce.update

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcBossBounce.npcCollide = lNpcBossBounce.collide
  function lNpcBossBounce.collide ()
    ---- debugFunctionEnterNL("npcBossBounce.collide") -- ATTENTION cet appel peut remplir le log
    local collisionType = lNpcBossBounce.npcCollide(pDt)

    if (not lNpcBossBounce.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    return collisionType
  end -- npcBossBounce.collide
  ]]

  --- le PNJ effectue une action (tire un projectile)
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcBossBounce.npcAction = lNpcBossBounce.action
  function lNpcBossBounce.action ()
    -- debugFunctionEnter("npc.action")
    lNpcBossBounce.npcAction()

    if (not lNpcBossBounce.checkIfCanMakeAction()) then return end
    local lProjectile

    -- COMPORTEMENT SPÉCIFIQUE: le projectile est tiré en rotation autour du boss et en fonction des positions relatives
    -- ******************************
    lProjectile = createProjectile(SPRITE_TYPE_PROJ_NPC, FOLDER_PROJECTILES_IMAGES.."/projectile_npc_3.ptxt", 0, 0, 1   , 1     , 2      , 200)
    lProjectile.navPath = createNavPath(NAVPATH_TYPE_FROMTO_CIRCLE, lProjectile, lNpcBossBounce, player)
    lProjectile.color = {255, 0, 0, 255 }
  end

  -- initialisation par défaut
  lNpcBossBounce.initialize()

  return lNpcBossBounce
end -- createDoor
