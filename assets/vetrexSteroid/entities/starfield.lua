-- Fonctions pour générer un star field
-- ****************************************

-- nombre d'étoiles pour le star field
STARFIELD_STAR_COUNT = 400

--- Crée un pseudo objet de type starField
-- @param pStarCount (OPTIONNEL) nombre d'étoiles à créer de la map (niveau de jeu) associé à la map
-- @param pEntity entité à laquelle est liée l'objet.
-- @return un pseudo objet starField
function createStarField (pStarsCount, pEntity)
    -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createStarField ", pStarsCount, pEntity)
  local lStarField = {}

  -- entité à laquelle est lié l'objet
  lStarField.entity = pEntity
  -- position X
  lStarField.vX = 0
  -- position Y
  lStarField.vY = 0
  -- vitesse X minimale pour une étoile
  lStarField.vXmin = 0
  -- vitesse X maximale pour une étoile
  lStarField.vXmax = 0
  -- vitesse Y minimale pour une étoile
  lStarField.vYmin = 0
  -- vitesse Y maximale pour une étoile
  lStarField.vYmax = 0
  -- position X minimale pour une étoile
  lStarField.minX = 0
  -- position X maximale pour une étoile
  lStarField.maxX = 0
  -- position Y minimale pour une étoile
  lStarField.minY = 0
  -- position X maximale pour une étoile
  lStarField.maxY = 0
  -- taille minimale pour une étoile
  lStarField.minSize = 0
  -- taille maximale pour une étoile
  lStarField.maxSize = 0
  -- Nombre d'étoiles
  lStarField.startCount = pStarsCount
  -- couleur de base des étoiles
  lStarField.baseColor = 0
  -- facteur de vitesse de déplacement liée à celle du joueur. Si absent, 12 sera utilisée. Si 0, le star field ne se déplacera pas avec le joueur
  lStarField.moveWithPlayerSpeedFactor = 0
  -- contenu (étoiles)
  lStarField.content = {}

  --- Initialise l'objet
  -- @param pStartCount (OPTIONNEL) Nombre d'étoiles. Si absent, STARFIELD_STAR_COUNT sera utilisée.
  -- @param pEntity entité à laquelle est liée l'objet.
  -- @param pVX (OPTIONNEL) position X. Si absent, 0 sera utilisée.
  -- @param pVY (OPTIONNEL) position Y. Si absent, 0 sera utilisée.
  -- @param pVXmin (OPTIONNEL) vitesse X minimale pour une étoile. Si absent, 1 sera utilisée.
  -- @param pVXmax (OPTIONNEL) vitesse X maximale pour une étoile. Si absent, 5 sera utilisée.
  -- @param pVYmin (OPTIONNEL) vitesse Y minimale pour une étoile. Si absent, 10 sera utilisée.
  -- @param pVYmax (OPTIONNEL) vitesse Y maximale pour une étoile. Si absent, 50 sera utilisée.
  -- @param pMinX (OPTIONNEL) position X minimale pour une étoile. Si absent, 1 sera utilisée.
  -- @param pMaxX (OPTIONNEL) position X maximale pour une étoile. Si absent, viewport.getWidth() sera utilisée.
  -- @param pMinY (OPTIONNEL) position Y minimale pour une étoile. Si absent, 1 sera utilisée.
  -- @param pMaxY (OPTIONNEL) position X maximale pour une étoile. Si absent, viewport.getHeight() sera utilisée.
  -- @param pMinSize (OPTIONNEL) taille minimale pour une étoile. Si absent, 1 sera utilisée.
  -- @param pMaxSize (OPTIONNEL) taille maximale pour une étoile. Si absent, 3 sera utilisée.
  -- @param pBaseColor (OPTIONNEL) couleur de base des étoiles. Si absent, {255, 255, 255, 255} sera utilisée.
  -- @param pMoveWithPlayerSpeedFactor (OPTIONNEL) facteur de vitesse de déplacement liée à celle du joueur. Si absent, 12 sera utilisée. Si 0, le star field ne se déplacera pas avec le joueur
  function lStarField.initialize (pStartCount, pEntity, pVX, pVY, pVXmin, pVXmax, pVYmin, pVYmax, pMinX, pMaxX, pMinY, pMaxY, pMinSize, pMaxSize, pBaseColor, pMoveWithPlayerSpeedFactor)
    -- debugFunctionEnter("starField.initialize ", pStartCount, pEntity, pVX, pVY, pVXmin, pVXmax, pVYmin, pVYmax, pMinX, pMaxX, pMinY, pMaxY, pMinSize, pMaxSize, pBaseColor, pMoveWithPlayerSpeedFactor)
    if (pVX == nil) then pVX = 0 end
    if (pVY == nil) then pVY = 0 end
    if (pVXmin == nil) then pVXmin = 1 end
    if (pVXmax == nil) then pVXmax = 5 end
    if (pVYmin == nil) then pVYmin = 10 end
    if (pVYmax == nil) then pVYmax = 50 end
    if (pMinX == nil) then pMinX = 1 end
    if (pMaxX == nil) then pMaxX = viewport.getWidth() end
    if (pMinY == nil) then pMinY = 1 end
    if (pMaxY == nil) then pMaxY = viewport.getHeight() end
    if (pMinSize == nil) then pMinSize = 1 end
    if (pMaxSize == nil) then pMaxSize = 3 end
    if (pStartCount == nil) then pStartCount = STARFIELD_STAR_COUNT end
    if (pBaseColor == nil) then pBaseColor = {255, 255, 255, 255} end
    if (pMoveWithPlayerSpeedFactor == nil) then pMoveWithPlayerSpeedFactor = 12 end

    lStarField.vX = pVX
    lStarField.vY = pVY
    lStarField.vXmin = pVXmin
    lStarField.vXmax = pVXmax
    lStarField.vYmin = pVYmin
    lStarField.vYmax = pVYmax
    lStarField.minX = pMinX
    lStarField.maxX = pMaxX
    lStarField.minY = pMinY
    lStarField.maxY = pMaxY
    lStarField.minSize = pMinSize
    lStarField.maxSize = pMaxSize
    lStarField.startCount = pStartCount
    lStarField.baseColor = pBaseColor
    lStarField.moveWithPlayerSpeedFactor = pMoveWithPlayerSpeedFactor
    lStarField.entity = pEntity

    -- Génération du star field
    -- ******************
    local i
    for i = 1, lStarField.startCount do
      local newStar = {
        vX = math.random(lStarField.vXmin, lStarField.vXmax),
        vY = math.random(lStarField.vYmin, lStarField.vYmax),
        size = math.random(lStarField.minSize, lStarField.maxSize),
        x = math.random(lStarField.minX, lStarField.maxX),
        y = math.random(lStarField.minY, lStarField.maxY)
      }
      lStarField.content[i] = newStar
    end -- for
  end -- starField.initialize

  --- Actualise le star field
  -- @param pDt delta time
  function lStarField.update (pDt)
    ---- debugFunctionEnterNL("starField.update ", pDt) -- ATTENTION cet appel peut remplir le log
    local i
    local speedFactor = lStarField.moveWithPlayerSpeedFactor / pDt
    for i = 1, lStarField.startCount do
      local newStar = lStarField.content[i]

      -- déplacement de l'étoile
      newStar.x = newStar.x + newStar.vX * pDt
      newStar.y = newStar.y + newStar.vY * pDt

      -- déplacement du star field avec la position du joueur
      if (lStarField.moveWithPlayerSpeedFactor ~= nil and lStarField.moveWithPlayerSpeedFactor ~= 0) then
        newStar.x = newStar.x + (player.vX / speedFactor)
        newStar.y = newStar.y + (player.vY / speedFactor)
      end

      -- une étoile qui sortent de l'écran réapparaissent de l'autre coté
      if (newStar.x > lStarField.maxX) then
        newStar.x = lStarField.minX
      elseif (newStar.x < lStarField.minX) then
        newStar.x = lStarField.maxX
      end
      if (newStar.y > lStarField.maxY) then
        newStar.y = lStarField.minX
      elseif (newStar.y < lStarField.minY) then
        newStar.y = lStarField.maxY
      end
    end -- for
  end -- starField.update

  --- Dessine le star field
  function lStarField.draw ()
    ---- debugFunctionEnterNL("starField.draw") -- ATTENTION cet appel peut remplir le log
        -- on utilise des alias pour optimiser les accès
    local mathRandom = math.random

    -- Affichage du star field
    -- ******************
    local i

    for i = 1, lStarField.startCount do
      local color = {}
      local newStar = lStarField.content[i]
      color[1] = lStarField.baseColor[1] * mathRandom(0.7, 1)
      color[2] = lStarField.baseColor[2] * mathRandom(0.7, 1)
      color[3] = lStarField.baseColor[3]
      color[4] = lStarField.baseColor[4] * mathRandom(0.7, 1)
      love.graphics.setColor(color)
      love.graphics.circle("fill", newStar.x, newStar.y, newStar.size)
    end -- for
    love.graphics.setColor(255, 255, 255, 255)
  end -- starField.draw

  -- initialisation par défaut
  lStarField.initialize(pStartCount, pEntity)

  return lStarField
end -- createStarField
