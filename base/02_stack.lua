-- Entité représentant une pile de valeurs
-- ****************************************

--- Crée un pseudo objet de type stack
-- @return un pseudo objet stack
function createStack ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createStack")

  local lStack = {}

  --- Initialise l'objet
  -- @param pCount (OPTIONNEL) Nombre de valeurs à créer
  -- @param pValue (OPTIONNEL) Valeur à définir pour chaque élément. Si absent, d'initialisation sera 0.
  function lStack.initialize (pCount, pValue)
    debugFunctionEnter("stack.initialize ", pCount, pValue)
    -- contenu
    lStack.content = {}

    each = iterator(lStack.content)
    if (pValue == nil) then pValue = 0 end
    if (pCount ~= nil and pCount > 0) then
      local i
      for i = 1, pCount do
        lStack.push (pValue)
      end
    end
  end -- stack.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  --- Ajoute une valeur à la fin de la pile
  -- @param pValue valeur à ajouter
  -- @return index de la valeur dans la pile
  function lStack.push (pValue)
    ---- debugFunctionEnterNL("stack.push ", pValue)  -- ATTENTION cet appel peut remplir le log
    local index = #lStack.content
    lStack.content[index + 1] = pValue
    return index
  end -- stack.push

  --- Retourne la dernière valeur ajoutée
  -- @param pIndex valeur à ajouter
  -- @return dernière valeur ajoutée ou nil si la pile est vide
  function lStack.pop ()
    ---- debugFunctionEnterNL("stack.pop")  -- ATTENTION cet appel peut remplir le log
    if (#lStack.content < 1) then return nil end

    local index = #lStack.content
    return lStack.content[index]
  end -- stack.pop

  --- Retourne la valeur de l'élément donné
  -- @param pIndex index de l'élément
  -- @return valeur de l'élément ou nil si l'index est invalide
  function lStack.val (pIndex)
    ---- debugFunctionEnterNL("stack.val ", pIndex) -- ATTENTION cet appel peut remplir le log
    if (lStack.content == nil) then
      -- cas ou la pile est vide et non initialisée
      lStack.initialize(1, pValue)
    end

    if (#lStack.content < pIndex) then return nil end
    return lStack.content[pIndex]
  end -- stack.val

  --- Définit la valeur de l'élément donné
  -- @param pIndex index de l'élément
  -- @param pValue valeur de l'élément
  function lStack.setval (pIndex, pValue)
    ---- debugFunctionEnterNL("stack.setval ", pIndex, pValue) -- ATTENTION cet appel peut remplir le log
    if (lStack.content == nil) then
      -- cas ou la pile est vide et non initialisée
      lStack.initialize(1, pValue)
    end

    if (#lStack.content >= pIndex) then
      lStack.content[pIndex] = pValue
    end
  end -- stack.setval

  --- Définit la valeur de tous les éléments de la pile
  -- @param pValue valeur à définir
  function lStack.reset (pValue)
    ---- debugFunctionEnterNL("stack.reset ", pValue)  -- ATTENTION cet appel peut remplir le log
    if (#lStack.content < 1) then return end

    for element in lStack.each do
      element = pValue
    end
  end -- stack.reset

  --- Ajoute une valeur à tous les éléments de la liste
  -- @param pValue valeur à ajouter. Si absent, 1 sera utilisée.
  function lStack.update (pValue)
    if (pValue == nil) then pValue = 1 end
    ---- debugFunctionEnterNL("stack.update ", pValue) -- ATTENTION cet appel peut remplir le log
    if (#lStack.content < 1) then
      -- cas ou la pile est vide et non initialisée
      lStack.initialize(1, pValue)
    else
      local i
      for i = 1, #lStack.content do
        lStack.content[i] = lStack.content[i] + pValue
      end
    end
  end -- stack.add

  --- Recherche une valeur et retourne la clé associée
  -- @param pValue valeur à rechercher
  -- @return clé de la valeur, nil si non trouvée
  function lStack.find (pValue)
    ---- debugFunctionEnterNL("stack.find ", pValue)  -- ATTENTION cet appel peut remplir le log
    if (#lStack.content < 1) then return nil end

    for key, valeur in pairs(lStack.content) do
      if (valeur == pValue) then return key end
    end
    return nil
  end -- stack.find

  --- recherche une valeur et retourne l'index associé
  -- @param pValue valeur à rechercher
  -- @return index de la valeur, nil si non trouvée
  function lStack.ifind (pValue)
    ---- debugFunctionEnterNL("stack.ifind ", pValue)  -- ATTENTION cet appel peut remplir le log
    if (#lStack.content < 1) then return nil end

    for index, valeur in ipairs(lStack.content) do
      if (valeur == pValue) then return index end
    end
    return nil
  end -- stack.ifind

  -- initialisation par défaut
  lStack.initialize()
  return lStack

end -- createStack