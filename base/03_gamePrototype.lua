-- Entité représentant un prototype de jeu
-- ****************************************

-- NOTE: SOUS-TYPE DE JEU doit être défini dans contants.lua car il est utilisé pour la définition des dossiers et avant l'auto load des modules.

--- Création d'un prototype de jeu
-- @return un pseudo objet prototype
function createPrototype ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createGame")"

  local lPrototype = {}

  -- nombre de niveaux PAR DÉFAUT à atteindre pour gagner le jeu
  lPrototype.levelToWin = 1
  -- nombre de points PAR DÉFAUT à marquer pour passer un niveau
  lPrototype.pointForLevel = 0 -- inutilisé par défaut

  -- texte d'aide PAR DÉFAUT affiché sur l'écran de départ
  lPrototype.helpText = {
    {"Détruisez tous vos ennemis et restez en vie"},
    {"Atteignez le niveau "..lPrototype.levelToWin.." pour gagner"},
    {},
    {"ZQSD ou bien les flèches du clavier pour se déplacer"},
    {"Espace pour tirer"},
    {"Appuyer sur ESC pour quitter"}
  }

  -- coefficient de taille pour les polices utilisée sur les écrans (voir HUD)
  lPrototype.fontSizeFactor = 1.0

  --- Initialise l'objet
  function lPrototype.initialize ()
    debugFunctionEnter("prototype.initialize ")

    love.window.setIcon(love.image.newImageData(FOLDER_IMAGES.."/icon.png"))
  end --prototype.initialize

  -- Fonctions "virtuelles" remplacées dans les "classes enfants"
  -- ******************************

  --- Initialise le prototype
  function lPrototype.startGame ()
    --debugFunctionEnter("prototype.startGame")
    -- rien ici, le code doit être implémenté dans les classes filles
  end -- prototype.startGame

  --- Calcule un facteur de difficulté pour une vitesse (de déplacement) en fonction du niveau du jeu
  function lPrototype.getSpeedDifficultyFactor()
    debugFunctionEnter("prototype.getSpeedDifficultyFactor ")
    return (1 + player.level / lPrototype.levelToWin / 3)
  end -- prototype.getSpeedDifficultyFactor

  --- Calcule un facteur de difficulté pour un délai (de tir) en fonction du niveau du jeu
  function lPrototype.getDelaiDifficultyFactor()
    debugFunctionEnter("prototype.getDelaiDifficultyFactor ")
    return 0.8 + (player.level / lPrototype.levelToWin / 20)
  end -- prototype.getDelaiDifficultyFactor

  return lPrototype
end -- createPrototype
