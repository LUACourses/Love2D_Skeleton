-- Entité gérant les musiques
-- ****************************************
-- TODO: finir les type de ressources non implémentées
-- NOTE: S'il n'est pas explicitement spécifié, le type de resource sera déterminé automatiquement en fonction de l'extension du fichier
RESOURCE_TYPE = "resource_type"
-- fichier audio standard
RESOURCE_TYPE_AUDIO = RESOURCE_TYPE..SEP_LAST.."audio"
-- fichier image standard
RESOURCE_TYPE_IMAGE = RESOURCE_TYPE..SEP_LAST.."image"
-- fichier texte standard
RESOURCE_TYPE_TEXTFILE = RESOURCE_TYPE..SEP_LAST.."textFile"
-- fichier texte contenant une définition similaire à une map
RESOURCE_TYPE_MAPFILE = RESOURCE_TYPE..SEP_LAST.."mapFile"
-- fichier texte contenant une liste de points
RESOURCE_TYPE_POINTFILE = RESOURCE_TYPE..SEP_LAST.."pointFile"
-- une liste de points générés aléatoirement
RESOURCE_TYPE_RANDOMPOINT = RESOURCE_TYPE..SEP_LAST.."randomPoint"
-- ressource indéfinie
RESOURCE_TYPE_UNDEFINED = RESOURCE_TYPE..SEP_LAST.."undefined"
-- ressource introuvable
RESOURCE_TYPE_NOTFOUND = RESOURCE_TYPE..SEP_LAST.."notfound"
-- ressource avec détection automatique du type
RESOURCE_TYPE_AUTO = RESOURCE_TYPE..SEP_LAST.."auto"

-- association entre types de ressource et extensions de fichier
RESOURCE_TYPE_EXT = {
  {
    resType = RESOURCE_TYPE_AUDIO,
    resExt = {"mp3","wav"}
  },
  {
    resType = RESOURCE_TYPE_IMAGE,
    resExt = {"png","gif","jpeg","jpg"}
  },
  {
    resType = RESOURCE_TYPE_TEXTFILE,
    resExt = {"txt","log"}
  },
  {
    resType = RESOURCE_TYPE_MAPFILE,
    resExt = {"mtxt"}
  },
  {
    resType = RESOURCE_TYPE_POINTFILE,
    resExt = {"ptxt"}
  },
  {
    resType = RESOURCE_TYPE_RANDOMPOINT,
    resExt = {"prnd"}
  }
}

--- Crée un pseudo objet de type resourceManager
-- @return un pseudo objet resourceManager
function createResourceManager ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createResourceManager")

  local lRM = {}

  -- resource renvoyée en cas de problème dans le traitement
  local badResource = {
    source = nil,
    mode = nil,
    key = nil,
    type = RESOURCE_TYPE_UNDEFINED,
    width = 0,
    height = 0
  }

  --- Initialise l'objet
  function lRM.initialize ()
    debugFunctionEnter("resourceManager.initialize")

    lRM.content = {}
  end -- resourceManager.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lRM.destroy ()
    debugFunctionEnter("resourceManager.destroy")
    lRM.clean()
    lRM = nil
  end -- resourceManager.

  --- Effectue des nettoyages en quittant le jeu
  function lRM.clean ()
    debugFunctionEnter("resourceManager.clean")
    -- pour le moment rien à faire
  end -- resourceManager.clean

  --- Retourne une ressource de la liste, et l'ajoute si elle n'y figure pas encore
  -- @param pFileName nom complet du fichier resource (relatif au dossier du jeu)
  -- @param pKey (OPTIONNEL) clé associée à ce fichier dans la liste. Si absent, une clé sera calculée à partir du nom de fichier
  -- @param pType type de ressource associée à ce fichier (voir les constante RESOURCE_TYPE_XXX). Si absent, RESOURCE_TYPE_UNDEFINED sera utilisée
  -- @param pMode (OPTIONNEL) mode par défaut à associé à la resource , dépend du type de resource. si absent, la valeur "" sera utilisée.
  -- @return un pseudo objet de type Resource (avec Resource.source = nil et Resource.type = RESOURCE_TYPE_NOTFOUND si le fichier n'a pas été trouvé)
  function lRM.get (pFileName, pKey, pType, pMode)
    -- debugFunctionEnter("resourceManager.add ", pFileName, pKey, pType, pMode)
    if (assertEqual(pFileName, nil, "resourceManager.add:pFileName")) then return end
    if (pMode == nil) then pMode = "" end
    -- type non précisé explicitement, on va essayer de déterminer le type à partir de l'extension de fichier
    if (pType == nil) then
      pType = lRM.getTypeFromExtension(pFileName)
    end
    if (pKey == nil) then pKey = pFileName:sanitize() end

    local newResource = {}

    -- la ressource demandée existe t-elle dans le RM ?
    if (lRM.content[pKey] == nil) then
      -- non, on l'ajoute à la liste
      newResource.mode = pMode
      newResource.key = pKey
      newResource.type = pType
      newResource.width = 0
      newResource.height = 0
      local line, posX, posY
      local minX = 9999
      local maxX = 0
      local minY = 9999
      local maxY = 0
      local vertices = {}

      -- le fichier associé existe-t'il ?
      if (love.filesystem.isFile(pFileName)) then
        -- oui, on charge son contenu
        -- fichier audio
        -- *****************************
        if (pType == RESOURCE_TYPE_AUDIO) then
          newResource.source = love.audio.newSource(pFileName, pMode)

        -- fichier image
        -- *****************************
        elseif (pType == RESOURCE_TYPE_IMAGE) then
          newResource.source = love.graphics.newImage(pFileName)
          newResource.width = newResource.source:getWidth()
          newResource.height = newResource.source:getHeight()

        -- fichier texte
        -- *****************************
        elseif (pType == RESOURCE_TYPE_TEXTFILE) then
          -- pas de traitement spécifique
          newResource.source = readTextFile (pFileName)

        -- fichier texte contenant une définition similaire à une map
        -- *****************************
        elseif (pType == RESOURCE_TYPE_MAPFILE) then
          -- TODO: RESOURCE_TYPE_MAPFILE

        -- fichier texte contenant une liste de points
        -- *****************************
        elseif (pType == RESOURCE_TYPE_POINTFILE) then
          -- on doit lire le fichier car il faut extraire les valeur min et max pour déterminer la taille de "l'image"
          for line in love.filesystem.lines(pFileName) do
            -- on extrait les coordonnées de la ligne du fichier texte
            posX, posY = line:match("([^,]+),([^,]+)")
            posX = tonumber(posX)
            posY = tonumber(posY)
            vertices[#vertices + 1] = posX
            vertices[#vertices + 1] = posY
            -- on définit les min et max
            if (posX > maxX) then
              maxX = posX
            elseif (posX < minX) then
              minX = posX
            end
            if (posY > maxY) then
              maxY = posY
            elseif (posY < minY) then
              minY = posY
            end
          end -- for
          newResource.width = (maxX - minX)
          newResource.height = (maxY - minY)
          newResource.source = vertices

        -- une liste de points générés aléatoirement
        -- *****************************
        elseif (pType == RESOURCE_TYPE_RANDOMPOINT) then
        --[[
          le nom du fichier contient des infos sur les point à générer
          exemple:
          count = 5
          width = 30
          height = 30
        ]]
        local content = readTextFile (pFileName)

        -- converti le contenu du fichier en tableau contenant les variables et leur valeur
        local params = table.parseValues (content)
        if (params == {}) then return badResource end
        local verticeCount = tonumber(params.count)
        if (verticeCount < 1)  then return badResource end
        local width = tonumber(params.width) or 0
        local height = tonumber(params.height) or 0
        newResource.width = width
        newResource.height = height

        --  génère les points aléatoirement
        local i
        for i = 1, verticeCount - 1 do
          posX = math.random (1, width)
          posY = math.random (1, height)
          vertices[#vertices + 1] = posX
          vertices[#vertices + 1] = posY
        end -- for
        -- on rajoute le 1er point pour fermer le polygone
        vertices[#vertices + 1] = vertices[1]
        vertices[#vertices + 1] = vertices[2]

        --[[ résultat insatisfaisant
        local points = {}
        for i = 1, verticeCount do
          posX = math.random (1, width)
          posY = math.random (1, height)
          points[#points + 1] = {posX, posY}
        end -- for
        -- ordonne les points en un polygone convexe
        points = createConvexHull(points)
        -- transforme la table de paires de coordonnées en table de coordonnées
        for i = 1, #points do
          vertices[#vertices + 1] = points[i][1]
          vertices[#vertices + 1] = points[i][2]
        end -- for
        ]]

        newResource.source = vertices

        -- type de ressource non traité
        -- *****************************
        else
          newResource.source = pFileName
        end -- if (pType == RESOURCE_TYPE_AUDIO) then
        lRM.content[pKey] = newResource

      -- ressource introuvable
      else
        newResource = badResource
        newResource.type = RESOURCE_TYPE_NOTFOUND
      end -- if (love.filesystem.isFile(pFileName))
    else
      -- oui, on la retourne
      newResource = lRM.content[pKey]
    end -- if (lRM.content[pKey] == nil) then

    return newResource

  end -- resourceManager.get

  --- Retourne une image de la liste, et l'ajoute si elle n'y figure pas encore
  -- @param pFileName nom complet du fichier resource (relatif au dossier du jeu)
  -- @param pKey (OPTIONNEL) clé associée à ce fichier dans la liste. Si absent, une clé sera calculée à partir du nom de fichier.
  -- @return la source de la ressource, le type de la ressource, sa largeur et sa hauteur
  function lRM.getImage (pFileName, pKey)
    -- debugFunctionEnter("resourceManager.getImage ", pFileName, pKey)
    local resource = lRM.get(pFileName, pKey, RESOURCE_TYPE_IMAGE)
    return resource.source, resource.type, resource.width, resource.height
  end -- resourceManager.getImage

  --- Retourne un son de la liste, et l'ajoute  s'il n'y figure pas encore
  -- @param pFileName nom complet du fichier resource (relatif au dossier du jeu)
  -- @param pMode (OPTIONNEL) le mode de chargement du son. Si absent, "static" sera utilisée.
  -- @param pKey (OPTIONNEL) clé associée à ce fichier dans la liste. Si absent, une clé sera calculée à partir du nom de fichier.
  -- @return la source de la ressource, le type de la ressource
  function lRM.getSound (pFileName, pMode, pKey)
    -- debugFunctionEnter("resourceManager.getSound ", pFileName, pMode, pKey)
    if (pMode == nil) then pMode = "static" end
    local resource = lRM.get(pFileName, pKey, RESOURCE_TYPE_AUDIO, pMode)
    return resource.source, resource.type
  end -- resourceManager.getSound

  --- Retourne un fichier texte de la liste, et l'ajoute s'il n'y figure pas encore
  -- @param pFileName nom complet du fichier resource (relatif au dossier du jeu)
  -- @param pKey (OPTIONNEL) clé associée à ce fichier dans la liste. Si absent, une clé sera calculée à partir du nom de fichier.
  -- @return la source de la ressource, le type de la ressource
  function lRM.getTextFile (pFileName, pKey)
    -- debugFunctionEnter("resourceManager.getTextFile ", pFileName, pKey)
   local resource = lRM.get(pFileName, pKey, RESOURCE_TYPE_TEXTFILE)
   return resource.source, resource.type
  end -- resourceManager.getTextFile

  --- Efface une ressource de la liste
  -- @param pFileName (OPTIONNEL) nom complet du fichier resource (relatif au dossier du jeu)
  -- @param pKey (OPTIONNEL) clé associée à ce fichier dans la liste. Si absent, une clé sera calculée à partir du nom de fichier
  function lRM.remove (pFileName, pKey)
    -- debugFunctionEnter("resourceManager.remove ", pFileName, pKey)
    if ((pFileName == nil or pFileName == "") and (pKey == nil or pKey == "" )) then
      logError("resourceManager.remove: les paramètres pFileName et pKey ne peuvent être vide en même temp")
      return
    end
    if (pKey == nil or pKey == "") then pKey = pFileName:sanitize() end

    lRM.content[pKey] = nil
  end -- resourceManager.remove

  --- Détermine le type d'une ressource en fonction de l'extension de fichier
  -- @param pFileName nom complet du fichier resource
  -- @return le type de ressource (constantes du type RESOURCE_TYPE_XXX)
  function lRM.getTypeFromExtension (pFileName)
    -- debugFunctionEnter("resourceManager.getTypeFromExtension ", pFileName)
    local i, j
    -- extension du fichier
    local fileExt = getFileExtension(pFileName)
    fileExt = string.lower(fileExt)
    local result = RESOURCE_TYPE_UNDEFINED
    -- boucle sur toutes les types et le extensions définies
    for i = 1, #RESOURCE_TYPE_EXT do
      local resType = RESOURCE_TYPE_EXT[i].resType
      local resExt = RESOURCE_TYPE_EXT[i].resExt
      for j = 1, #resExt do
        -- l'extension de fichier correspond-elle à une des extensions de la liste d'un type ?
        if (resExt[j] == fileExt) then
          -- oui, le type trouvé est renvoyé
          result = resType
          return result
        end
      end -- for
    end -- for
    return result
  end -- resourceManager.getTypeFromExtension

  -- initialisation par défaut
  lRM.initialize()
  return lRM
end -- createresourceManager