-- Entité représentant les stats du jeu
-- ****************************************
--- Crée un pseudo objet de type gameStats
-- @param Nom du fichier (sans chemin) où sont enregistrées les données. Si absent, "gameStats.txt" sera utilisée.
-- @return un pseudo objet gameStats
function createGameStats (pFileName)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createGameStats ",pFileName)
  if (pFileName == nil) then pFileName = "gameStats.txt" end
  
  local pGameStats = {}

  -- nom du fichier PAR DEFAUT
  pGameStats.fileName = pFileName
  -- contenu du tableau des scores
  pGameStats.entries = {}

  -- retourne le tableau des scores
  function pGameStats.getScoreEntries()
    return pGameStats.entries
  end -- score.getScoreEntries

  --- lit les stats enregistrées dans le fichier
  function pGameStats.readEntries ()
    debugFunctionEnter("gamestats.readEntries")
    pGameStats.entries = {}
    if love.filesystem.exists(pGameStats.fileName) then
      local file = love.filesystem.newFile(pGameStats.fileName)
      file:open("r")
      for line in file:lines() do
        local entry = {}
        local temp = line:split('*')
        if (#temp == 3) then
          entry.score = temp[1]
          entry.date = temp[2]
          entry.nickname = temp[3]
          pGameStats.entries[#pGameStats.entries + 1] = entry
        end
      end
      file:close()
    end
  end -- gamestats.readEntries

  --- ajoute une entrée au tableau des scores
  -- @param pScore Score.
  -- @param pNickname Nom du joueur.
  function pGameStats.addScore (pScore, pNickname)
    debugFunctionEnter("gamestats.readEntries ", pScore, pNickname)
    local currentDate = os.date("%d-%m-%Y")
    local newEntries = {}
    local i = 1
    if (#pGameStats.entries == 0) then
      table.insert(newEntries, {['score'] = pScore, ['date'] = currentDate, ['nickname'] = pNickname})
    else
      while (i <= #pGameStats.entries and i <= 10) do
        if (tonumber(pGameStats.entries[i].score) < pScore) then
          table.insert(newEntries, {['score'] = pScore, ['date'] = currentDate, ['nickname'] = pNickname})
          break
        else
          table.insert(newEntries, pGameStats.entries[i])
        end
        i = i + 1
      end
      while (i <= #pGameStats.entries + 1 and i < 10) do
        table.insert(newEntries, pGameStats.entries[i])
        i = i + 1
      end
      if (i < 10 and #pGameStats.entries == #newEntries) then
        table.insert(newEntries, {['score'] = pScore, ['date'] = currentDate, ['nickname'] = pNickname})
      end
    end
    pGameStats.entries = nil
    pGameStats.entries = newEntries
  end -- gamestats.readEntries

  -- enregistre le tableau des cores dans le fichier
  function pGameStats.saveScore ()
    debugFunctionEnter("gamestats.saveScore")
    local fileName = pGameStats.fileName
    local entries = ""
    for i = 1, #pGameStats.entries do
      entries = entries .. pGameStats.entries[i].score..'*'..pGameStats.entries[i]['date']..'*'..pGameStats.entries[i]['nickname']..'\n'
    end
    if (string.len(entries) > 0) then
      local result, message
      result, message = love.filesystem.write(fileName, entries)
      if (result) then
        -- OK
      else
        logError("Impossible d'enregistrer les statistiques dans le fichier ".. fileName..".L'erreur suivante est survenue:"..message)
      end
    end
  end -- gamestats.saveScore

  -- Vérifie si un score est un high score
  -- @param pScore Score.
  -- @return true ou false
  function pGameStats.isHighscore (pScore)
    debugFunctionEnter("gamestats.isHighscore ", pScore)
    if (pScore <= 0) then return false end

    if (#pGameStats.entries < 10) then
      return true
    end
    for i = 1, #pGameStats.entries do
      if (tonumber(pGameStats.entries[i].score) < pScore) then
        return true
      end
    end
    return false
  end

  return pGameStats
end