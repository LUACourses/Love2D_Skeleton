-- Entité représentant la zone de jeu
-- ****************************************

--- Crée un pseudo objet de type viewport
-- @return un pseudo objet viewport
function createViewPort ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createViewPort")

  local lViewport = {}

  --- Initialise l'objet
  function lViewport.initialize ()
    debugFunctionEnter("viewport.initialize")

    -- position X minimale
    lViewport.Xmin = 0
    -- position X maximale
    lViewport.Xmax = love.graphics.getWidth()
    -- position Y minimale
    lViewport.Ymin = 0
    -- position Y maximale
    lViewport.Ymax = love.graphics.getHeight()
    -- largeur de caractère
    lViewport.charWidth  = 10 -- par défaut, actualisé lors de l'initialisation du HUD
    -- hauteur de caractère
    lViewport.charHeight = 15 -- par défaut, actualisé lors de l'initialisation du HUD
    -- couleur de fond par défaut
    lViewport.backgroundColor = {0, 0, 50}
    -- couleur de texte par défaut
    lViewport.textColor = {255, 255, 255}
    -- le viewport a t'il déjà été dessiné ?
    lViewport.firstDrawDone = false

    -- cache le curseur de la souris
    love.mouse.setVisible(false)
    -- par défaut confine la souris dans la fenêtre, sauf en mode debug
    if (DEBUG_MODE == 0) then love.mouse.setGrabbed(true) end
  end -- viewport.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  function lViewport.getWidth ()
    return math.floor((lViewport.Xmax - lViewport.Xmin) / DISPLAY_SCALE_X)
  end
  function lViewport.getHeight ()
    return math.floor((lViewport.Ymax - lViewport.Ymin) / DISPLAY_SCALE_Y)
  end

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lViewport.destroy ()
    debugFunctionEnter("viewport.destroy")
    lViewport.clean()
    lViewport = nil
  end -- viewport.destroy

  --- Effectue des nettoyages en quittant le jeu
  function lViewport.clean ()
    debugFunctionEnter("viewport.clean")
    -- affiche le curseur de la souris
    love.mouse.setVisible(true)
  end -- viewport.clean

  --- Dessine l'objet
  function lViewport.draw ()
    ---- debugFunctionEnterNL("viewport.draw") -- ATTENTION cet appel peut remplir le log
    -- utilise la couleur de fond définie pour remplir la zone de jeu
    love.graphics.setColor(lViewport.backgroundColor[1], lViewport.backgroundColor[2], lViewport.backgroundColor[3])
    love.graphics.rectangle("fill", lViewport.Xmin / DISPLAY_SCALE_X, lViewport.Ymin / DISPLAY_SCALE_Y, lViewport.Xmax / DISPLAY_SCALE_X, lViewport.Ymax / DISPLAY_SCALE_Y)
    love.graphics.setColor(lViewport.textColor[1], lViewport.textColor[2], lViewport.textColor[3])
    love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
    lViewport.firstDrawDone = true
  end -- viewport.draw

  -- initialisation par défaut
  lViewport.initialize()
  return lViewport
end -- createViewPort
