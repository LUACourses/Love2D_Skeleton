-- Entité représentant un écran de jeu
-- ****************************************
-- TYPE D'ECRAN
SCREEN_UNDEFINED = "screenUndefined"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screen
function createScreen (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreen ", pName, pBgImage, pMusic)
  if (pName == nil) then pName = SCREEN_UNDEFINED end

  local lScreen = {}
  -- index du sprite dans la table entities.sprites (utile pour son update et sa suppression)
  lScreen.listIndex = 0 -- mis à jour lors de l'ajout entities.sprites, ne pas remettre à 0 dans l'initialize
  -- nom de l'écran
  lScreen.name = pName
  -- l'écran a t'il été initialisé
  lScreen.isInitialized = false
  -- image de fond
  lScreen.bgImage = nil
  -- musique
  lScreen.music = nil
  -- menu associé à l'écran
  lScreen.menu = nil

  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screen.initialize")

    local tempName
    if (lScreen.bgImage == nil) then
      tempName = FOLDER_UI_IMAGES.."/"..lScreen.name..".png"
      lScreen.bgImage = resourceManager.getImage(tempName)
    end

    if (lScreen.music == nil) then
      tempName = FOLDER_MUSICS.."/"..lScreen.name..".mp3"
      lScreen.music = resourceManager.getSound(tempName,"stream")
    end

    debugInfos = ""

    -- uniquement dans les classe filles lScreen.isInitialized = true
  end -- screen.initialize

  --- affiche l'écran
  function lScreen.show ()
    debugFunctionEnter("screen.show")
    if (not lScreen.isInitialized) then lScreen.initialize() end

    -- on switch la musique
    musicManager.pause()
    if (appState.currentScreen ~= nil and appState.currentScreen.music ~= nil) then appState.currentScreen.music:stop() end
    if (lScreen.music ~= nil) then lScreen.music:play() end

    if (lScreen.menu ~= nil) then lScreen.menu.show() end

    appState.currentScreen = lScreen
  end -- screen.show

  --- Actualise l'écran
  -- @param pDt delta time
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screen.update") -- ATTENTION cet appel peut remplir le log
    if (lScreen.menu ~= nil) then lScreen.menu.update(pDt) end
  end -- screen.update

  --- Dessine l'écran
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screen.draw") -- ATTENTION cet appel peut remplir le log
    local color, content, font, textWidth
    if (lScreen.bgImage ~= nil) then HUD.drawImageScaleCenter(lScreen.bgImage) end

    -- AFFICHAGE du message "automatique" (avec timer)
    -- ******************************
    -- note: doit être positionné APRES l'affichage de l'image de fond
    HUD.DisplayAutoHideMessage(6)

    -- fait dans chaque écran car la position du menu varie: if (lScreen.menu ~= nil) then lScreen.menu.draw() end
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screen.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    -- rien ici, tout est fait dans les classes filles
  end -- screen.keypressed

  --- Gestion du "mousepressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screen.mousepressed")
    -- rien ici, tout est fait dans les classes filles
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screen.mousemoved ", pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien ici, tout est fait dans les classes filles
  end -- screen.mousemoved

  --- Affichage de l'aide de l'écran
  -- @param pHelp table contenant les lignes d'aide à afficher.
  -- @param pOffsetX (OPTIONNEL) décalage en X du texte par rapport au centre de l'écran. Si absent,  0 sera utilisée.
  -- @param pOffsetY (OPTIONNEL) décalage en Y du texte par rapport au centre de l'écran. Si absent,  0 sera utilisée.
  -- @param pFont (OPTIONNEL) police d'affichage. Si absent, la police courante sera utilisée.
  -- @param pColor (OPTIONNEL) couleur d'affichage.
  -- @return pOffsetX, pOffsetY mis à jour
  function lScreen.drawHelp (pHelp, pOffsetX, pOffsetY, pFont, pColor)
    ---- debugFunctionEnterNL("screen.drawHelp ", pHelp, pOffsetX, pOffsetY, pFont, pColor) -- ATTENTION cet appel peut remplir le log
    if (pHelp ~= nil and pHelp ~= {}) then
      local i, font, h, content
      if (pFont == nil) then font = love.graphics.getFont() else font = pFont end
      h = font:getHeight(" ") -- alt+255 et non espace
      for i= 1, #pHelp do
        HUD.displayTextScale(pHelp[i], font, POS_CENTER, pOffsetX, pOffsetY, pColor)
        pOffsetY = pOffsetY + h
      end -- for
    end -- if(pHelp ~= nil and pHelp ~= {})
    return pOffsetX, pOffsetY
  end -- screen.HelpDraw

  -- l'initialisation par défaut
  lScreen.initialize(pBgImage, pMusic)

  -- Ajout de l'écran à la liste
  if (entities.screens ~= nil) then
    local screenIndex = #entities.screens + 1
    lScreen.tableIndex = screenIndex
    entities.screens[screenIndex] = lScreen
  end

  return lScreen
end -- createScreen
