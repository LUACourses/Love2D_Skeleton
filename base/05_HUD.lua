-- Entité gérant l'affichage en général
-- ****************************************

-- positions relatives utilisées par la fonction displayText()
POS_TOP_LEFT      = "posTopLeft"
POS_TOP_RIGHT     = "posTopRight"
POS_TOP_CENTER    = "posTopCenter"
POS_CENTER        = "posCenter"
POS_CENTER_LEFT   = "posCenterLeft"
POS_CENTER_RIGHT  = "posCenterRight"
POS_BOTTOM_LEFT   = "posBottomLeft"
POS_BOTTOM_RIGHT  = "posBottomRight"
POS_BOTTOM_CENTER = "posBottomCenter"

-- animation possibles pour les textes utilisées par la fonction animText()
TEXT_ANIM = "textAnim"
TEXT_ANIM_SINUS_CENTER = TEXT_ANIM..SEP_LAST.."sinusCenter"
TEXT_ANIM_SCROLL_SINUS_HORZ = TEXT_ANIM..SEP_LAST.."scrollSinusHorz"
TEXT_ANIM_SCROLL_SINUS_VERT = TEXT_ANIM..SEP_LAST.."scrollSinusVert"
TEXT_ANIM_SCROLL_HORZ = TEXT_ANIM..SEP_LAST.."scrollHorz"

-- type de fonts disponibles
FONT_MENUTITLE = "fontMenutitle"
FONT_MENUCONTENT = "fontMenucontent"
FONT_CONTENT = "fontContent"
FONT_GUI = "fontGui"
FONT_NORMAL = "fontNormal"
FONT_DEBUG = "fontDebug"

--- Crée un pseudo objet de type HUD
-- @return un pseudo objet HUD
function createHUD ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createHUD")

  local lHUD = {}

  --- Initialise l'objet
  function lHUD.initialize ()
    debugMessage("HUD.initialize")
    assertEqualQuit(viewport, nil, "HUD.displayText:viewport", true)

    -- différents timers pour gérer les animations de texte
    lHUD.timersForAnimation = createStack()

    -- polices (fonts) utilisées par les différents affichages
    lHUD.fonts = {}
    -- Note: la taille des polices tient compte de l'échelle de l'écran
    -- police pour les titres dans les menus
    lHUD.fonts[FONT_MENUTITLE] = love.graphics.newFont(FOLDER_FONTS.."/menuTitle.ttf", math.floor(50 / DISPLAY_SCALE_X * game.fontSizeFactor))
    -- police pour le contenu dans les menus
    lHUD.fonts[FONT_MENUCONTENT] = love.graphics.newFont(FOLDER_FONTS.."/menuContent.ttf", math.floor(20 / DISPLAY_SCALE_X * game.fontSizeFactor))
    -- police pour les contenu (par défaut)
    lHUD.fonts[FONT_CONTENT] = love.graphics.newFont(FOLDER_FONTS.."/content.ttf", math.floor(20 / DISPLAY_SCALE_X * game.fontSizeFactor))
    -- police pour les GUI (score, niveau...)
    lHUD.fonts[FONT_GUI] = love.graphics.newFont(FOLDER_FONTS.."/gui.ttf", math.floor(30 / DISPLAY_SCALE_X * game.fontSizeFactor))
    -- police pour les affichages normaux (debug)
    lHUD.fonts[FONT_NORMAL] = love.graphics.newFont(FOLDER_FONTS.."/normal.ttf", math.floor(10 / DISPLAY_SCALE_X * game.fontSizeFactor))
    -- police pour les affichages normaux (debug)
    lHUD.fonts[FONT_DEBUG] = love.graphics.newFont(FOLDER_FONTS.."/normal.ttf", math.floor(15 / DISPLAY_SCALE_X * game.fontSizeFactor))

    -- message à afficher au centre de l'écran de manière automatique
    -- NOTE : il suffit que autoHideMessage.text soit non vide pour qu'il soit affiché pendant la durée autoHideMessage.delai
    lHUD.autoHideMessage = {
      -- texte du message, s'il est vide, rien ne sera affiché, sinon un message sera affiché pendant le timerDelai s
      text = "",
      -- durée en s d'affichage du message  par défaut
      delai = 3,
      -- délai courant d'affichage du message
      timer = 0,
      -- police d'affichage
      font = fontNormal,
      -- couleur
      color = {255, 128, 0, 255}
    }

    love.graphics.setFont(lHUD.fonts[FONT_CONTENT])
    viewport.charWidth = lHUD.fonts[FONT_CONTENT]:getWidth("_")
    viewport.charHeight = lHUD.fonts[FONT_CONTENT]:getHeight("|")

  end -- HUD.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************
  function lHUD.get_fontMenuTitle ()
    return lHUD.fonts[FONT_MENUTITLE]
  end
  function lHUD.set_fontMenuTitle (pValue)
    lHUD.fonts[FONT_MENUTITLE] = pValue
  end
  function lHUD.get_fontMenuContent ()
    return lHUD.fonts[FONT_MENUCONTENT]
  end
  function lHUD.set_fontMenuContent (pValue)
    lHUD.fonts[FONT_MENUCONTENT] = pValue
  end
  function lHUD.get_fontContent ()
    return lHUD.fonts[FONT_CONTENT]
  end
  function lHUD.set_fontContent (pValue)
    lHUD.fonts[FONT_CONTENT] = pValue
  end
  function lHUD.get_fontGUI ()
    return lHUD.fonts[FONT_GUI]
  end
  function lHUD.set_fontGUI (pValue)
    lHUD.fonts[FONT_GUI] = pValue
  end
  function lHUD.get_fontNormal ()
    return lHUD.fonts[FONT_NORMAL]
  end
  function lHUD.set_fontNormal (pValue)
    lHUD.fonts[FONT_NORMAL] = pValue
  end
  function lHUD.get_fontDebug ()
    return lHUD.fonts[FONT_DEBUG]
  end
  function lHUD.set_fontDebug (pValue)
    lHUD.fonts[FONT_DEBUG] = pValue
  end

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lHUD.destroy ()
    debugFunctionEnter("HUD.destroy")
    lHUD.clean()
    lHUD = nil
  end -- HUD.destroy

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function lHUD.clean ()
    debugFunctionEnter("HUD.clean")
    -- pour le moment rien à faire
  end -- HUD.clean

  --- Actualise l'objet (Override)
  -- @param pDt delta time
  function lHUD.update (pDt)
    ---- debugFunctionEnterNL("HUD.update ", pDt) -- ATTENTION cet appel peut remplir le log
    -- actualise TOUS les timers définis
    lHUD.timersForAnimation.update(pDt)
    if (lHUD.autoHideMessage.timer < lHUD.autoHideMessage.delai) then lHUD.autoHideMessage.timer = lHUD.autoHideMessage.timer + pDt end
  end -- HUD.update

  --- Affiche un texte à l'écran en le positionnant relativement dans le viewport
  -- @param pText texte à afficher
  -- @parampFont police à utiliser.
  -- @param pPosition (OPTIONNEL) position relative à l'écran (voir les constantes POS_xxx). Le texte sera centré à de l'écran par défaut
  -- @param pOffsetX (OPTIONNEL) offset à ajouter en X
  -- @param pOffsetY (OPTIONNEL) offset à ajouter en Y
  -- @param pColor (OPTIONNEL) couleur du texte. La couleur blanc sera utilisée. par défaut
  -- @param pTextWidth (OPTIONNEL) largeur du texte. Si absent la valeur sera calculée automatiquement.
  -- @param pTextHeight (OPTIONNEL) hauteur du texte. Si absent la valeur sera calculée automatiquement.
  function lHUD.displayText (pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)
    ---- debugFunctionEnterNL("HUD.displayText ", pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight) -- ATTENTION cet appel.HS peut remplir le log
    love.graphics.setFont(pFont)

    if (pPosition == nil) then pPosition = POS_CENTER end
    if (pOffsetX == nil) then pOffsetX = 0 else pOffsetX = (pOffsetX / DISPLAY_SCALE_X) end
    if (pOffsetY == nil) then pOffsetY = 0 else pOffsetY = (pOffsetY / DISPLAY_SCALE_Y) end
    if (pColor == nil) then pColor = {255, 255, 255, 255} end
    if (pTextWidth == nil) then pTextWidth = pFont:getWidth(pText) else pTextWidth = (pTextWidth / DISPLAY_SCALE_X) end -- note la font tient déjà compte de l'échelle
    if (pTextHeight == nil) then pTextHeight = pFont:getHeight(pText) else pTextHeight = (pTextHeight / DISPLAY_SCALE_Y) end -- note la font tient déjà compte de l'échelle

    love.graphics.setColor(pColor[1], pColor[2], pColor[3], pColor[4])

    local Xmax = viewport.getWidth() - pTextWidth
    local Ymax = viewport.getHeight() - pTextHeight
    local drawX, drawY

    switch (pPosition) {
      [POS_TOP_LEFT]      = function () drawX = pOffsetX           ; drawY = pOffsetY            end,
      [POS_TOP_RIGHT]     = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY            end,
      [POS_TOP_CENTER]    = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY            end,
      [POS_CENTER]        = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax / 2 end,
      [POS_CENTER_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax / 2 end,
      [POS_CENTER_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax / 2 end,
      [POS_BOTTOM_LEFT]   = function () drawX = pOffsetX           ; drawY = pOffsetY + Ymax     end,
      [POS_BOTTOM_RIGHT]  = function () drawX = pOffsetX + Xmax    ; drawY = pOffsetY + Ymax     end,
      [POS_BOTTOM_CENTER] = function () drawX = pOffsetX + Xmax / 2; drawY = pOffsetY + Ymax     end
    }

    -- NOTE: lors de la mise à l'échelle, il se peut que les textes affichés en bordure d'écran sortent. on corrige en les repositionnant dans les limites
    if (drawX > Xmax ) then drawX = Xmax end
    if (drawY > Ymax ) then drawY = Ymax end
    drawX = math.floor(drawX)
    drawY = math.floor(drawY)
    -- debugMessage("drawX, drawY=",drawX, drawY)
    love.graphics.print(pText, drawX, drawY)
    love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
  end -- HUD.displayText

 --- Affiche un texte à l'écran en le positionnant relativement dans le viewport avec mise à l'échelle
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pText texte à afficher
  -- @parampFont police à utiliser
  -- @param pPosition (OPTIONNEL) position relative à l'écran (voir les constantes POS_xxx). Le texte sera centré à de l'écran par défaut
  -- @param pOffsetX (OPTIONNEL) offset à ajouter en X
  -- @param pOffsetY (OPTIONNEL) offset à ajouter en Y
  -- @param pColor (OPTIONNEL) couleur du texte. La couleur blanc sera utilisée. par défaut
  -- @param pTextWidth (OPTIONNEL) largeur du texte. Si absent la valeur sera calculée automatiquement.
  -- @param pTextHeight (OPTIONNEL) hauteur du texte. Si absent la valeur sera calculée automatiquement.
  function lHUD.displayTextScale (pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)
    ---- debugFunctionEnterNL("HUD.displayTextScale ", pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight) -- ATTENTION cet appel peut remplir le log
    love.graphics.setFont(pFont)

    if (pOffsetX == nil) then pOffsetX = 0 else pOffsetX = (pOffsetX / DISPLAY_SCALE_X) end
    if (pOffsetY == nil) then pOffsetY = 0 else pOffsetY = (pOffsetY / DISPLAY_SCALE_Y) end
    if (pTextWidth == nil) then pTextWidth = pFont:getWidth(pText) else pTextWidth = (pTextWidth / DISPLAY_SCALE_X) end -- note la police tient déjà compte de l'échelle
    if (pTextHeight == nil) then pTextHeight = pFont:getHeight(pText) else pTextHeight = (pTextHeight / DISPLAY_SCALE_Y) end -- note la police tient déjà compte de l'échelle

    lHUD.displayText (pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)
  end -- HUD.displayTextScale

  --- Affiche un texte à l'écran en utilisant une animation simple
  -- @param pText texte à afficher
  -- @param pFont police à utiliser
  -- @param pTextAnim (OPTIONNEL) type d'animation à utiliser (voir les constantes TEXT_ANIM_xxx)
  -- @param pOffsetX (OPTIONNEL) offset à ajouter en X. Si absent, sera 0
  -- @param pOffsetY (OPTIONNEL) offset à ajouter en Y. Si absent, sera 0
  -- @param pWidth (OPTIONNEL) largeur de l'animation. Si absent, sera la hauteur de la zone d'affichage
  -- @param pHeight (OPTIONNEL) Hauteur de l'animation. Si absent, sera la largeur de la zone d'affichage
  -- @param pColors (OPTIONNEL) tableau contenant les couleurs des lettres du texte. Si absent, sera blanc
  -- @param pTimerAnimIndex (OPTIONNEL) index du timer gérant l'animation. Si absent, sera 1
  -- @param pSpeed (OPTIONNEL) vitesse de déplacement, peut être négative pour inverser le sens de déplacement. Si absent, 50
  -- @param pXstart (OPTIONNEL) position de départ en X. Si absent, l'animation sera centrée en X
  -- @param pYstart (OPTIONNEL) position de départ en Y. Si absent, l'animation sera centrée en Y
  function lHUD.animText (pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    ---- debugFunctionEnterNL("HUD.animText ", pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed) -- ATTENTION cet appel peut remplir le log
    if (DISPLAY_SCALE_X ~= 1 or DISPLAY_SCALE_Y ~= 1) then
      -- dans cas ou l'échelle de l'affichage n'est pas 1, on affiche le texte sans animation car sinon il sortirait de l'écran
        HUD.displayTextScale(pText, pFont, POS_CENTER, pOffsetX, pOffsetY)
    else
      love.graphics.setFont(pFont)
      local textWidth = pFont:getWidth(pText)
      local textHeight = pFont:getHeight(pText)
      local Xmax = viewport.getWidth()-- valeur max
      local Ymax = viewport.getHeight() -- valeur max

      if (pTextAnim == nil) then pTextAnim = TEXT_ANIM_SINUS_CENTER end
      if (pOffsetX == nil) then pOffsetX = 0 end
      if (pOffsetY == nil) then pOffsetY = 0 end
      if (pWidth == nil) then pWidth = viewport.getWidth() end
      if (pHeight == nil) then pHeight = viewport.getHeight() end
      if (pColors == nil) then pColors = {{255, 255, 255, 255}} end
      if (pTimerAnimIndex == nil) then pTimerAnimIndex = 1 end
      if (pSpeed == nil) then pSpeed = 50 end
      if (pXstart == nil) then pXstart = (Xmax - textWidth) / 2 end -- centré en X
      if (pYstart == nil) then pYstart = (Ymax - textHeight) / 2 end -- centré en Y

      local Xdraw = 0
      local Ydraw = 0
      local colorIndex = 1
      local color, char

      switch (pTextAnim) {
          [TEXT_ANIM_SINUS_CENTER] = function ()
          -- OK TEXT_ANIM_SINUS_CENTER animation du texte avec une sinusoïdale centrée
          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            Ydraw = math.sin((Xdraw + lHUD.timersForAnimation.val(pTimerAnimIndex) * 200) / 50) * pSpeed
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end,
        [TEXT_ANIM_SCROLL_SINUS_VERT] = function ()
          -- OK TEXT_ANIM_SCROLL_SINUS_VERT animation du texte avec un scrolling vertical sinusoïdal centrée
          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            Ydraw = math.sin((lHUD.timersForAnimation.val(pTimerAnimIndex) * 200) / 50) * pSpeed
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end,
        [TEXT_ANIM_SCROLL_SINUS_HORZ] = function ()
          -- OK TEXT_ANIM_SCROLL_SINUS_HORZ animation du texte avec un scrolling horizontal sinusoïdal centrée
          Xdraw = math.sin((lHUD.timersForAnimation.val(pTimerAnimIndex)* 200) / 50) * pSpeed
          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end,
        [TEXT_ANIM_SCROLL_HORZ] = function ()
          -- TEXT_ANIM_SCROLL_HORZ animation du texte avec un scrolling horizontal sinusoïdal centrée
          -- animation du texte avec un scrolling horizontal
          Xdraw = lHUD.timersForAnimation.val(pTimerAnimIndex) * pSpeed
          if (pSpeed > 0 and (pXstart + Xdraw + pOffsetX) >= Xmax) then
            lHUD.timersForAnimation.setval(pTimerAnimIndex, 0)
            Xdraw = lHUD.timersForAnimation.val(pTimerAnimIndex) * pSpeed
          end
          if (pSpeed < 0 and (pXstart + Xdraw + pOffsetX) < pXstart) then
            lHUD.timersForAnimation.setval(pTimerAnimIndex, 0)
            Xdraw = lHUD.timersForAnimation.val(pTimerAnimIndex) * pSpeed
          end

          for c = 1, pText:len() do
            char = string.sub(pText, c, c)
            color = pColors[colorIndex]
            love.graphics.setColor(color[1], color[2], color[3])
            love.graphics.print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY)
            -- print(char, pXstart + Xdraw + pOffsetX, pYstart + Ydraw + pOffsetY, "Xmax ", Xmax)
            Xdraw = Xdraw + pFont:getWidth(char)
            colorIndex = colorIndex + 1
            if colorIndex > #pColors then colorIndex = 1 end
          end
        end
      }
    love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
    end -- if (DISPLAY_SCALE_X ~= 1 or DISPLAY_SCALE_Y ~= 1) then
  end -- HUD.animText

  --- Affiche une image sans mise à l'échelle
  -- @param pImage see love.graphics.draw
  -- @param pX see love.graphics.draw
  -- @param pY see love.graphics.draw
  -- @param pRotation see love.graphics.draw
  -- @param pScaleX see love.graphics.draw
  -- @param pScaleY see love.graphics.draw
  -- @param pOrigineX see love.graphics.draw
  -- @param pOrigineY see love.graphics.draw
  function lHUD.drawImage (pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
    ---- debugFunctionEnterNL("HUD.drawImage ", pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY) -- ATTENTION cet appel peut remplir le log
    if (pImage == nil) then return end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pRotation == nil) then pRotation = 0 end
    if (pScaleX == nil) then pScaleX = 1 end
    if (pScaleY == nil) then pScaleY = 1 end
    if (pOrigineX == nil) then pOrigineX = 0 end
    if (pOrigineY == nil) then pOrigineY = 0 end
    love.graphics.draw(pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
  end -- HUD.drawImage

  --- Affiche une image avec mise à l'échelle
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pImage see love.graphics.draw
  -- @param pX see love.graphics.draw
  -- @param pY see love.graphics.draw
  -- @param pRotation see love.graphics.draw
  -- @param pScaleX see love.graphics.draw
  -- @param pScaleY see love.graphics.draw
  -- @param pOrigineX see love.graphics.draw
  -- @param pOrigineY see love.graphics.draw
  -- @param offsetX (OPTIONNEL) offset en X NON MIS À L'ÉCHELLE
  -- @param offsetY (OPTIONNEL) offset en Y NON MIS À L'ÉCHELLE
  function lHUD.drawImageScale (pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY, offsetX, offsetY)
    ---- debugFunctionEnterNL("HUD.drawImageScale ", pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY, offsetX, offsetY, isCentered) -- ATTENTION cet appel peut remplir le log
    if (pImage == nil) then return end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pRotation == nil) then pRotation = 0 end
    if (pScaleX == nil) then pScaleX = 1 end
    if (pScaleY == nil) then pScaleY = 1 end
    if (pOrigineX == nil) then pOrigineX = 0 end
    if (pOrigineY == nil) then pOrigineY = 0 end
    if (offsetX == nil) then offsetX = 0 end
    if (offsetY == nil) then offsetY = 0 end
    if (isCentered == nil) then isCentered = false end

    pX = math.floor(pX / DISPLAY_SCALE_X)
    pY = math.floor(pY / DISPLAY_SCALE_Y)
    pScaleX = pScaleX / DISPLAY_SCALE_X
    pScaleY = pScaleY / DISPLAY_SCALE_Y
    pOrigineX = math.floor(pOrigineX / DISPLAY_SCALE_X)
    pOrigineY = math.floor(pOrigineY / DISPLAY_SCALE_Y)
    love.graphics.draw(pImage, pX + offsetX, pY + offsetY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
  end -- HUD.drawImageScale

  --- Affiche une image avec mise à l'échelle, centrée sur l'écran
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pImage see love.graphics.draw
  -- @param pRotation see love.graphics.draw
  -- @param pScaleX see love.graphics.draw
  -- @param pScaleY see love.graphics.draw
  -- @param pOrigineX see love.graphics.draw
  -- @param pOrigineY see love.graphics.draw
  function lHUD.drawImageScaleCenter (pImage, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
    ---- debugFunctionEnterNL("HUD.drawImageScaleCenter ", pImage, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY) -- ATTENTION cet appel peut remplir le log
    if (pImage == nil) then return end
    if (pRotation == nil) then pRotation = 0 end
    if (pScaleX == nil) then pScaleX = 1 end
    if (pScaleY == nil) then pScaleY = 1 end
    if (pOrigineX == nil) then pOrigineX = 0 end
    if (pOrigineY == nil) then pOrigineY = 0 end

    pX = (viewport.getWidth()  - pImage:getWidth()) /2
    pY = (viewport.getHeight() - pImage:getHeight()) /2

    pX = math.floor(pX / DISPLAY_SCALE_X)
    pY = math.floor(pY / DISPLAY_SCALE_Y)
    pScaleX = pScaleX / DISPLAY_SCALE_X
    pScaleY = pScaleY / DISPLAY_SCALE_Y
    pOrigineX = math.floor(pOrigineX / DISPLAY_SCALE_X)
    pOrigineY = math.floor(pOrigineY / DISPLAY_SCALE_Y)

    love.graphics.draw(pImage, pX, pY, pRotation, pScaleX, pScaleY, pOrigineX, pOrigineY)
  end -- HUD.drawImageScale

  --- Affiche une rectangle sans mise à l'échelle
  -- @param pMode see love.graphics.rectangle
  -- @param pX see love.graphics.rectangle
  -- @param pY see love.graphics.rectangle
  -- @param pW see love.graphics.rectangle
  -- @param pH see love.graphics.rectangle
  function lHUD.rectangle (pMode, pX, pY, pW, pH)
    ---- debugFunctionEnterNL("HUD.rectangle ", pMode, pX, pY, pW, pH) -- ATTENTION cet appel peut remplir le log
    if (pMode == nil) then pMode = "fill" end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pW == nil) then pW = 1 end
    if (pH == nil) then pH = 1 end
    love.graphics.rectangle(pMode, pX, pY, pW, pH)
  end -- HUD.rectangle

  --- Affiche une rectangle avec mise à l'échelle
  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- @param pMode see love.graphics.rectangle
  -- @param pX see love.graphics.rectangle
  -- @param pY see love.graphics.rectangle
  -- @param pW see love.graphics.rectangle
  -- @param pH see love.graphics.rectangle
  -- @param offsetX (OPTIONNEL) offset en X NON MIS À L'ÉCHELLE
  -- @param offsetY (OPTIONNEL) offset en Y NON MIS À L'ÉCHELLE
  function lHUD.rectangleScale (pMode, pX, pY, pW, pH, offsetX, offsetY)
    ---- debugFunctionEnterNL("HUD.rectangleScale ", pMode, pX, pY, pW, pH, offsetX, offsetY) -- ATTENTION cet appel peut remplir le log
    if (pMode == nil) then pMode = "fill" end
    if (pX == nil) then pX = 0 end
    if (pY == nil) then pY = 0 end
    if (pW == nil) then pW = 1 end
    if (pH == nil) then pH = 1 end
    if (offsetX == nil) then offsetX = 0 end
    if (offsetY == nil) then offsetY = 0 end
    pX = math.floor(pX / DISPLAY_SCALE_X)
    pY = math.floor(pY / DISPLAY_SCALE_Y)
    pW = math.floor(pW / DISPLAY_SCALE_X)
    pH = math.floor(pH / DISPLAY_SCALE_Y)
    love.graphics.rectangle(pMode, pX + offsetX, pY + offsetY, pW, pH)
  end -- HUD.rectangleScale

  --- Affiche automatiquement pendant le délai défini un texte qui a été pré-défini
  -- @param pNewDelai (OPTIONNEL) nouveau délai d'affichage.
  function lHUD.DisplayAutoHideMessage (pNewDelai)
    if (lHUD.autoHideMessage ~= nil and lHUD.autoHideMessage.text ~= nil and lHUD.autoHideMessage.text ~= "") then
      if (pNewDelai ~= nil) then lHUD.autoHideMessage.delai = pNewDelai end
      if (lHUD.autoHideMessage.timer < lHUD.autoHideMessage.delai) then
        -- affiche le message à l'écran
        love.graphics.setFont(lHUD.autoHideMessage.font)
        love.graphics.setColor(lHUD.autoHideMessage.color)
        local w = lHUD.autoHideMessage.font:getWidth(lHUD.autoHideMessage.text)
        local h = lHUD.autoHideMessage.font:getHeight(lHUD.autoHideMessage.text)
        love.graphics.print(lHUD.autoHideMessage.text, (viewport.getWidth() - w) / 2 , (viewport.getHeight() - h) / 2 - 2 * h) -- texte centré
        love.graphics.setColor(255, 255, 255, 255)
      else
        -- réinitialise le délai et efface le message
        lHUD.autoHideMessage.timer = 0
        lHUD.autoHideMessage.text = ""
      end
    end
  end

  -- initialisation par défaut
  lHUD.initialize()
  return lHUD
end -- createHUD