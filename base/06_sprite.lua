-- Entité représentant un sprite
-- Utilise une forme d'héritage simple: sprite qui hérite de class
-- ****************************************

-- ÉTAT POSSIBLES
-- ATTENTION l'ajout ou la modification de ses valeurs peut entrainer des modifications dans les fonctions spritecheckIfCanXXX
-- étas de base pour le gameplay
SPRITE_STATUS = "spriteStatus"
SPRITE_STATUS_NORMAL = SPRITE_STATUS..SEP_LAST.."normal"
SPRITE_STATUS_DESTROYING = SPRITE_STATUS..SEP_LAST.."destroying"
SPRITE_STATUS_DESTROYED = SPRITE_STATUS..SEP_LAST.."destroyed"
SPRITE_STATUS_SLEEPING = SPRITE_STATUS..SEP_LAST.."sleeping"
SPRITE_STATUS_ANIM_RUNNING = SPRITE_STATUS..SEP_LAST.."animRunning" -- status particulier pendant que le sprite est en cours d'animation

-- "SUPER POUVOIRS" POSSIBLES
SPRITE_STATUS_INVISIBLE = SPRITE_STATUS..SEP_LAST.."invisible" -- non affiché
SPRITE_STATUS_INVINCIBLE = SPRITE_STATUS..SEP_LAST.."invincible" -- invincible (ne peut être touché)
SPRITE_STATUS_DEADLY = SPRITE_STATUS..SEP_LAST.."deadly" -- mortel (détruit tout ce qu'il touche sans tenir compte d'éventuel bouclier)
SPRITE_STATUS_FREEZE = SPRITE_STATUS..SEP_LAST.."freeze" -- immobile (ne se déplace plus sauf avec la caméra)

-- TYPE DE SPRITE
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
-- NOTE: les autres types de sprites sont dans leur fichier de classe respectif
SPRITE_TYPE = "spriteType"

 -- note: les constantes suivantes sont utilisées dans defineAssetsFolders pour le dossier et dans sprite.initialize pour le nom de fichier des assets associés (sans le SEP_FIRST)
SPRITE_EXT_CHARACTER = SEP_FIRST.."character"  -- note: utilisé pour identifier un sprite de type personnage
SPRITE_EXT_NPC = SEP_FIRST.."npc" -- note: utilisé pour identifier un sprite de type pnj
SPRITE_EXT_PROJ = SEP_FIRST.."projectile" -- note: utilisé pour identifier un sprite de type projectile
SPRITE_EXT_ITEM = SEP_FIRST.."item" -- note: utilisé pour identifier un sprite de type élément de décor
SPRITE_EXT_EFFECT = SEP_FIRST.."effect" -- note: utilisé pour identifier un sprite de type effect
SPRITE_EXT_BOSS = SEP_FIRST.."boss"  -- note: utilisé pour identifier un sprite de type boss
SPRITE_EXT_ELT = SEP_FIRST.."elt" -- note: utilisé pour identifier un sprite de type élément de décors
SPRITE_EXT_UNDEFINED = SEP_FIRST.."undefined"

SPRITE_TYPE_UNDEFINED = SPRITE_TYPE..SPRITE_EXT_UNDEFINED

-- DIRECTION DE DÉPLACEMENT
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
-- TODO: TESTER les déplacements top et bottom
SPRITE_MOVE = "spriteMove"
SPRITE_MOVE_NONE = SPRITE_MOVE..LAST_EXT_NONE
SPRITE_MOVE_LEFT = SPRITE_MOVE..LAST_EXT_LEFT
SPRITE_MOVE_RIGHT = SPRITE_MOVE..LAST_EXT_RIGHT
SPRITE_MOVE_UP = SPRITE_MOVE..LAST_EXT_UP
SPRITE_MOVE_DOWN = SPRITE_MOVE..LAST_EXT_DOWN

-- TYPE DE HOTSPOTS
HOTSPOT_TYPE = "hotspotType"
HOTSPOT_TYPE_TOP = HOTSPOT_TYPE..LAST_EXT_TOP
HOTSPOT_TYPE_RIGHT = HOTSPOT_TYPE..LAST_EXT_RIGHT
HOTSPOT_TYPE_BOTTOM = HOTSPOT_TYPE..LAST_EXT_BOTTOM
HOTSPOT_TYPE_LEFT = HOTSPOT_TYPE..LAST_EXT_LEFT
HOTSPOT_TYPE_UNIQUE = HOTSPOT_TYPE..SEP_LAST..LAST_EXT_UNIQ

-- MODÈLE DE COLLISION (IE. COMBINAISONS DE HOTSPOTS)
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
-- TODO: finir d'intégrer les hotspots
COLLISION_MODEL = "collisionModel"
COLLISION_MODEL_BOX4 = COLLISION_MODEL..SEP_LAST.."box4"
COLLISION_MODEL_BOX6 = COLLISION_MODEL..SEP_LAST.."box6"
COLLISION_MODEL_POINT = COLLISION_MODEL..SEP_LAST.."point"
COLLISION_MODEL_NONE = COLLISION_MODEL..SEP_LAST.."none"

-- TYPE DE COLLISION
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
SPRITE_COLLISION = "spriteCollision"
SPRITE_COLLISION_NONE = SPRITE_COLLISION..LAST_EXT_NONE
SPRITE_COLLISION_TILE = SPRITE_COLLISION..NEXT_EXT_TILE
SPRITE_COLLISION_LIMIT = SPRITE_COLLISION..NEXT_EXT_LIMIT
SPRITE_COLLISION_NPC = SPRITE_COLLISION..NEXT_EXT_NPC
SPRITE_COLLISION_PLAYER = SPRITE_COLLISION..NEXT_EXT_PLAYER
SPRITE_COLLISION_PROJ = SPRITE_COLLISION..NEXT_EXT_PROJ
SPRITE_COLLISION_OBJECT = SPRITE_COLLISION..NEXT_EXT_ITEM

SPRITE_COLLISION_TILE_SOLID = SPRITE_COLLISION_TILE..NEXT_EXT_SOLID
SPRITE_COLLISION_TILE_JUMPTHROUGH = SPRITE_COLLISION_TILE..NEXT_EXT_JUMPTHROUGH
SPRITE_COLLISION_TILE_WATER = SPRITE_COLLISION_TILE..NEXT_EXT_WATER
SPRITE_COLLISION_TILE_DAMAGE = SPRITE_COLLISION_TILE..NEXT_EXT_DAMAGE
SPRITE_COLLISION_TILE_DEADLY = SPRITE_COLLISION_TILE..NEXT_EXT_DEADLY

SPRITE_COLLISION_LIMIT_LEFT = SPRITE_COLLISION_LIMIT..LAST_EXT_LEFT
SPRITE_COLLISION_LIMIT_RIGHT = SPRITE_COLLISION_LIMIT..LAST_EXT_RIGHT
SPRITE_COLLISION_LIMIT_TOP = SPRITE_COLLISION_LIMIT..LAST_EXT_TOP
SPRITE_COLLISION_LIMIT_BOTTOM = SPRITE_COLLISION_LIMIT..LAST_EXT_BOTTOM

SPRITE_COLLISION_TILE_SOLID_LEFT = SPRITE_COLLISION_TILE_SOLID..LAST_EXT_LEFT
SPRITE_COLLISION_TILE_SOLID_RIGHT = SPRITE_COLLISION_TILE_SOLID..LAST_EXT_RIGHT
SPRITE_COLLISION_TILE_SOLID_TOP = SPRITE_COLLISION_TILE_SOLID..LAST_EXT_TOP
SPRITE_COLLISION_TILE_SOLID_BOTTOM = SPRITE_COLLISION_TILE_SOLID..LAST_EXT_BOTTOM
SPRITE_COLLISION_TILE_JUMPTHROUGH_BOTTOM = SPRITE_COLLISION_TILE_JUMPTHROUGH..LAST_EXT_BOTTOM

-- échelle de dessin par défaut pour les sprites
SPRITE_SCALE = 1

--- Crée un pseudo objet de type sprite
-- Note: certaines valeurs sont divisées par l'échelle de l'écran
-- le sprite est automatiquement ajouté à la variable entities.sprites
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur "" sera utilisée.
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur 0 sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur 0 sera utilisée.
-- @param pvX (OPTIONNEL) vélocité en X. Si absent la valeur 0 sera utilisée.
-- @param pvY (OPTIONNEL) vélocité en Y. Si absent la valeur 0 sera utilisée.
-- @param pType (OPTIONNEL) type de sprite (voir les constantes SPRITE_TYPE_xxx). Si absent la valeur SPRITE_TYPE_UNDEFINED sera utilisée.
-- @param pWeight (OPTIONNEL) poids. Si absent la valeur 0 sera utilisée. (pas de chute par défaut)
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent, 1 sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur 1 sera utilisée.
-- @param pPoints (OPTIONNEL) Nombre de point que rapporte le sprite lors de sa destruction. Si absent, une valeur dépendant du nombre de vies initial sera utilisée.
-- @param pCanCollideWithTile (OPTIONNEL) true pour que le sprite puisse collisioner avec les tuiles de la carte. Si absent, true sera utilisée.
-- @param pVelocityMax (OPTIONNEL) vélocité maximale. Si absent la valeur 0 sera utilisée.
-- @param pVelocityJump (OPTIONNEL) vélocité pour un saut. Si absent la valeur 0 sera utilisée.
-- @param pGravity (OPTIONNEL) chute due à la gravité. Si absent la valeur 0 sera utilisée.
-- @param pCanMakeAction (OPTIONNEL) true pour que le sprite puisse faire une action automatique (tirer un projectile). Si absent, false sera utilisée.
-- @param pStatus (OPTIONNEL) status. Si absent, SPRITE_STATUS_NORMAL sera utilisée.
-- @param pMustDestroyedAtBorder (OPTIONNEL) true pour que le sprite soit détruit en touchant le bas de l'écran. Si absent, true sera utilisée.
-- @param pMustDestroyedOnCollision (OPTIONNEL) true pour détruire le projectile s'il touche une élément de la map (tuile solide ou décors). Si absent, true sera utilisée.
-- @param pTimeToLive (OPTIONNEL) durée de vie. Si absent, 0 sera utilisée. 0 indique une durée de vie illimitée.
-- @param pSpeed (OPTIONNEL) vitesse. Si absent, 0 sera utilisée.
-- @param pMustRespawnIfDestroyed (OPTIONNEL) true pour re-spawner une fois si détruit. Si absent, false sera utilisée.
-- @return le pseudo objet sprite créé et l'index du sprite dans la liste
function createSprite (pImage, pX, pY, pvX, pvY, pType, pWeight, pLife, pEnergy, pPoints, pCanCollideWithTile, pVelocityMax, pVelocityJump, pGravity, pCanMakeAction, pStatus, pMustDestroyedAtBorder, pTimeToLive, pMustDestroyedOnCollision, pSpeed, pMustRespawnIfDestroyed)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createSprite ", pImage, pX, pY, pvX, pvY, pType, pWeight, pLife, pEnergy, pPoints, pCanCollideWithTile, pVelocityMax, pVelocityJump, pGravity, pCanMakeAction, pStatus, pMustDestroyedAtBorder, pTimeToLive, pMustDestroyedOnCollision, pSpeed, pMustRespawnIfDestroyed)
  assertEqualQuit(viewport, nil, "createSprite:viewport", true)

  if (pImage == nil) then pImage = "" end
  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pvX == nil) then pvX = 0 end
  if (pvY == nil) then pvY = 0 end
  if (pWeight == nil) then pWeight = 0 end  -- pas de chute par défaut
  if (pType == nil) then pType = SPRITE_TYPE_UNDEFINED end
  if (pLife == nil) then pLife = 1 end
  if (pEnergy == nil) then pEnergy = 1 end
  if (pPoints == nil) then pPoints = (pEnergy * 5 + pLife * 10 + 5) end -- un re calcul sera fait dans updateInitialValues
  if (pCanCollideWithTile == nil) then pCanCollideWithTile = true end
  if (pVelocityMax == nil) then pVelocityMax = 0 end
  if (pVelocityJump == nil) then pVelocityJump = 0 end
  if (pGravity == nil) then pGravity = settings.gravity end
  if (pCanMakeAction == nil) then pCanMakeAction = false end
  if (pStatus == nil) then pStatus = SPRITE_STATUS_NORMAL end
  if (pMustDestroyedAtBorder == nil) then pMustDestroyedAtBorder = true end
  if (pMustDestroyedOnCollision == nil) then pMustDestroyedOnCollision = true end
  if (pTimeToLive == nil) then pTimeToLive = 0 end
  if (pSpeed == nil) then pSpeed = 0 end
  if (pMustRespawnIfDestroyed == nil) then pMustRespawnIfDestroyed = false end

  local lSprite = {}

  -- Note: certaines valeurs sont divisées par l'échelle de l'écran
  -- index du sprite dans la table entities.sprites (utile pour son update et sa suppression)
  lSprite.listIndex = 0 -- mis à jour lors de l'ajout entities.sprites, ne pas remettre à 0 dans l'initialize
  -- flag pour déterminer si le sprite a été actualisé au moins une fois (utilisé par la CCD)
  lSprite.firstUpdateDone = false
  -- position en X dans le viewport
  lSprite.x = pX
  -- position en Y dans le viewport
  lSprite.y = pY
  -- type de sprite différencier le personnage principal, les ennemis, les objets…
  lSprite.type = pType
  -- status
  lSprite.status = pStatus

  -- dimensions du sprite en fonction de sa 1ere image ou de la taille des tuiles de la map ou fixée a 32 px
  if (map ~= nil and map.tilewidth ~= nil and map.tileheight ~= nil) then
    pWidth = map.tilewidth
    pHght = map.tileheight
 else
    pWidth = 32
    pHeight = 32
  end

  -- image principale (hors animations) et ses propriétés
  lSprite.resource = resourceManager.get(pImage)  -- type de ressource déterminé automatiquement par l'extension de fichier
  lSprite.image = lSprite.resource.source
  lSprite.imageType = lSprite.resource.type
  pWidth = lSprite.resource.width
  pHeight = lSprite.resource.height

  -- largeur (mise à l'échelle)
  lSprite.width = pWidth * SPRITE_SCALE
  -- hauteur (mise à l'échelle)
  lSprite.height = pHeight * SPRITE_SCALE
  -- origine en X (décalage vers la droite) pour l'affichage du sprite
  lSprite.rotationCenterX = lSprite.width / 2
  -- origine en Y (décalage vers le bas) pour l'affichage du sprite
  lSprite.rotationCenterY = lSprite.height / 2
  -- dernière position actualisées (utilisée par le CCD)
  lSprite.lastPosition = {}
  -- rotation en degrés (la modification de la vitesse)
  lSprite.rotation = 0
    -- vitesse courante (modifiée par l'environnement ou les actions du joueur)
  lSprite.speed = pSpeed
  -- vitesse en X
  lSprite.vX = pvX
  -- vitesse en Y
  lSprite.vY = pvY
  -- vélocité maximale
  -- NOTE: si la valeur est nulle, le sprite ne pourra pas se déplacer
  lSprite.velocityMax = math.floor(pVelocityMax / DISPLAY_SCALE_X)
  -- vélocité pour un saut
  lSprite.velocityJump = math.floor(pVelocityJump / DISPLAY_SCALE_Y)
  -- direction du déplacement (PNJ)
  lSprite.direction = SPRITE_MOVE_RIGHT

  -- poids (influence la gravité et la résistance de l'environnement)
  -- NOTE: si la valeur est nulle, le sprite ne sera pas soumis à la gravité et au freinage
  lSprite.weight = pWeight
  -- gravité courante (modifiée par l'environnement ou les actions du joueur)
  lSprite.gravity = math.floor(pGravity / DISPLAY_SCALE_Y)
  -- modularité de la gravité
  lSprite.gravityModularity = 0
  -- timer pour gérer la granularité de la gravité
  lSprite.timerDrop = 0
  -- dernière position actualisée
  lSprite.lastPosition = {}
  -- couleur d'affichage utilisée si le sprite n'a pas d'image principale ou d'animation
  lSprite.color = {128, 128, 128, 255}

  -- niveau d'énergie courant avant la perte d'une vie
  lSprite.energy = pEnergy
  -- faut il afficher le niveau d'énergie ?
  lSprite.mustDisplayEnergy = true
  -- nombre de vies avant destruction
  lSprite.life = pLife
  -- nombre de point (ajoutés au score quand le sprite est détruit)
  lSprite.points = pPoints
  -- liste des points de vérification des collisions (hotspot)
  lSprite.hotSpots = {}
  -- doit se déplacer avec la caméra ?
  lSprite.mustMoveWithCamera = true
  -- doit il être détruit s'il touche le bords de l'écran ?
  lSprite.mustDestroyedAtBottom = pMustDestroyedAtBorder
  lSprite.mustDestroyedAtTop = pMustDestroyedAtBorder
  lSprite.mustDestroyedAtLeft = pMustDestroyedAtBorder
  lSprite.mustDestroyedAtRight = pMustDestroyedAtBorder
  -- un effet visuel doit il être joué si détruit ?
  lSprite.mustDestroyedWithFX = true
  -- un effet sonore doit il être joué si détruit ?
  lSprite.mustDestroyedWithSound = true
  -- le sprite doit-il être re-spawné une fois si détruit ?
  lSprite.mustRespawnIfDestroyed = pMustRespawnIfDestroyed
   -- la destruction du sprite implique-elle le passage au niveau suivant ?
  lSprite.mustChangeLevelIfDestroyed = false
  -- doit il être détruit s'il touche une élément de la map (tuile solide ou décors) ?
  lSprite.mustDestroyedOnCollision = pMustDestroyedOnCollision
  -- Est-il sur le sol ?
  lSprite.isStanding = true
  -- Est-il sur en train de sauter ?
  lSprite.isJumping = false
  -- Est-il sur en train de grimper ?
  lSprite.isClimbing = false
    -- Est-il sur en train de tomber ?
  lSprite.isFalling = false
  -- peut il sauter  ?
  lSprite.canJump = false
  -- peut il faire une action automatique ?
  lSprite.canMakeAction = true
  -- peut-il collisioner avec les tuiles de la carte ?
  -- NOTE: peut être consommateur de ressources avec la CDD activée et un grand nombre de sprites
  lSprite.canCollideWithTile = pCanCollideWithTile
  -- angle de tri courant
  lSprite.actionAngle = 0
  -- délai entre 2 tirs successifs
  lSprite.actionDelai = {
    min = 1,
    max = 2
  }
  -- trajectoire liée au sprite
  lSprite.navPath = nil
  -- liste des sons liés au sprite
  lSprite.sounds = {}
  -- liste des animations liées au sprite
  lSprite.animations = {}
  -- animation en cours de lecture
  lSprite.currentAnimation = ANIM_NAME_NONE
  -- timer pour l'action en cours
  lSprite.actionTimer = math.random(lSprite.actionDelai.min, lSprite.actionDelai.max)
  -- nom du dernier son joué
  lSprite.lastSound = nil
  -- dernière position en "standing" (permet d'évaluer les hauteurs de chute ou de saut)
  lSprite.lastStanding = {x = 0, y = 0}
  -- valeur limite en Y pour prendre en compte une chute
  -- NOTE: ce n'est pas une différence en Y stricte mais plutôt une valeur d'accélération.
  -- Le son "Fall" sera joué et les dommages fallDamage seront appliqué si une chute est au dela de cette limite
  lSprite.fallLimit = 0
  -- dommages appliqués en cas de chute au dela de la valeur fallLimit
  lSprite.fallDamage = 0
  -- délai avant la destruction effective.
  -- Permet de laisser du temps entre l'affichage de l'explosion et la suppression (utile pour les boss et le joueur).
  -- note: Si =-1, ce delai ne sera pas pris en compte (utilisé par exemple quand il y a une animation de destruction du sprite)
  lSprite.destroyDelay = 0
  -- durée de vie du sprite. 0 pour illimité
  lSprite.timeToLive = pTimeToLive
  -- épaisseur du trait dans le cas ou le sprite est dessiné à partir de point (voir ressourceManager)
  lSprite.lineWidth = 2

  -- mémorise certains des paramètres initiaux du sprite
  -- cela permet de retrouver ces valeurs car elle peuvent être modifiées pendant le jeu
  lSprite.initialValues = {}

  --- Initialise le sprite
  function lSprite.initialize ()
    debugFunctionEnter("sprite.initialize")
    lSprite.updateLastPosition()
    lSprite.loadDefaultSounds()

    -- modèle de collision par défaut
    lSprite.createCollisionBox(COLLISION_MODEL_POINT)

    lSprite.updateInitialValues() -- important
  end -- sprite.initialize

  -- mémorise certains des paramètres initiaux du sprite
  function lSprite.updateInitialValues ()
    -- debugFunctionEnterNL("sprite.updateInitialValues") -- ATTENTION  cet appel peut remplir le log

    lSprite.points = (lSprite.energy * 5 + lSprite.life * 10 + 5)

    -- on ne mémorise que les valeurs susceptible de changer et devant éventuellement être réinitialisées durant le jeu
    lSprite.initialValues = {
      x = lSprite.x,
      y = lSprite.y,
      status = lSprite.status,
      type = lSprite.type,
      speed = lSprite.speed,
      vX = lSprite.vX,
      vY = lSprite.vY,
      width = lSprite.width,
      height = lSprite.height,
      rotation = lSprite.rotation,
      velocityMax = lSprite.velocityMax,
      velocityJump = lSprite.velocityJump,
      direction = lSprite.direction,
      weight = lSprite.weight,
      gravity  = lSprite.gravity,
      energy = lSprite.energy,
      life = lSprite.life,
      points = lSprite.points,
      mustDestroyedWithFX = lSprite.mustDestroyedWithFX,
      mustDestroyedWithSound = lSprite.mustDestroyedWithSound,
      mustRespawnIfDestroyed = lSprite.mustRespawnIfDestroyed,
      mustDestroyedOnCollision = lSprite.mustDestroyedOnCollision,
      canCollideWithTile = lSprite.canCollideWithTile,
      canJump = lSprite.canJump,
      canMakeAction = lSprite.canMakeAction,
      mustMoveWithCamera = lSprite.mustMoveWithCamera,
      canCollideWithTile = lSprite.canCollideWithTile,
      fallDamage = lSprite.fallDamage,
      timeToLive = lSprite.timeToLive
    }
  end -- sprite.updateInitialValues

  --- Permet de récupérer le contenu de la table en utilisant print() ou tostring()
  -- @return chaîne décrivant le sprite
  function lSprite.toString ()
    ---- debugFunctionEnterNL("sprite.toString") -- ATTENTION  cet appel peut remplir le log
    return "type="..lSprite.type
      .." listIndex="..lSprite.listIndex
      .." status="..lSprite.status
      .." x="..lSprite.x
      .." y="..lSprite.y
      .." vX="..lSprite.vX
      .." vY="..lSprite.vY
      .." width="..lSprite.width
      .." height="..lSprite.height
      .." rotation="..lSprite.rotation
      .." energy="..lSprite.energy
      .." life="..lSprite.life
      .." velocityMax="..lSprite.velocityMax
      .." velocityJump="..lSprite.velocityJump
      .." direction="..lSprite.direction
      .." weight="..lSprite.weight
      .." gravity ="..lSprite.gravity
      .." isStanding="..tostring(lSprite.isStanding)
      .." isJumping="..tostring(lSprite.isJumping)
      .." canJump="..tostring(lSprite.canJump)
      .." canMakeAction="..tostring(lSprite.canMakeAction)
      .." canCollideWithTile="..tostring(lSprite.canCollideWithTile)
      .." actionAngle="..lSprite.actionAngle
      .." mustDestroyedOnCollision="..tostring(lSprite.mustDestroyedOnCollision)
      .." timeToLive="..lSprite.timeToLive
      .." speed="..lSprite.speed
  end -- sprite.toString

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  --- retourne l'identifiant (simple)
  -- @return string
  function lSprite.getID ()
    return lSprite.type.."#"..lSprite.listIndex
  end -- sprite.getID

  --- actualise la dernière position en fonction des positions lSprite.x et lSprite.y
  function lSprite.updateLastPosition ()
    lSprite.lastPosition.x = lSprite.x
    lSprite.lastPosition.y = lSprite.y
    lSprite.lastPosition.vX = lSprite.vX
    lSprite.lastPosition.vY = lSprite.vY
    lSprite.lastPosition.direction = lSprite.direction
  end -- sprite.updateLastPosition

  --- mémorise la dernière position en standing
  -- @param pIsStanding (OPTIONNEL) nouvelle valeur pour l'attribut isStanding. Si absent, l'attribut ne sera pas modifié.
  function lSprite.updateLastStanding (pIsStanding)
    if (pIsStanding ~= nil) then
      lSprite.isStanding = pIsStanding
      if (pIsStanding) then
        lSprite.isJumping = false
        lSprite.isClimbing = false
        lSprite.isFalling = false
      end
    end
    lSprite.lastStanding.x = lSprite.x
    lSprite.lastStanding.y = lSprite.y
  end

  -- Autres fonctions
  -- ******************************

  --- détruit le sprite en jouant éventuellement une animation ANIM_NAME_STATUS_DESTROY lors de sa destruction (Override)
  -- @param pAddScore (OPTIONNEL) true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pShowFX (OPTIONNEL) true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pPlaySound (OPTIONNEL) true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  -- NOTE : les paramètres ne sont utilisés que pour la compatibilité avec les fonctions surchargées dans les classes enfants
  function lSprite.destroy (pAddScore, pShowFX, pPlaySound)
    -- debugFunctionEnter("sprite.destroy", pAddScore, pShowFX, pPlaySound)
    -- si une animation pour la destruction existe, on la joue
    if (lSprite.animations[ANIM_NAME_STATUS_DESTROY] ~= nil) then
      lSprite.playAnimation(ANIM_NAME_STATUS_DESTROY)
      -- c'est la fin de l'animation qui déclenchera la destruction effective et non le destroyDelay
      lSprite.destroyDelay = -1
      --lSprite.destroyDelay = lSprite.animations[ANIM_NAME_STATUS_DESTROY].frameCount * lSprite.animations[ANIM_NAME_STATUS_DESTROY].animationSpeed
    end
    lSprite.image = "" -- permet de ne plus afficher le player pendant le délai de sa destruction effective
    lSprite.status = SPRITE_STATUS_DESTROYING
  end -- sprite.destroy

  --- Effectue du nettoyage lié à le sprite en quittant le jeu (Override)
  --- supprime définitivement le sprite de la liste des sprites
  function lSprite.clean ()
    -- debugFunctionEnter("sprite.clean")
    -- debugMessage("suppression ", lSprite.getID())
    -- la suppression sera effective dans la boucle de sprite.update
    -- lSprite.listIndex = 0 -- WTF ? en mettant l'index de l'élément à supprimer à 0 on supprime le bon élément, en mettant la valeur de table index, cela supprime le mauvais
    -- if (entities.sprites ~= nil) then table.remove(entities.sprites, lSprite.listIndex) end
    lSprite.status = SPRITE_STATUS_DESTROYED

    -- marque toutes les animations liées comme étant à supprimer et vide la liste
    for index = #lSprite.animations, 1, -1  do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
      lSprite.animations[index].status = ANIM_STATUS_DESTROYING
      lSprite.animations = {}
    end
  end -- sprite.clean

  --- affiche le sprite
  function lSprite.draw ()
    ---- debugFunctionEnterNL("sprite.draw") -- ATTENTION  cet appel peut remplir le log

    if (lSprite.status == SPRITE_STATUS_DESTROYED) then return end

    local resource, imageFrame, imageFrameType, halfw, halfh, flipCoef, w, h, animColor

    -- le sprite possède t-il une animation en cours de lecture ?
    if (lSprite.currentAnimation ~= ANIM_NAME_NONE and lSprite.currentAnimation ~= nil) then
      -- Oui, on récupère l'image de la frame en cours et on l'affiche
      resource = lSprite.currentAnimation.resources[lSprite.currentAnimation.frame]
      assertEqualQuit (resource, nil, "lSprite.draw:la ressource "..lSprite.currentAnimation.frame.." de l'animation "..lSprite.currentAnimation.name.." du sprite "..lSprite.type.." n'a pas été trouvée")
      imageFrame = resource.source
      imageFrameType = resource.type
      animColor = lSprite.currentAnimation.color
    else
      -- non on prend l'image principale
      resource = lSprite.resource
      imageFrame = resource.source
      imageFrameType = resource.type
    end --  if (lSprite.currentAnimation ~= ANIM_NAME_NONE and lSprite.currentAnimation ~= nil) then

    if (animColor == nil) then animColor = lSprite.color end

    if (lSprite.status ~= SPRITE_STATUS_INVISIBLE) then

      -- Le sprite a t'il au moins une image ?
      if (resource.type ~= RESOURCE_TYPE_UNDEFINED and resource.type ~= RESOURCE_TYPE_NOTFOUND) then
        -- ATTENTION: pour l'affichage de l'image pas de mise à l'échelle des dimensions !!
        w = resource.width
        h = resource.height
        halfw = w / 2
        halfh = h / 2
        -- pour test de symétrie uniquement
        --[[
          if (lSprite.rotation == nil) then lSprite.rotation = 0 else lSprite.rotation = lSprite.rotation + 0.1 end
          lSprite.currentAnimation.flipedToRight = not lSprite.currentAnimation.flipedToRight
        ]]
        if (lSprite.currentAnimation.flipedToRight) then flipCoef = -1 else  flipCoef = 1 end
        -- NOTE on place l'origine de l'image au centre pour faciliter les opération de rotation
        -- mais on garde l'origine de la positon de l'image en haut à gauche afin de rester cohérent avec les collisions
        -- donc au final on décale l'image d'une 1/2 hauteur et d'une 1/2 largeur
        local rotationRadian = math.rad(lSprite.rotation)

        -- fichier image standard
        if (imageFrameType == RESOURCE_TYPE_IMAGE) then
          -- on affiche l'image en utilisant les origines X et Y du sprite
          HUD.drawImageScale(imageFrame, lSprite.x + halfw, lSprite.y + halfh, rotationRadian, flipCoef * SPRITE_SCALE, SPRITE_SCALE, halfw, halfh)

        -- fichier texte standard
        elseif (imageFrameType == RESOURCE_TYPE_TEXTFILE) then
          -- TODO: RESOURCE_TYPE_TEXTFILE

        -- fichier texte contenant une définition similaire à une map
        elseif (imageFrameType == RESOURCE_TYPE_MAPFILE) then
          -- TODO: RESOURCE_TYPE_MAPFILE

        -- fichier texte contenant une liste de points
        elseif (imageFrameType == RESOURCE_TYPE_POINTFILE or imageFrameType == RESOURCE_TYPE_RANDOMPOINT) then
          love.graphics.setColor(animColor[1], animColor[2], animColor[3], animColor[4])
          -- ajoute la position du joueur à chaque point du polygone
          local verticesSprite = {}
          local i
          for i = 1, #imageFrame, 2 do
            local posX = imageFrame[i] - lSprite.rotationCenterX
            local posY = imageFrame[i + 1]- lSprite.rotationCenterY

            -- calcul de la rotation des points en fonction de l'angle de rotation du sprite
            --[[ appliquer une rotation : voir page HTTP://WWW.HELIXSOFT.NL/ARTICLES/CIRCLE/SINCOS.HTM
            new_x = x * cos (angle) - y * sin (angle)
            new_y = x * sin (angle) + y * cos (angle)
            NOTE: In the table below you can look up how to rotate by 90, 180 and 270 degrees in this way.
                              90 degrees  180 degrees 270 degrees 360 degrees
                  new x value -y          -x          y           x
                  new y value x           -y          -x          y
            ]]
            local rotationRadian = math.rad(lSprite.rotation + 90)
            local cosAngle = math.cos(rotationRadian)
            local sinAngle = math.sin(rotationRadian)
            local newX = posX * cosAngle - posY * sinAngle
            local newY = posX * sinAngle + posY * cosAngle

            verticesSprite[i] = newX + lSprite.x + lSprite.rotationCenterX
            verticesSprite[i + 1] = newY + lSprite.y + lSprite.rotationCenterY
          end
          -- rend les lignes plus visibles
          love.graphics.setLineWidth(lSprite.lineWidth)
          love.graphics.setLineStyle( "smooth" ) -- smooth and rough ??
          love.graphics.polygon("line", verticesSprite)
          love.graphics.setLineWidth(1)
        else
          -- ressource non définie, normalement c'est une erreur
          love.graphics.setColor(lSprite.color[1], lSprite.color[2], lSprite.color[3], lSprite.color[4])
          HUD.rectangleScale("fill", lSprite.x, lSprite.y, lSprite.width, lSprite.height)
        end
      elseif (lSprite.status ~= SPRITE_STATUS_DESTROYING) then
        -- non, on affiche un simple rectangle de la couleur
        -- ATTENTION: les dimensions sont DEJA mises à l'échelle
        love.graphics.setColor(lSprite.color[1], lSprite.color[2], lSprite.color[3], lSprite.color[4])
        HUD.rectangleScale("fill", lSprite.x, lSprite.y, lSprite.width, lSprite.height)
      end
    end -- if (lSprite.status ~= SPRITE_STATUS_INVISIBLE) then

    -- affiche des infos supplémentaires sur le sprite
    if (DEBUG_MODE >= 3) then
      -- l'id du sprite
      local content = lSprite.listIndex
      local font = HUD.get_fontDebug()
      love.graphics.setColor(0, 0, 255, 255)
      love.graphics.setFont(font)
      w = font:getWidth(content)
      h = font:getHeight(content)
      halfw = lSprite.width -- mise à l'échelle incluse
      halfh = lSprite.height -- mise à l'échelle incluse
      halfw = halfw / 2
      halfh = halfh / 2
      love.graphics.print(content, lSprite.x + halfw - w / 2, lSprite.y - h)
      -- son contour
      HUD.rectangleScale("line", lSprite.x, lSprite.y, lSprite.width, lSprite.height)
      -- son centre de rotation
      HUD.rectangleScale("line", lSprite.x + halfw, lSprite.y + halfh, 2, 2)
      -- ses hotspots
      local key, hotspot
      love.graphics.setColor(0, 255, 0, 255)
      for key, hotspot in pairs(lSprite.hotSpots) do
        HUD.rectangleScale("line",lSprite.x + hotspot.x, lSprite.y + hotspot.y, 2, 2)
      end

    end

    love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
  end -- sprite.draw

  --- place le sprite dans la zone de jeu avec des coordonnées en pixel
  -- @param pX (OPTIONNEL) position X où placer le sprite. Si absent une valeur dans la zone de jeu sera choisie aléatoirement.
  -- @param pY (OPTIONNEL) position Y où placer le sprite.  Si absent une valeur dans la zone de jeu sera choisie aléatoirement.
  function lSprite.spawn (pX, pY)
    -- debugFunctionEnter("sprite.spawn", pX, pY)
    -- si une animation pour le spawn existe, on la joue
    if (lSprite.animations[ANIM_NAME_STATUS_SPAWN] ~= nil) then
      lSprite.playAnimation(ANIM_NAME_STATUS_SPAWN)
    end

    if (pX == nil) then pX = (viewport.getWidth() - lSprite.width) / 2 end
    if (pY == nil) then pY = ((viewport.getHeight() - lSprite.height - viewport.charHeight * 2) / 2) end

    lSprite.x = pX
    lSprite.y = pY
    lSprite.speed = lSprite.initialValues.speed
    lSprite.vX = lSprite.initialValues.vX
    lSprite.vY = lSprite.initialValues.vY
    lSprite.rotation = lSprite.initialValues.rotation
    lSprite.direction = lSprite.initialValues.direction
    lSprite.weight = lSprite.initialValues.weight
    lSprite.gravity  = lSprite.initialValues.gravity
    lSprite.energy = lSprite.initialValues.energy
    lSprite.canJump = lSprite.initialValues.canJump
    lSprite.canMakeAction = lSprite.initialValues.canMakeAction

  end -- sprite.spawn

  --- place le sprite dans dans la zone de jeu avec des coordonnées en ligne/colonne
  -- @param pCol (OPTIONNEL) colonne où placer le sprite. Si absent la valeur playerStart.col sera utilisée, ou bien la valeur correspondant au centre de l'écran.
  -- @param pLine (OPTIONNEL) ligne où placer le sprite. Si absent la valeur playerStart.line sera utilisée, ou bien la valeur correspondant au centre de l'écran.
  function lSprite.spawnToMap (pCol, pLine)
    -- debugFunctionEnter("sprite.spawnToMap ", pCol, pLine)
    local pX, pY
    pX = 1
    pY = 1
    if (pCol == nil) then pCol = map.playerStart.col end
    if (pLine == nil) then pLine = map.playerStart.line end
    if (map == nil or map.playerStart == nil or map.playerStart == {} or pCol == nil or pLine == nil) then 
      pX = (viewport.getWidth() - lSprite.width) /2
      pY = (viewport.getHeight() - lSprite.height) /2
    else
      pX, pY = map.mapToPixel (pCol, pLine)
    end
    lSprite.spawn(pX, pY)
  end -- sprite.spawnToMap

  --- Aligne le sprite sur la colonne o il se trouve
  function lSprite.alignOnColumnTile ()
    -- debugFunctionEnter("sprite.alignOnColumnTile")
    local mathFloor = math.floor
    lSprite.x = mathFloor((lSprite.x + map.tilewidth / 2) / map.tilewidth) * map.tilewidth
  end -- sprite.alignOnColumnTile

   --- Aligne le sprite sur la ligne ou il se trouve
  function lSprite.alignOnLineTile ()
    -- debugFunctionEnter("sprite.alignOnLineTile")
    local mathFloor = math.floor
    lSprite.y = mathFloor((lSprite.y + map.tileheight / 2) / map.tileheight) * map.tileheight
  end -- sprite.alignOnLineTile

 --- ajoute un hotSpot (point de vérification des collision) au sprite
  -- NOTe : la fonction createCollisionBox() permet de créer des ensembles de hotspot à partir de modèles prédéfinis
  -- @param pOffsetX décalage en X du point par rapport à l'origine du sprite
  -- @param pOffsetY décalage en Y du point par rapport à l'origine du sprite
  -- @param pType type de hotspot permettant de savoir quelles collisions tester (voir les constantes HOTSPOT_TYPE_xxx)
  -- @param OPTIONNEL pKey clé du hotspot dans la liste, elle doit être unique. Si absent, la clé utilisée sera égale au le nombre d'éléments existant dans la liste (index numérique croissant)
  function lSprite.addHotSpot (pOffsetX, pOffsetY, pType, pKey)
    ---- debugFunctionEnterNL("sprite.addHotSpot ", pOffsetX, pOffsetY, pType, pKey) -- ATTENTION cet appel peut remplir le log
    if (pKey == nil) then pKey = #lSprite.hotSpots end
    lSprite.hotSpots[pKey] = {x = pOffsetX, y = pOffsetY, type = pType}
  end -- sprite.addHotSpot

  --- Vérifie si le sprite entre en collision avec un autre sprite (collideWithSprite)
  -- note : REMPLACER CETTE FONCTION PAR LES COLLSISONS DE TYPE HOTSPOT
  -- @param pSprite autre sprite à vérifier
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  function lSprite.collideWithSprite (pSprite)
    ---- debugFunctionEnterNL("sprite.collideWithSprite ", pSprite) -- ATTENTION  cet appel peut remplir le log
    -- TODO: utiliser les hotspot de manière optimisée pour la detection de collision. pour le moment, seul une "box collision" basée sur les position et les dimensions est utilisée
    if (
      (lSprite == pSprite)
      or(not lSprite.checkIfCanCollide())
      or(not pSprite.checkIfCanCollide())
    )
    then
      return SPRITE_COLLISION_NONE
    end
    --[[ CE CODE NE FONCTIONNE PAs CORRECTEMENT. WTF ?
    local dx = lSprite.x - pSprite.x
    local dy = lSprite.y - pSprite.y
    if (math.abs(dx) < lSprite.width + pSprite.width) and (math.abs(dy) < lSprite.height + pSprite.height) then
      debugLastcollision = {
        s1 = {lSprite.x, lSprite.y, lSprite.width, lSprite.height},
        s2 = {pSprite.x, pSprite.y, pSprite.width, pSprite.height},
        timer = 2
      }
      return SPRITE_COLLISION_SPRITE
    end
    ]]
    if (lSprite.x < pSprite.x + pSprite.width and
      pSprite.x < lSprite.x + lSprite.width and
      lSprite.y < pSprite.y + pSprite.height and
      pSprite.y < lSprite.y + lSprite.height
      ) then
      --[[
      debugLastcollision = {
        s1 = {lSprite.x, lSprite.y, lSprite.width, lSprite.height},
        s2 = {pSprite.x, pSprite.y, pSprite.width, pSprite.height},
        timer = 2
      }
      ]]
      return SPRITE_COLLISION_SPRITE
    end
    return SPRITE_COLLISION_NONE
  end -- sprite.collideWithSprite

  --- Vérifie si un hotspot d'un type donnée entre en collision
  -- @param pType: type de hotspot (voir les constantes HOTSPOT_TYPE_xxx)
  -- @return SPRITE_COLLISION_TILE_SOLID, SPRITE_COLLISION_TILE_JUMPTHROUGH ou SPRITE_COLLISION_NONE
  function lSprite.collideWithTile (pType)
    ---- debugFunctionEnterNL("sprite.collideWithTile ", pType) -- ATTENTION cet appel peut remplir le log
    if (not lSprite.checkIfCanCollide()) then
      return SPRITE_COLLISION_NONE
    end

    local key, hotspot, indexTile
    local mapIsTiled = (map ~= nil and map.grid ~= nil and map.type ~= MAP_TYPE_NOT_TILED)

    -- on boucle sur tous les hotSpot définis pour le sprite
    for key, hotspot in pairs(lSprite.hotSpots) do
      -- collision avec les tuiles
      if (mapIsTiled) then
        -- index de la tuile située sous le hotSpot
        indexTile = map.getTileAt(lSprite.x + hotspot.x, lSprite.y + hotspot.y)
        if ((hotspot.type == pType or hotspot.type == HOTSPOT_TYPE_UNIQUE)-- hot spot du type attendu ou de type HOTSPOT_TYPE_UNIQUE
          and indexTile ~= nil and indexTile ~= TILE_SPECIAL_EMPTY -- tuile existante
        ) then
          if (map.isSolid(indexTile)) then
            return SPRITE_COLLISION_TILE_SOLID -- tuile solide
          elseif (map.isJumpThrough(indexTile)) then
            return SPRITE_COLLISION_TILE_JUMPTHROUGH -- tuile jumpThrough
          end
        end
      else
      end -- if (mapIsTiled)
    end

    return SPRITE_COLLISION_NONE
  end -- sprite.collideWithTile

  --- Vérifie si le sprite entre en collision à gauche
  -- @return SPRITE_COLLISION_TILE_SOLID_LEFT ou SPRITE_COLLISION_NONE
  function lSprite.collideWithTileLeft ()
    ---- debugFunctionEnterNL("sprite.collideWithTileLeft") -- ATTENTION cet appel peut remplir le log
    if (lSprite.collideWithTile(HOTSPOT_TYPE_LEFT) == SPRITE_COLLISION_TILE_SOLID) then
      return SPRITE_COLLISION_TILE_SOLID_LEFT
    end
    return SPRITE_COLLISION_NONE
  end -- sprite.collideWithTileLeft

  --- Vérifie si le sprite entre en collision à droite
  -- @return SPRITE_COLLISION_TILE_SOLID_RIGHT ou SPRITE_COLLISION_NONE
  function lSprite.collideWithTileRight ()
    ---- debugFunctionEnterNL("sprite.collideWithTileRight") -- ATTENTION cet appel peut remplir le log
    if (lSprite.collideWithTile(HOTSPOT_TYPE_RIGHT) == SPRITE_COLLISION_TILE_SOLID) then
      return SPRITE_COLLISION_TILE_SOLID_RIGHT
    end
    return SPRITE_COLLISION_NONE
  end -- sprite.collideWithTileRight

  --- Vérifie si le sprite entre en collision en haut
  -- @return SPRITE_COLLISION_TILE_SOLID_TOP ou SPRITE_COLLISION_NONE
  function lSprite.collideWithTileTop ()
    ---- debugFunctionEnterNL("sprite.collideWithTileTop") -- ATTENTION cet appel peut remplir le log
    if (lSprite.collideWithTile(HOTSPOT_TYPE_TOP) == SPRITE_COLLISION_TILE_SOLID) then
      return SPRITE_COLLISION_TILE_SOLID_TOP
    end
    return SPRITE_COLLISION_NONE
  end -- sprite.collideWithTileTop

  --- Vérifie si le sprite entre en collision en bas
  -- @return SPRITE_COLLISION_TILE_SOLID_BOTTOM, SPRITE_COLLISION_TILE_JUMPTHROUGH_BOTTOM ou SPRITE_COLLISION_NONE
  function lSprite.collideWithTileBottom ()
    ---- debugFunctionEnterNL("sprite.collideWithTileBottom") -- ATTENTION cet appel peut remplir le log
    local test = lSprite.collideWithTile(HOTSPOT_TYPE_BOTTOM)
    local mathFloor = math.floor
    if (test == SPRITE_COLLISION_TILE_SOLID) then
      -- tuile solide
      return SPRITE_COLLISION_TILE_SOLID_BOTTOM
    elseif (test == SPRITE_COLLISION_TILE_JUMPTHROUGH) then
      -- tuile jumpThrough
      -- local lineY = math.floor((y + map.get_tileHeight() / 2) / map.get_tileHeight()) * map.get_tileHeight()

      local lineY = mathFloor((lSprite.y + map.tileheight / 2) / map.tileheight) * map.tileheight
      local distance = lSprite.y - lineY
      if (distance >= 0 and distance < 10) then
        return SPRITE_COLLISION_TILE_JUMPTHROUGH_BOTTOM
      end
    end
    return SPRITE_COLLISION_NONE
  end -- sprite.collideWithTileBottom

  --- Vérifie si le sprite est solide
  function lSprite.checkIfSolid ()
    ---- debugFunctionEnterNL("sprite.checkIfSolid") -- ATTENTION cet appel peut remplir le log
    local result
    result = (lSprite.type:find(SPRITE_TYPE_CHARACTER) ~= nil)
    result = result or (lSprite.type:find(SPRITE_TYPE_NPC) ~= nil)
    result = result or (lSprite.type:find(SPRITE_TYPE_PROJ) ~= nil)
    result = result or (lSprite.type:find(SPRITE_TYPE_ITEM) ~= nil)
    result = result or (lSprite.type:find(NEXT_EXT_EXIT) ~= nil)
    result = result and (lSprite.status ~= SPRITE_STATUS_DESTROYED)
    return result
  end -- sprite.checkIfSolid

  --- Vérifie si le sprite peut entrer en collision
  function lSprite.checkIfCanCollide ()
    ---- debugFunctionEnterNL("sprite.checkIfCanCollide ", pCheckHotSpots) -- ATTENTION cet appel peut remplir le log
    -- NOTE: pour le moment la collision entre sprite avec les hotspot n'est pas implémentée
    local result
    result = lSprite.status == SPRITE_STATUS_NORMAL
    result = result or lSprite.status == SPRITE_STATUS_INVINCIBLE
    result = result or lSprite.status == SPRITE_STATUS_DEADLY
    result = result or lSprite.status == SPRITE_STATUS_FREEZE
    return result
  end -- sprite.checkIfCanCollide

  --- Vérifie si le sprite peut entrer en collision avec les tuiles de la map
  function lSprite.checkIfCanCollideWithTiles ()
    ---- debugFunctionEnterNL("sprite.checkIfCanCollideWithTiles") -- ATTENTION cet appel peut remplir le log
    local result
    result = lSprite.canCollideWithTile
    result = result and lSprite.checkIfCanCollide()
    result = result and map.type == MAP_TYPE_TILED
    return result
  end -- sprite.checkIfCanCollideWithTiles

  --- Vérifie si le sprite peut se déplacer
  function lSprite.checkIfActive ()
    ---- debugFunctionEnterNL("sprite.checkIfActive") -- ATTENTION cet appel peut remplir le log
    local result
    result = lSprite.status ~= SPRITE_STATUS_FREEZE
    result = result and lSprite.status ~= SPRITE_STATUS_DESTROYING
    result = result and lSprite.status ~= SPRITE_STATUS_DESTROYED
    result = result and lSprite.status ~= SPRITE_STATUS_SLEEPING
    result = result and lSprite.status ~= SPRITE_STATUS_ANIM_RUNNING
    return result
  end -- sprite.checkIfActive

  --- Vérifie si le sprite doit se déplacer avec la caméra
  function lSprite.checkIfMoveWithCamera ()
    ---- debugFunctionEnterNL("sprite.checkIfMoveWithCamera") -- ATTENTION cet appel peut remplir le log
    local result
    result = lSprite.status == SPRITE_STATUS_FREEZE
    result = result or lSprite.status == SPRITE_STATUS_SLEEPING
    result = result or lSprite.status == SPRITE_STATUS_INVISIBLE
    result = result or lSprite.mustMoveWithCamera
    return result
  end -- sprite.checkIfActive

  --- Vérifie si le sprite peut faire une action
  function lSprite.checkIfCanMakeAction ()
    ---- debugFunctionEnterNL("sprite.checkIfCanMakeAction") -- ATTENTION cet appel peut remplir le log
    local result
    result = lSprite.canMakeAction and lSprite.checkIfActive()
    return result
  end -- sprite.checkIfCanMakeAction

  --- Vérifie si le sprite peut être touché
  function lSprite.checkIfCanBeDamaged ()
    ---- debugFunctionEnterNL("sprite.checkIfCanBeDamaged") -- ATTENTION cet appel peut remplir le log
    local result
    result = lSprite.status ~= SPRITE_STATUS_INVINCIBLE
    result = result and lSprite.status ~= SPRITE_STATUS_DEADLY
    result = result and lSprite.status ~= SPRITE_STATUS_INVISIBLE
    result = result and lSprite.status ~= SPRITE_STATUS_DESTROYING
    result = result and lSprite.status ~= SPRITE_STATUS_DESTROYED
    result = result and lSprite.status ~= SPRITE_STATUS_SLEEPING
    result = result and lSprite.status ~= SPRITE_STATUS_ANIM_RUNNING
    return result
  end -- sprite.checkIfCanBeDamaged

  --- définit les hotSpots du joueur pour les collisions
  function lSprite.createCollisionBox (pModel)
    ---- debugFunctionEnterNL("sprite.createCollisionBox")  -- ATTENTION cet appel peut remplir le log
    if (pModel == nil) then pModel = COLLISION_MODEL_POINT end
    lSprite.hotSpots = {}
    if (pModel == COLLISION_MODEL_BOX6) then
    -- modèle de collision avec 6 hotspots : top right, top left, middle right, middle left, bottom right, bottom left
      lSprite.addHotSpot(lSprite.width, 3, HOTSPOT_TYPE_RIGHT, "RT")
      lSprite.addHotSpot(lSprite.width, lSprite.height - 2, HOTSPOT_TYPE_RIGHT, "RB")
      lSprite.addHotSpot(-1, 3, HOTSPOT_TYPE_LEFT, "LT")
      lSprite.addHotSpot(-1, lSprite.height - 2, HOTSPOT_TYPE_LEFT, "LB")
      lSprite.addHotSpot(1, lSprite.height, HOTSPOT_TYPE_BOTTOM, "BL")
      lSprite.addHotSpot(lSprite.width - 2, lSprite.height, HOTSPOT_TYPE_BOTTOM, "BR")
      lSprite.addHotSpot(1, -1, HOTSPOT_TYPE_TOP, "TL")
      lSprite.addHotSpot(lSprite.width - 2, -1, HOTSPOT_TYPE_TOP, "TR")
    elseif (pModel == COLLISION_MODEL_BOX4) then
    -- modèle de collision avec 4 hotspots, middle top, middle right, middle bottom, middle left
      lSprite.addHotSpot(lSprite.width / 2 , 0 , HOTSPOT_TYPE_TOP, "MT")
      lSprite.addHotSpot(lSprite.width, lSprite.height / 2, HOTSPOT_TYPE_RIGHT, "MR")
      lSprite.addHotSpot(0, lSprite.height / 2, HOTSPOT_TYPE_LEFT, "ML")
      lSprite.addHotSpot(lSprite.width / 2 , lSprite.height, HOTSPOT_TYPE_BOTTOM, "MB")
    elseif (pModel == COLLISION_MODEL_POINT) then
    -- modèle de collision avec 1 hotspots: centre
      lSprite.addHotSpot(lSprite.width / 2 , lSprite.height / 2 , HOTSPOT_TYPE_UNIQUE, "CE")
    elseif (pModel == COLLISION_MODEL_NONE) then
    -- pas de modèle de collision , aucun de hotspot
    end
  end -- sprite.createCollisionBox

 --- Déplace, vérifie les collisions et adapte le mouvement en conséquence (Override)
  -- En cas de collision, rectifie les attributs lSprite.x et lSprite.y de le sprite
  -- @param pDt delta time
  -- @return true si il lSprite.y a une collision ou false sinon
  function lSprite.update (pDt)
    -- debugFunctionEnter("sprite.update:", pDt) -- ATTENTION cet appel peut remplir le log
    if (lSprite.status == SPRITE_STATUS_DESTROYED) then return end

    local createAnimationName = lSprite.currentAnimation.name

    -- verification pour éviter les bug en cas de mauvaise prise en compte de la destruction
    if (lSprite.life <= 0) then lSprite.status = SPRITE_STATUS_DESTROYING end

    debugVar = lSprite.toString()
    local lDt = pDt
    local speedForIdle = 0.5
    local speedForWalk = 10
    local speedForRun = 30

    local spriteIsPlayer = (lSprite.type:find(SPRITE_TYPE_CHARACTER) ~= nil)
    local spriteIsActive = lSprite.checkIfActive()
    local spriteCanCollideWithTiles = lSprite.checkIfCanCollideWithTiles()
    -- local spriteIsPNJ = (lSprite.type:find(SPRITE_TYPE_NPC) ~= nil)
    -- local spriteIsProjectile = (lSprite.type:find(SPRITE_TYPE_PROJ) ~= nil)
    -- local spriteIsObject = (lSprite.type:find(SPRITE_TYPE_ITEM) ~= nil)
    -- local spriteIsCollectable = (lSprite.type:find(NEXT_EXT_COLLECT) ~= nil)
    -- local spriteIsExit = (lSprite.type:find(NEXT_EXT_EXIT) ~= nil)

   -- création de variables locales pour optimiser les temps d'accès
    local spriteCollide = lSprite.collide
    local spriteCollideTileRight = lSprite.collideWithTileRight
    local spriteCollideTileLeft = lSprite.collideWithTileLeft
    local spriteCollideTileTop = lSprite.collideWithTileTop
    local spriteCollideTileBottom = lSprite.collideWithTileBottom

    if (appState.currentScreen.name ~= SCREEN_PLAY) then return end

    -- vérifie si la durée de vie du sprite n'est pas écoulée  (uniquement si une durée de vie a été définie)
    if (lSprite.timeToLive > 0) then
      lSprite.timeToLive  = lSprite.timeToLive - pDt
      if (lSprite.timeToLive <= 0) then
        -- oui, on le supprime
        lSprite.destroy(false, false, false)
        return
      end
    end

    -- pas de CCD avant le 1ere update (pour éviter les collision hors init et hors champs)
    if (not lSprite.firstUpdateDone) then
      lSprite.firstUpdateDone = true
      lSprite.updateLastPosition()
      return
    end

    local collisionType = SPRITE_COLLISION_NONE
    local collisionTestDone = false

    if (spriteIsActive) then
      -- le sprite est actif, on utilise la CDD
      -- ******************************
      local xMin, yMin, xMax, yMax= lSprite.getScreenLimits()

      -- DEBUT CCD
      -- on utilise la CCD pour palier au problèmes de LAG
      -- ******************************

      local stepX = 0.5
      local stepY = 0.5
      --if (DEBUG_MODE > 0) then stepX = 2; stepY = 2 end -- utile pour laisser du temps CPU au gameplay

      -- CCD Horizontale
      -- ******************************
      -- calcul de la distance X depuis le dernier update
      local distanceX = lSprite.vX * lDt

      -- distance à atteindre
      local destX = (lSprite.x + distanceX)
      -- le step n'est jamais supérieur à la valeur de l'update
      if (stepX > math.abs(distanceX)) then stepX = math.abs(distanceX) end
      if (distanceX > 0) then
        -- on limite la distance à atteindre à une valeur permettant d'effectuer au moins un test de collision avec les bords
        if (destX > xMax + stepX) then destX = xMax + stepX + 1 end
        -- vérification des collisions vers la Droite et les collisions propres au type de sprite
        -- on boucle de la position enregistrée à celle actualisée avec le pDt
        while (lSprite.x < destX) do
          if (spriteCanCollideWithTiles) then collisionType = spriteCollideTileRight() end -- permet de laisser faire une détection des collision spécifiques aux PNJ
            -- on vérifie si le sprite ne collisionne pas avec autre chose
          if (collisionType == SPRITE_COLLISION_NONE) then
            collisionType = spriteCollide(lDt)
            if (collisionType == SPRITE_COLLISION_LIMIT_RIGHT) then destX = lSprite.x end
            collisionTestDone = true
          end
          --[[ TODO voir si nécessaire
          if (collisionType ~= SPRITE_COLLISION_NONE and spriteCanCollideWithTiles) then
            -- collision, annule la vitesse en X
            lSprite.vX = 0
            lSprite.alignOnColumnTile()
          end
          ]]
          if (collisionType ~= SPRITE_COLLISION_NONE) then
            if (lSprite.mustDestroyedOnCollision) then lSprite.destroy() end
            break
          end
          -- pas de collision, on passe à la position suivante (INCREMENTATION)
          lSprite.x = lSprite.x + stepX
        end
      end
      if (distanceX < 0) then
        -- on limite la distance à atteindre à une valeur permettant d'effectuer au moins un test de collision avec les bords
        if (destX < xMin - stepX) then destX = xMin - stepX - 1 end
        -- vérification des collisions vers la Gauche et les collisions propres au type de sprite
        -- on boucle de la position enregistrée à celle actualisée avec le pDt
        while (lSprite.x > destX) do
          if (spriteCanCollideWithTiles) then collisionType = spriteCollideTileLeft() end -- permet de laisser faire une détection des collision spécifiques aux PNJ
            -- on vérifie si le sprite ne collisionne pas avec autre chose
          if (collisionType == SPRITE_COLLISION_NONE) then
            collisionType = spriteCollide(lDt)
            if (collisionType == SPRITE_COLLISION_LIMIT_LEFT) then destX = lSprite.x end
            collisionTestDone = true
          end
          --[[ TODO voir si nécessaire
          if (collisionType ~= SPRITE_COLLISION_NONE and spriteCanCollideWithTiles) then
            -- collision, annule la vitesse en X
            lSprite.vX = 0
            lSprite.alignOnColumnTile()
          end
          ]]
          if (collisionType ~= SPRITE_COLLISION_NONE) then
            if (lSprite.mustDestroyedOnCollision) then lSprite.destroy() end
            break
          end
          -- pas de collision, on passe à la position suivante (DECREMENTATION)
          lSprite.x = lSprite.x - stepX
        end
      end

      -- CCD Verticale
      -- ******************************
      -- calcul de la distance Y depuis le dernier update
      local distanceY = (lSprite.vY * lDt)
      -- si le sprite (autre que le joueur) n'a ni gravité, ni poids, il ne se déplace pas en Y
      if (not spriteIsPlayer) then
        -- if (lSprite.gravity == 0 or lSprite.weight == 0) then
        if (lSprite.weight == 0) then
          distanceY = 0
        end
      end
      -- distance à atteindre
      local destY = lSprite.y + distanceY
      -- le step n'est jamais supérieur à la valeur de l'update
      if (stepY > math.abs(distanceY)) then stepY = math.abs(distanceY) end

      if (distanceY < 0) then
        -- on limite la distance à atteindre à une valeur permettant d'effectuer au moins un test de collision avec les bords
        if (destY < yMin - stepY) then destY = yMin - stepY -1 end
        -- vérification des collisions vers le haut et les collisions propres au type de sprite
        -- on boucle de la position enregistrée à celle actualisée avec le pDt
        while (lSprite.y > destY) do
          if (spriteCanCollideWithTiles) then collisionType = spriteCollideTileTop() end -- permet de laisser faire une détection des collision spécifiques aux PNJ
            -- on vérifie si le sprite ne collisionne pas avec autre chose
          if (collisionType == SPRITE_COLLISION_NONE) then
            collisionType = spriteCollide(lDt)
            if (collisionType == SPRITE_COLLISION_LIMIT_TOP) then destY = lSprite.y end
            collisionTestDone = true
          end
          --[[ TODO voir si nécessaire
          if (collisionType ~= SPRITE_COLLISION_NONE and spriteCanCollideWithTiles) then
            -- collision, annule la vitesse en Y
            lSprite.vY = 0
            lSprite.alignOnLineTile()
          end
          ]]
          if (collisionType ~= SPRITE_COLLISION_NONE) then
            if (lSprite.mustDestroyedOnCollision) then lSprite.destroy() end
            break
          end
          -- pas de collision, on passe à la position suivante (DECREMENTATION)
          lSprite.y = lSprite.y - stepY
        end
      end

      -- vérifie si le sprite est en train de tomber
      if (lSprite.isStanding or lSprite.vY > 0) then
        if (spriteCanCollideWithTiles) then collisionType = spriteCollideTileBottom() end -- permet de laisser faire une détection des collision spécifiques aux PNJ
        if (collisionType ~= SPRITE_COLLISION_NONE and spriteCanCollideWithTiles) then
          lSprite.updateLastStanding(true)
          lSprite.vY = 0
          --lSprite.alignOnLineTile()
        else
          if (lSprite.gravity ~= 0) then
            if (not lSprite.isJumping) then
              local fallY = lSprite.y - lSprite.lastStanding.y
              if (lSprite.fallLimit > 0 and fallY > lSprite.fallLimit) then
                lSprite.isFalling = true
              end
            end -- if (not lSprite.isJumping) then
            lSprite.updateLastStanding(false)
          end -- if (lSprite.gravity ~= 0) then
        end -- if (collisionType ~= SPRITE_COLLISION_NONE and spriteCanCollideWithTiles) then
      end -- if (lSprite.isStanding or lSprite.vY > 0) then

      if (distanceY > 0) then
        -- vérification des collisions vers le bas et les collisions propres au type de sprite
        -- boucle de la position enregistrée à celle actualisée avec le pDt
        while (lSprite.y < destY) do
        -- on limite la distance à atteindre à une valeur permettant d'effectuer au moins un test de collision avec les bords
          if (destY > yMax + stepY) then destY = yMax + stepY + 1 end
          if (spriteCanCollideWithTiles) then collisionType = spriteCollideTileBottom() end -- permet de laisser faire une détection des collision spécifiques aux PNJ
          if (collisionType == SPRITE_COLLISION_NONE) then
              -- on vérifie si le sprite ne collisionne pas avec autre chose
            collisionType = spriteCollide(lDt)
            if (collisionType == SPRITE_COLLISION_LIMIT_BOTTOM) then destY = lSprite.y end
            collisionTestDone = true
          end
          if (collisionType ~= SPRITE_COLLISION_NONE and spriteCanCollideWithTiles and lSprite.isFalling) then
            -- collision, annule la vitesse en Y
            lSprite.vY = 0
            lSprite.updateLastStanding(true)
            lSprite.isJumping = false
            lSprite.stopSound("Fall")
            lSprite.playSound("Standing")
          end
          if (collisionType ~= SPRITE_COLLISION_NONE) then
            if (lSprite.mustDestroyedOnCollision) then lSprite.destroy() end
            break
          end
          -- pas de collision, on passe à la position suivante (INCREMENTATION)
          lSprite.y = lSprite.y + stepY
        end
      end

      -- Chute, tient compte de la gravité et du poids
      if (not lSprite.isStanding) then
        if (lSprite.gravityModularity <= 0) then
          -- le déplacement du à la gravité est fluide
          lSprite.vY = lSprite.vY + (lSprite.weight * lSprite.gravity * lDt)
        else
          -- le déplacement du à la gravité se fait avec de la modularité
          lSprite.timerDrop = lSprite.timerDrop + (lSprite.weight * lSprite.gravity * lDt)
          if (lSprite.timerDrop >= lSprite.gravityModularity) then
            lSprite.timerDrop = 0
            lSprite.vY = lSprite.vY + lSprite.gravityModularity
          end
        end
      end
      -- FIN CCD

      -- limite la vélocité en X
      -- rappel: on ne tient pas compte de la vélocité max si elle est inférieure à 0
      if (lSprite.velocityMax >= 0 and lSprite.vX < -lSprite.velocityMax) then
        lSprite.vX = -lSprite.velocityMax
      elseif (lSprite.velocityMax >= 0 and lSprite.vX > lSprite.velocityMax) then
        lSprite.vX = lSprite.velocityMax
      end
      
      -- est on dans un plateformer ?
      if (settings.gameType:find(GAME_TYPE_PLATEFORMER) == nil) then
        -- non, on limite la vélocité en Y, pour permettre les sauts et les chutes
        -- rappel: on ne tient pas compte de la vélocité max si elle est inférieure à 0
        if (lSprite.velocityMax >= 0 and lSprite.vY < -lSprite.velocityMax) then
          lSprite.vY = -lSprite.velocityMax
        elseif (lSprite.velocityMax >= 0 and lSprite.vY > lSprite.velocityMax) then
          lSprite.vY = lSprite.velocityMax
        end
      end
    end -- if (spriteIsActive) then

    -- vérification des collisions pour les sprites n'ayant pas été testés dans les boucles
    if (not collisionTestDone and lSprite.checkIfSolid() ) then
      collisionType = spriteCollide(lDt)
    end

    -- update de l'animation
    -- ******************************
    local absVx = math.abs(lSprite.vX)
    local absVy = math.abs(lSprite.vY)
    local animIsMove = (lSprite.currentAnimation ~= ANIM_NAME_NONE) and (lSprite.currentAnimation.name:find(ANIM_NAME_MOVE) ~= nil)
    local animIsState = (lSprite.currentAnimation ~= ANIM_NAME_NONE) and (lSprite.currentAnimation.name:find(ANIM_NAME_STATUS) ~= nil)
    local animIsAction = (lSprite.currentAnimation ~= ANIM_NAME_NONE) and (lSprite.currentAnimation.name:find(ANIM_NAME_ACT) ~= nil)

    -- le sprite saute t-il ?
    if (lSprite.isJumping) then
      createAnimationName = ANIM_NAME_MOVE_JUMP
    elseif (lSprite.isClimbing) then
      if (absVy > speedForIdle) then
        createAnimationName = ANIM_NAME_MOVE_CLIMB
      else
        createAnimationName = ANIM_NAME_MOVE_CLIMB_IDLE
      end
    -- le sprite chute t-il ?
    elseif (lSprite.isFalling) then
      -- on joue l'animation et le son pour une chute
      if (lSprite.currentAnimation.name ~= ANIM_NAME_MOVE_FALL and lSprite.lastSound ~= "Fall") then
        -- TODO: pourquoi ne pas mettre le son dans l'anim ?
        lSprite.playSound("Fall")
        createAnimationName = ANIM_NAME_MOVE_FALL
      end
      -- doit on appliquer des dégâts en cas de chute dépassant la limite ?
      if (lSprite.fallDamage > 0) then lSprite.loseEnergy(lSprite.fallDamage, false, false, true, true) end
    elseif (animIsMove) then
      -- quel mouvement en fonction de sa vitesse de déplacement ?
      createAnimationName = ANIM_NAME_MOVE_IDLE
      if (absVx > speedForWalk or absVy > speedForWalk) and (lSprite.animations[ANIM_NAME_MOVE_WALK] ~= nil) then createAnimationName = ANIM_NAME_MOVE_WALK end
      if (absVx > speedForRun or absVy > speedForRun) and (lSprite.animations[ANIM_NAME_MOVE_RUN] ~= nil) then createAnimationName = ANIM_NAME_MOVE_RUN end
    end
    --debugMessage(lSprite.type .." absVx="..absVx...." absVy="..absVy)
    if (createAnimationName ~= lSprite.currentAnimation.name) then
      lSprite.playAnimation(createAnimationName, true)
    else
      lSprite.updateAnimation(lDt)
    end

    -- update de la trajectoire qui lui est liée
    -- ******************************
    if (lSprite.navPath ~= nil) then lSprite.navPath.update(pDt) end

    lSprite.updateLastPosition()

    return collisionType
  end -- sprite.CCD

  --- Ajoute un son à partir d'un fichier
  -- Le son est ajouté à l'attribut sounds, indexé par le nom de l'animation.
  -- @param pName Nom du son.
  -- @param pFilename nom du fichier audio.
  -- @param pIsLooping (OPTIONNEL) true pour jouer l'animation en boucle. Si absent, par défaut sera utilisée.
  -- @return l'objet sound créé.
  function lSprite.addSound (pName, pFilename, pIsLooping)
    debugFunctionEnter("sprite.addSound ", pName, pFilename, pIsLooping)
    assertEqualQuit(pName, nil, "sprite.addSound:pName", true)
    pExt = ".mp3"
    if (pFilename == nil) then
      local pos, sepLen, nameExt, filename
      pos = lSprite.type:find(SEP_FIRST)
      -- recherche du séparateur indiquant le type de sprite
      if (pos ~= nil) then
        sepLen = #SEP_FIRST
        nameExt = string.sub(lSprite.type, pos + sepLen)
        -- recherche du dernier séparateur des sous-types de sprite
        pos = nameExt:find(SEP_NEXT)
        while (pos ~= nil) do
          sepLen = #SEP_NEXT
          nameExt = string.sub(nameExt, pos + sepLen)
          pos = nameExt:find(SEP_NEXT)
        end
        -- recherche du séparateur indiquant la fin du type de sprite
        pos = nameExt:find(SEP_LAST)
        if (pos ~= nil) then
          sepLen = #SEP_LAST
          nameExt = string.sub(nameExt, 1, pos - 1)
        end
        nameExt = "/"..nameExt
        pFilename = FOLDER_SOUNDS..nameExt..pName..pExt
      end
    end
    local lSound = createSound(pName, pFilename, lSprite, pIsLooping)
    -- rappel: createSound(pName, pName, pFilename, pEntity, pIsLooping)
    lSprite.sounds[pName] = lSound
    return lSound
  end -- sprite.addSound

  --- Joue un son
  -- @param pName Nom du son.
  function lSprite.playSound (pName)
    -- debugFunctionEnter("sprite.playSound ", pName)
    assertEqualQuit(pName, nil, "sprite.playSound:pName", true)
    if (lSprite.sounds[pName] ~= nil) then
      lSprite.sounds[pName].play()
      lSprite.lastSound = pName
    end
  end -- sprite.playSound

  --- Arrête la lecture d'un son
  -- @param pName Nom du son.
  function lSprite.stopSound (pName)
    -- debugFunctionEnter("sprite.playSound ", pName)
    assertEqualQuit(pName, nil, "sprite.stopSound:pName", true)
    if (lSprite.sounds[pName] ~= nil) then
      lSprite.sounds[pName].stop()
      lSprite.lastSound = pName
    end
  end -- sprite.playSound

--- Ajoute une animation à partir d'un dossier contenant ses images
-- L'animation est ajoutée à l'attribut animations, indexée par le nom de l'animation.
-- @param pName Nom de l'animation.
-- @param pFolder (OPTIONNEL) le chemin du sous répertoire contenant les images de l'animation
-- @param pSpeed (OPTIONNEL) la durée d'une frame en secondes. Si absent, par défaut sera utilisée.
-- @param pExt (OPTIONNEL) extension des fichiers à utiliser. Si absent, par défaut sera utilisée.
-- @param pIsLooping (OPTIONNEL) true pour jouer l'animation en boucle. Si absent, par défaut sera utilisée.
-- @param pNextAnimationName (OPTIONNEL) nom de l'animation suivante. Si absent, par défaut sera utilisée.
-- @param pSound (OPTIONNEL) son pour le lancement de l'animation. Si absent, aucun son ne sera joué.
-- @param pMustPlaySound (OPTIONNEL) faut il jouer le son pour le lancement de l'animation. Si absent, par défaut sera utilisée.
-- @param pSoundIsLooping (OPTIONNEL) faut il jouer le son en boucle. Si absent, par défaut sera utilisée.
-- @param pSoundEnd (OPTIONNEL) son pour la fin de l'animation. Si absent, Si absent, aucun son ne sera joué.
-- @param pMustPlaySoundEnd (OPTIONNEL) faut il jouer le son pour la fin de l'animation. Si absent, par défaut sera utilisée.
-- @param pEntityStatusWhenRunning (OPTIONNEL) status de l'entité pendant l'animation.
-- @param pEntityStatusWhenFinished (OPTIONNEL) status de l'entité à la fin de l'animation.
-- @param pCallBackWhenFinished (OPTIONNEL) fonction à appeler à la fin de l'animation.
-- @return l'objet animation créée.
  function lSprite.addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    -- debugFunctionEnter("sprite.addAnimation ", pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    assertEqualQuit(pFolder, nil, "sprite.addAnimation:pFolder", true)
    assertEqualQuit(pName, nil, "sprite.addAnimation:pName", true)

    -- rappel: createAnimation(pName, pFolder, pExt, pEntity, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    local lAnimation = createAnimation(pName, pFolder, pExt, lSprite, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lSprite.animations[pName] = lAnimation
    return lAnimation
  end -- sprite.addAnimation

  --- Joue une (nouvelle) animation
  -- @param pName le nom de l'animation
  -- @param pMustPlaySound (OPTIONNEL) true pour jouer le son lié à l'animation. Si absent la valeur par défaut  sera utilisée.
  function lSprite.playAnimation (pName, pMustPlaySound)
    -- debugFunctionEnter("sprite.playAnimation ", pName)
    assertEqualQuit(pName, nil, "sprite.playAnimation:pName", true)

    if (lSprite.animations[pName] ~= nil and pName ~= lSprite.currentAnimation.name) then
      lSprite.stopAnimation()
      lSprite.animations[pName].play(pMustPlaySound)
      lSprite.currentAnimation = lSprite.animations[pName]
    end
  end -- sprite.playAnimation

  --- Stoppe l'animation courante ou une animation donnée.
  -- @param pName (OPTIONNEL) le nom de l'animation. Si absent, l'animation en cours sera arrêtée.
  function lSprite.stopAnimation (pName)
    -- debugFunctionEnter("lSprite.playAnimation ", pName)
    local lAnimation = nil
    if (pName == nil) then
      lAnimation = lSprite.currentAnimation
    elseif (lSprite.animations[pName] ~= nil) then
      lAnimation = lSprite.animations[pName]
    end
    -- si l'animation existe, on la stoppe
    if (lAnimation ~= nil and lAnimation ~= ANIM_NAME_NONE) then lAnimation.stop() end
    lSprite.currentAnimation = ANIM_NAME_NONE
  end -- sprite.stopAnimation

  --- actualise l'animation en cours
  -- @param pDt delta time
  function lSprite.updateAnimation (pDt)
    -- debugFunctionEnter("sprite.updateAnimation ", pDt) -- ATTENTION cet appel peut remplir le log
    local currentAnimation = lSprite.currentAnimation

    if (currentAnimation ~= nil and currentAnimation ~= ANIM_NAME_NONE) then
      currentAnimation.update(pDt)
    end
  end -- sprite.updateAnimation

  --- Retourne les limites de la zone de jeu en fonction de la taille du sprite
  -- @param pOffsetX (OPTIONNEL) décalage en X de l'origine du sprite. Si absent, 0 sera utilisée.
  -- @param pOffsetY (OPTIONNEL) décalage en Y de l'origine du sprite. Si absent, 0 sera utilisée.
  function lSprite.getScreenLimits (pOffsetX, pOffsetY)
    ---- debugFunctionEnterNL("sprite.getScreenLimits ", pOffsetX, pOffsetY) -- ATTENTION  cet appel peut remplir le log
    if (pOffsetX == nil) then pOffsetX = 0 end
    if (pOffsetY == nil) then pOffsetY = 0 end
    local xMin = pOffsetX
    local yMin = pOffsetY
    local xMax = viewport.getWidth() - lSprite.width + pOffsetX
    local yMax = viewport.getHeight() - lSprite.height + pOffsetY
    return xMin, yMin, xMax, yMax
  end -- lSprite.getScreenLimits

  --- Le sprite perd de l'énergie
  -- @param pLoseAllAtOnce (OPTIONNEL) true pour perdre la totalité d'un vie en une fois (ne teins pas compte de l'énergie restante). Si absent, false sera utilisée.
  -- @param pRespawn (OPTIONNEL) true pour replacer le sprite sur la map. Si absent, sprite.mustRespawnIfDestroyed sera utilisée.
  -- @param pShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  -- @param pDamages (OPTIONNEL) dommages reçus. Si absent la valeur 1 sera utilisée.
  -- @return nombre de vies et énergie restants
  function lSprite.loseEnergy (pDamages, pLoseAllAtOnce, pRespawn, pShowFX, pPlaySound)
    debugFunctionEnter("sprite.loseEnergy", pLoseAllAtOnce, pRespawn, pShowFX, pPlaySound)

    if (not lSprite.checkIfCanBeDamaged()) then return end

    if (pDamages == nil) then pDamages = 1 end
    if (pLoseAllAtOnce == nil) then pLoseAllAtOnce = false end
    if (pRespawn == nil) then pRespawn = lSprite.mustRespawnIfDestroyed end
    if (pShowFX == nil) then pShowFX = lSprite.mustDestroyedWithFX end
    if (pPlaySound == nil) then pPlaySound = lSprite.mustDestroyedWithSound end

    if (pLoseAllAtOnce) then lSprite.energy = 0 end -- détruit toute l'énergie (bouclier ou autre) en 1 fois

    lSprite.energy = lSprite.energy - pDamages
    if (pPlaySound) then lSprite.sounds["LoseEnergy"].play() end

    -- reste t-il de l'énergie ?
    if (lSprite.energy > 0) then
      -- oui, rien de spécial
      -- si une animation pour la perte d'énergie existe, on la joue (ici car tous les sprites n'en n'ont pas)
      if (lSprite.animations[ANIM_NAME_STATUS_LOSE_ENERGY] ~= nil) then
        lSprite.playAnimation(ANIM_NAME_STATUS_LOSE_ENERGY)
      end
    else
      -- non, on enlève une vie
      lSprite.life = lSprite.life - 1
      lSprite.energy = lSprite.initialValues.energy
      if (pPlaySound) then lSprite.sounds["LoseLife"].play() end

      -- existe t-il une animation pour la perte de vie existe ?
      if (lSprite.animations[ANIM_NAME_STATUS_LOSE_LIFE] ~= nil) then
        -- oui, on la joue (ici car tous les sprites n'en n'ont pas)
        lSprite.playAnimation(ANIM_NAME_STATUS_LOSE_LIFE)
        -- note: la fonction lSprite.checkLife() est appelée en fin d'animation SANS PARAMETRE mais en utilisant les attributs du sprite que l'on modifie temporairement
        lSprite.mustRespawnIfDestroyed = pRespawn
        lSprite.mustDestroyedWithFX = pShowFX
        lSprite.mustDestroyedWithSound = pPlaySound
      else
        -- non, on passe directement à la verification du nombre de vies restantes
        lSprite.checkLife(pRespawn, pShowFX, pPlaySound)
      end
    end -- if (lSprite.energy > 0) then
    return lSprite.life, lSprite.energy
  end

  -- vérifie les vies restantes
  -- NOTE: fonction appelée à la fin de l'animation ANIM_NAME_STATUS_LOSE_LIFE
  -- @param pRespawn (OPTIONNEL) true pour replacer le sprite sur la map. Si absent, sprite.mustRespawnIfDestroyed sera utilisée.
  -- @param pShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  function lSprite.checkLife(pRespawn, pShowFX, pPlaySound)
    debugFunctionEnter("sprite.checkLife", pRespawn, pShowFX, pPlaySound)
    -- note: les attributs du sprite ont pu être modifiés temporairement
    lSprite.mustRespawnIfDestroyed = lSprite.initialValues.mustRespawnIfDestroyed
    lSprite.mustDestroyedWithFX = lSprite.initialValues.mustDestroyedWithFX
    lSprite.mustDestroyedWithSound = lSprite.initialValues.mustDestroyedWithSound

    if (pRespawn == nil) then pRespawn = lSprite.mustRespawnIfDestroyed end
    if (pShowFX == nil) then pShowFX = lSprite.mustDestroyedWithFX end
    if (pPlaySound == nil) then pPlaySound = lSprite.mustDestroyedWithSound end

    -- reste t-il des vies ?
    if (lSprite.life > 0) then
      -- doit on faire un re-spawn ?
      if (pRespawn) then
        -- oui, on re-spawne le sprite
        -- note: l'animation est lancée dans spawn()
        if (map.type == MAP_TYPE_NOT_TILED) then
          lSprite.spawn()
        else
          lSprite.spawnToMap()
        end
      end
    else
      -- non, on détruit le sprite
      lSprite.destroy(pAddScore, pShowFX, pPlaySound)
      -- note: l'animation est lancée dans destroy()
    end
  end -- sprite.checkLife

  -- Charge les sons par défaut liés au sprite
  -- note: utilise le type de sprite en le décomposant pour retrouver le nom du fichier à associer
  function lSprite.loadDefaultSounds ()
    -- debugFunctionEnter("sprite.loadDefaultSounds")
    lSprite.addSound("Action")
    lSprite.addSound("Action2")
    lSprite.addSound("LoseLife")
    lSprite.addSound("LoseEnergy")
    lSprite.addSound("Destroy")
  end -- sprite.loadDefaultSounds

  -- Fonctions "virtuelles" remplacées dans les "classes enfants"
  -- ******************************
  function lSprite.collide ()
    ---- debugFunctionEnterNL("sprite.collide") -- ATTENTION cet appel peut remplir le log
    return SPRITE_COLLISION_NONE
  end

  --- Le sprite effectue une action (ie. tire un projectile)
   -- @param pType (OPTIONNEL) type de l'action (voir les constantes LAST_EXT_ACTIONXXX). Si absent, LAST_EXT_ACTION1 sera utilisée.
  function lSprite.action (pType)
    if (pType == nil) then pType = LAST_EXT_ACTION1 end
    -- debugFunctionEnter("sprite.action ", pType)
    lSprite.playSound("Action")
  end

  -- initialisation par défaut
  lSprite.initialize()

  -- Ajout du sprite à la liste
  if (entities.sprites ~= nil) then
    -- index du sprite dans la table (utile pour sa suppression)
    lSprite.listIndex = #entities.sprites + 1
    entities.sprites[lSprite.listIndex] = lSprite
  end

  return lSprite, lSprite.listIndex
end -- createSprite
