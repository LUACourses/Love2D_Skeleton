-- Entité représentant une map
-- ****************************************

-- TYPE DE MAP
MAP_TYPE_TILED = "mapTypeTiled"
MAP_TYPE_NOT_TILED = "mapTypeNotTiled"

-- tuiles avec des actions spécifiques dans la map
TILE_SPECIAL_PLAYER_START = "P" -- départ du joueur
TILE_SPECIAL_COIN = "c" -- une pièce
TILE_SPECIAL_DOOR = "D" -- la porte de sortie
TILE_SPECIAL_BLOB = "@" -- 1er type de PNJ
TILE_SPECIAL_EMPTY = "0" -- vide

-- type de tuile
TILE_TYPE = "tileType"
TILE_TYPE_SOLID = TILE_TYPE..SEP_LAST.."solid" -- solide, non traversable
TILE_TYPE_NO_SOLID = TILE_TYPE..SEP_LAST.."no_solid" -- non solide, traversable
TILE_TYPE_JUMPTHROUGH = TILE_TYPE..SEP_LAST.."jumpthrough" -- solide, mais traversable avec un saut
TILE_TYPE_LADDER = TILE_TYPE..SEP_LAST.."ladder" -- fait partie d'une échelle
TILE_TYPE_INVISIBLE = TILE_TYPE..SEP_LAST.."invisible" -- invisible, juste pour utile pour détection d'objet
TILE_TYPE_GO_LEFT = TILE_TYPE..SEP_LAST.."go_left" -- invisible, force un déplacement vers la gauche
TILE_TYPE_GO_RIGHT = TILE_TYPE..SEP_LAST.."go_right" -- invisible, force un déplacement vers la droite
TILE_TYPE_GO_UP = TILE_TYPE..SEP_LAST.."go_up" -- invisible, force un déplacement vers le haut
TILE_TYPE_GO_DOWN = TILE_TYPE..SEP_LAST.."go_down" -- invisible, force un déplacement vers le bas

-- taille par défaut des tuiles de la map
MAP_MAP_TILE_WIDTH = 16
MAP_TILE_HEIGHT = 16

--- Crée un pseudo objet de type map
-- @param pLevel (OPTIONNEL) niveau de jeu associé à la map.
-- @return un pseudo objet map
function createMap (pLevel)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createMap ", pLevel)

  if (pLevel == nil) then pLevel = 1 end

  local lMap = {}
  -- type de map
  lMap.type = MAP_TYPE_NOT_TILED
  -- numéro du niveau
  lMap.level = 0
  -- nom associé à chaque niveau
  lMap.names = {}
  -- largeur (en pixel)
  lMap.width = 0
  -- hauteur (en pixel)
  lMap.height = 0
  -- tuiles utilisées par la map
  lMap.tiles = {}
  -- largeur des tuiles de la map
  lMap.tilewidth = 0
  -- hauteur des tuiles de la map
  lMap.tileheight = 0
  -- nombre de colonnes 
  lMap.cols = 0
  -- nombre de lignes
  lMap.rows = 0
  -- contenu de la map
  lMap.grid = {}
  -- position de départ du joueur
  lMap.playerStart = {}

  --- initialise la map, Crée et positionne les contenus (objets, PNJs...) dans la map pour un niveau (de difficulté) donné
  -- @param pLevel (OPTIONNEL) niveau (de difficulté). Si absent, 1 sera utilisée.
  function lMap.initialize (pLevel)
    -- debugFunctionEnter("map.initialize ", pLevel)
    lMap.level = pLevel
    lMap.tilewidth = MAP_MAP_TILE_WIDTH
    lMap.tileheight = MAP_TILE_HEIGHT
    lMap.width = viewport.getWidth()
    lMap.height = viewport.getHeight()
  end

  -- fonctions liées aux tuiles
  -- ******************************
  --- Convertit des coordonnées de map vers des coordonnées en pixel
  function lMap.mapToPixel (pCol, pLine)
    local x, y
    x = (pCol - 1) * lMap.tilewidth
    y = (pLine - 1) * lMap.tileheight
    -- ajustement car l'affichage du sprite se fait fait à partir du haut gauche et non de son centre
    -- x = math.floor(x + lMap.tilewidth / 2)
    -- y = math.floor(y + lMap.tileheight / 2)
    return x, y
  end -- map.mapToPixel

  --- Convertit des coordonnées en pixel vers des coordonnées de map
  function lMap.pixelToMap (pX, pY)
    local c, l
    -- ajustement car l'affichage du sprite se fait fait à partir du haut gauche et non de son centre
    -- pX = pX + lMap.tilewidth / 2
    -- pY = pY + lMap.tileheight / 2
    local mathFloor = math.floor -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    c = mathFloor(pX / lMap.tilewidth) + 1
    l = mathFloor(pY / lMap.tileheight) + 1
    return c, l
  end -- map.pixelToMap

  --- Retourne l'index de la tuile de la map située au coordonnées en pixels données
  -- @param pX position en X en pixel
  -- @param pY position en Y en pixel
  -- @return Index de la tuile
  function lMap.getTileAt (pX, pY)
    ---- debugFunctionEnterNL("map.getTileAt ", pX, pY) -- ATTENTION cet appel peut remplir le log
    if (lMap.grid == nil) then return nil end
    local l, c
    local stringSub = string.sub -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    c, l = lMap.pixelToMap(pX, pY)
    if (l > 0 and l <= #lMap.grid and c > 0 and c <= #lMap.grid[1]) then
      return stringSub(lMap.grid[l], c, c)
    end
    return nil
  end -- map.getTileAt

  --- Indique si la une tuile est solide ou pas (IE. elle bloque le joueur)
  -- @param pTileId Index de la tuile
  -- @return true ou false
  function lMap.isSolid (pTileId)
    ---- debugFunctionEnterNL("map.isSolid ", pTileId) -- ATTENTION cet appel peut remplir le log
    local tiles = lMap.tiles -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    if (tiles ~= nil and tiles[pTileId] ~= nil and tiles[pTileId].type == TILE_TYPE_SOLID) then return true end
    return false
  end -- map.isSolid

  --- Indique si la une tuile peut être traversée par un saut ou pas
  -- @param pTileId: Index de la tuile
  -- @return true ou false
  function lMap.isJumpThrough (pTileId)
    ---- debugFunctionEnterNL("map.isJumpThrough ", pTileId) -- ATTENTION cet appel peut remplir le log
    local tiles = lMap.tiles -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    if (tiles ~= nil and tiles[pTileId] ~= nil and tiles[pTileId].type == TILE_TYPE_JUMPTHROUGH) then return true end
    return false
  end -- map.isJumpThrough

  --- Indique si la une tuile fait partie d'une échelle
  -- @param pTileId: Index de la tuile
  -- @return true ou false
  function lMap.isLadder (pTileId)
    ---- debugFunctionEnterNL("map.isLadder ", pTileId) -- ATTENTION cet appel peut remplir le log
    local tiles = lMap.tiles -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    if (tiles ~= nil and tiles[pTileId] ~= nil and tiles[pTileId].type == TILE_TYPE_LADDER) then return true end
    return false
  end -- map.isLadder

  --- Indique si la une tuile est invisible pour le joueur
  -- @param pTileId: Index de la tuile
  -- @return true ou false
  function lMap.isInvisible (pTileId)
    ---- debugFunctionEnterNL("map.isInvisible ", pTileId) -- ATTENTION cet appel peut remplir le log
    if (DEBUG_MODE > 0 and DEBUG_SHOW_MAP_INVISIBLE) then return false end
    local tiles = lMap.tiles -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    if (tiles ~= nil and tiles[pTileId] ~= nil and (
        tiles[pTileId].type == TILE_TYPE_GO_LEFT
        or tiles[pTileId].type == TILE_TYPE_GO_RIGHT
        or tiles[pTileId].type == TILE_TYPE_GO_UP
        or tiles[pTileId].type == TILE_TYPE_GO_DOWN
        or pTileId == TILE_SPECIAL_PLAYER_START
        or pTileId == TILE_SPECIAL_EMPTY
        or pTileId == TILE_SPECIAL_BLOB
      )
    ) then
      return true
    end
    return false
  end -- map.isInvisible

  -- Fonctions "virtuelles" remplacées dans les "classes enfants"
  -- ******************************

  --- Crée et positionne les contenus (objets, PNJs...) dans la map
  -- @param pDt Delta time
  function lMap.update (pDt)
    ---- debugFunctionEnterNL("map.update ", pDt) -- ATTENTION cet appel peut remplir le log
  end

  --- affiche la map
  function lMap.draw ()
    ---- debugFunctionEnterNL("map.draw ") -- ATTENTION cet appel peut remplir le log
  end

  --- Crée et positionne les contenus (objets, PNJs...) dans la map pour un niveau (de difficulté) donné
  -- @param pLevel (OPTIONNEL) niveau (de difficulté). Si absent, 1 sera utilisée.
  function lMap.createLevelContent (pLevel)
    -- debugFunctionEnter("map.createLevelContent ", pLevel)
  end

  -- Retourne une info sur la map, affichée dans l'écran de jeu
  -- @param pParam (OPTIONNEL) paramètre générique.
  -- @return le contenu à afficher
  function lMap.getHudInfo (pParam)
    -- debugFunctionEnter("map.getHudInfo ", pParam)
    -- par défaut, on retourne le nom du niveau de la map
    local content = "Level "..lMap.level
    if (lMap.names ~= nil and lMap.names[lMap.level] ~= nil) then
      content = content..lMap.names[lMap.level]
    end
    return content
  end

  -- Retourne des infos de debug info sur la map, affichée dans la zone de debug
  -- @return le contenu à afficher
  function lMap.getDebugInfo ()
    -- debugFunctionEnter("map.getDebugInfo ")
    -- par défaut, on ne retourne rien
    return ""
  end

  -- NOTE : LES FONCTIONS SPÉCIFIQUES AUX MAP LIÉES À UN TYPE DE JEU SONT DANS LES FICHIERS gameMap.lua SITUÉS DANS LES DOSSIERS "ASSETS\TYPE_XXX\MAP"

  -- initialisation par défaut
  lMap.initialize(pLevel)
  
  return lMap
 end -- createMap
