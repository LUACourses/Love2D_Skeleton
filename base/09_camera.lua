-- Entité représentant une camera
-- ****************************************

function createCamera ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createCamera")

  local lCamera = {}

  --- initialise la camera
  -- @param pvX (OPTIONNEL) vitesse en X. Si absent la valeur 0 sera utilisée.
  -- @param pvY (OPTIONNEL) vitesse en Y. Si absent la valeur 0 sera utilisée.
  function lCamera.initialize (pvX, pvY)
    debugFunctionEnter("camera.initialize ", pvX, pvY)
    if (pvX == nil) then pvX = 0 end
    if (pvY == nil) then pvY = 0 end
  -- position en X dans le viewport
    lCamera.x = 0
  -- position en Y dans le viewport
    lCamera.y = 0
    -- vélocité en X
    lCamera.vX = pvX
    -- vélocité en Y
    lCamera.vY = pvY
  end -- camera.initialize

  --- Déplace la caméra en fonction des vitesse en X et Y
  -- @param pDt delta time
  function lCamera.update (pDt)
    ---- debugFunctionEnterNL("camera.update ", pDt) -- ATTENTION cet appel peut remplir le log
    lCamera.x = lCamera.x + lCamera.vX * pDt
    lCamera.y = lCamera.y + lCamera.vY * pDt
  end -- camera.update

  lCamera.initialize()

  return lCamera
end -- createCamera