-- Entité représentant un PNJ
-- Utilise une forme d'héritage simple: npc qui inclus sprite
---- ---- ---- ---- ---- ---- ---- ----

-- TYPE DE PNJ
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
SPRITE_TYPE_NPC = SPRITE_TYPE..SPRITE_EXT_NPC -- un PNJ
SPRITE_TYPE_NPC_BOSS = SPRITE_TYPE_NPC..NEXT_EXT_BOSS  -- un BOSS

--- Crée un pseudo objet de type NPC (pnj)
-- @param pType type de pnj (voir les constantes SPRITE_TYPE_NPC_XXX).
-- @param pImage (OPTIONNEL) image principale.  Si absent la valeur par défaut des sprites sera utilisée.
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids du npc. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur 1 sera utilisée.
-- @return un pseudo objet NPC
function createNPC (pType, pImage, pX, pY, pWeight, pLife, pEnergy)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createNPC ", pType, pImage, pX, pY, pWeight, pLife, pEnergy)

  -- rappel:   createSprite(pImage,pX,pY,pvX,pvY,pType,pWeight     , pLife, pEnergy, pPoints,pCanCollideWithTile,pVelocityMax,pVelocityJump,pGravity,pCanMakeAction,pStatus                ,pMustDestroyedAtBorder, pTimeToLive,pMustDestroyedOnCollision,pSpeed)
  local lNpc = createSprite(pImage, pX, pY, nil, nil, pType, pWeight, pLife, pEnergy, nil    , false             , -1         , 0           , 0      , true         , SPRITE_STATUS_SLEEPING, true               , 0         , false                  , 0) -- important: les pnjs sont créés avec le status SPRITE_STATUS_SLEEPING par défaut, il faut donc les "réveiller" pour qu'ils inter agissent

  -- attributs spécifiques au pnj
  -- ******************************
  -- Identifiant du pnj
  lNpc.id = 1 -- modifié lors de l'ajout à la liste des PNJs

  --- Initialise le pnj
  function lNpc.initialize ()
    debugFunctionEnter("npc.initialize ")

    -- par défaut le pnj ne se déplace pas avec la camera mais avec son propre mouvement
    lNpc.mustMoveWithCamera = false

    lNpc.updateInitialValues() -- important
  end -- npc.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --- affiche le pnj
  lNpc.spriteDraw = lNpc.draw -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lNpc.draw ()
    ---- debugFunctionEnterNL("npc.draw") -- ATTENTION  cet appel peut remplir le log

    -- affiche des effets visuels spécifiques
    -- TODO : améliorer les effets visuels en fonction de l'état du sprite
    local effectColor, effectSize, mode
    local offsetX = 10
    local offsetY = 10
    local halfW = lNpc.width / 2
    local halfH = lNpc.height / 2
    local opacity = 60 / lNpc.initialValues.energy * lNpc.energy

    -- état normal, on affiche juste un bouclier représentant l'énergie du sprite
    -- cyan quasi transparent, plein
    effectSize = 1
    mode = "fill"
    effectColor = {128, 255, 255, opacity}

    -- affiche le pnj comme un sprite normal
     lNpc.spriteDraw()

    if (lNpc.status ~= SPRITE_STATUS_DESTROYING) then
      -- on n'affiche l'effet que si l'énergie est supérieure à 1
      if (lNpc.energy > 1 and lNpc.mustDisplayEnergy) then
        love.graphics.setColor(effectColor)
        love.graphics.setLineWidth(effectSize)
        love.graphics.ellipse (mode, lNpc.x + halfW, lNpc.y + halfH, halfW + offsetX, halfH + offsetY)
        love.graphics.setLineWidth(1)
      end

      love.graphics.setColor (255, 255, 255, 255)
    end

  end -- npc.draw

  --- Détruit le pnj
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lNpc.spriteDestroy = lNpc.destroy -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lNpc.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("npc.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)
    -- ?? if (lNPC.status ~= SPRITE_STATUS_NORMAL) then return end

    if (pMustAddScore == nil) then pMustAddScore = true end
    if (pMustShowFX == nil) then pMustShowFX = lNpc.mustDestroyedWithFX end
    if (pMustPlaySound == nil) then pMustPlaySound = lNpc.mustDestroyedWithSound end

    if (pMustAddScore) then
      player.addScore(lNpc.points)
    end
    if (pMustShowFX) then
      if (lNpc.type:find(SPRITE_EXT_BOSS) ~= nil) then
        -- un pnj Boss
        createEffect(SPRITE_TYPE_EFFECT_EXPLODE_BOSS, lNpc.x, lNpc.y, lNpc.width, lNpc.height)
      else
        -- pnj normal
        createEffect(SPRITE_TYPE_EFFECT_EXPLODE_NPC, lNpc.x, lNpc.y, lNpc.width, lNpc.height)
      end
    end
    if (pMustPlaySound) then
      lNpc.playSound("Destroy")
    end
    -- la destruction effective se fait dans la classe parent
    lNpc.spriteDestroy()
  end -- npc.destroy

  --- Actualise le PNJ
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpc.spriteUpdate = lNpc.update
  function lNpc.update (pDt)
    ---- debugFunctionEnterNL("npc.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lNpc.spriteUpdate(pDt)

    if (lNpc.status == SPRITE_STATUS_DESTROYING or lNpc.status == SPRITE_STATUS_DESTROYED) then return end

    -- si le sprite est immobile, il déplace le sprite avec la caméra
    if ((camera.vX > 0 or camera.vY > 0) and lNpc.checkIfMoveWithCamera()) then
      lNpc.x = lNpc.x + camera.vX * pDt
      lNpc.y = lNpc.y + camera.vY * pDt
    end

    if (lNpc.checkIfActive()) then
      -- le pnj peut il faire une action
      if (lNpc.checkIfCanMakeAction()) then
        -- oui, on décrémente le timer d'action du PNJ
        lNpc.actionTimer = lNpc.actionTimer - pDt
        if (lNpc.actionTimer <= 0.1) then
          -- si le timer est écoule, on réinitialise avec une valeur du actionDelai et le PNJ effectue une action (tire un projectile)
          lNpc.actionTimer = math.random(lNpc.actionDelai.min, lNpc.actionDelai.max)
          lNpc.action()
        end
      end
    end -- if (lNPC.status == SPRITE_STATUS_NORMAL) then

  end -- npc.update

  --- vérifie les collisions du PNJ (override)
  -- @return SPRITE_COLLISION_PLAYER, SPRITE_COLLISION_TILE_SOLID_LEFT, SPRITE_COLLISION_TILE_SOLID_RIGHT ,SPRITE_COLLISION_TILE_SOLID_TOP, SPRITE_COLLISION_TILE_SOLID_BOTTOM, SPRITE_COLLISION_LIMIT_LEFT, SPRITE_COLLISION_LIMIT_RIGHT ,SPRITE_COLLISION_LIMIT_TOP, SPRITE_COLLISION_LIMIT_BOTTOM ou SPRITE_COLLISION_NONE
  function lNpc.collide ()
    ---- debugFunctionEnterNL("npc.collide") -- ATTENTION cet appel peut remplir le log

    local collisionType = SPRITE_COLLISION_NONE
    local mathAbs = math.abs -- accélère le traitement car l'accès à une variable local est plus rapide que l'accès à une fonction externe

    local isWithinX = false
    local isWithinY = false
    local xMin, yMin, xMax, yMax = lNpc.getScreenLimits()
    local newX = lNpc.x
    local newY = lNpc.y
    local newVX = lNpc.vX
    local newVY = lNpc.vY

    -- vérifie si le PNJ est sorti de l'écran en xMin ou xMax
    if (lNpc.x < xMin) then
      -- doit il être détruit s'il touche la gauche ?
      if (lNpc.status ~= SPRITE_STATUS_SLEEPING and lNpc.mustDestroyedAtLeft) then
        -- oui, on le détruit
        lNpc.destroy(false, false, false)
      else
        -- non, on la vitesse en X devient positive
        newVX = mathAbs(lNpc.initialValues.vX)
        newX = xMin + OFFSET_WHEN_BOUNCE
      end
      collisionType = SPRITE_COLLISION_LIMIT_LEFT
    elseif (lNpc.x > xMax) then
      -- doit il être détruit s'il touche la droite ?
      if (lNpc.status ~= SPRITE_STATUS_SLEEPING and lNpc.mustDestroyedAtRight) then
        -- oui, on le détruit
        lNpc.destroy(false, false, false)
      else
        -- non, on la vitesse en X devient négative
        newVX = -mathAbs(lNpc.initialValues.vX)
        newX = xMax - OFFSET_WHEN_BOUNCE
      end
      collisionType = SPRITE_COLLISION_LIMIT_RIGHT
    else
      isWithinX = true
    end
    -- vérifie si le PNJ est sorti de l'écran en yMin ou yMax
    if (lNpc.y < yMin) then
      -- doit il être détruit s'il touche le haut ?
      if (lNpc.status ~= SPRITE_STATUS_SLEEPING and lNpc.mustDestroyedAtTop) then
        -- oui, on le détruit
        lNpc.destroy(false, false, false)
      else
        -- non, on la vitesse en Y devient positive
        newVY = mathAbs(lNpc.initialValues.vY)
        newY = yMin + OFFSET_WHEN_BOUNCE
      end
      collisionType = SPRITE_COLLISION_LIMIT_TOP
    -- vérifie si le PNJ est sorti de l'écran en yMax
    elseif(lNpc.y > yMax) then
      -- doit il être détruit s'il touche le bas ?
      if (lNpc.status ~= SPRITE_STATUS_SLEEPING and lNpc.mustDestroyedAtBottom) then
        -- oui, on le détruit
        lNpc.destroy(false, false, false)
      else
        -- non, on la vitesse en Y devient négative
        newVY = -mathAbs(lNpc.initialValues.vY)
        newY = yMax - OFFSET_WHEN_BOUNCE
      end
      collisionType = SPRITE_COLLISION_LIMIT_BOTTOM
    else
      isWithinY = true
    end

    -- le PNJ est-il endormi ?
    if (lNpc.status == SPRITE_STATUS_SLEEPING) then
      -- est-il dans la zone de jeu ?
      if (isWithinX and isWithinY) then
        -- oui, on le réveille
        lNpc.status = SPRITE_STATUS_NORMAL
        -- debugMessage("REVEIL pour "..lNPC.getID())
      end
    end
    if (lNpc.status ~= SPRITE_STATUS_SLEEPING) then
      -- non, on rectifie son état (vitesse et position) en fonction des limites de la zone de jeu
      lNpc.x = newX
      lNpc.y = newY
      lNpc.vX = newVX
      lNpc.vY = newVY
      -- debugMessage("PAs de reveil pour "-- lNPC.getID())
    end

    -- ce pnj collisionne t'il avec le joueur ?
    if (lNpc.checkIfCanCollide() and lNpc.collideWithSprite(player) ~= SPRITE_COLLISION_NONE) then
      -- oui
      -- si le pnj est vulnérable ou en mode "deadly", le joueur perd une vie (sauf s'il est en mode status "deadly"), et re-spawne
      if (lNpc.status == SPRITE_STATUS_DEADLY or lNpc.checkIfCanBeDamaged()) then player.loseEnergy(lNpc.energy, true, true) end
      -- si le player est vulnérable ou en mode "deadly", le PNJ perd une vie, et ne re-spawne pas
      if (player.status == SPRITE_STATUS_DEADLY or player.checkIfCanBeDamaged()) then lNpc.loseEnergy(player.energy, true, false) end
      collisionType = SPRITE_COLLISION_PLAYER
    end -- if (lNPC.checkIfCanCollide() and lNPC.collideWithSprite(player) ~= SPRITE_COLLISION_NONE) then

    return collisionType
  end -- npc.collide

  -- Autres fonctions
  -- ******************************

  --- le PNJ effectue une action (tire un projectile)
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpc.spriteAction = lNpc.action
  function lNpc.action ()
    -- debugFunctionEnter("npc.action")
    lNpc.spriteAction()
  end -- npc.action

  -- Ajout du pnj à la liste
  if (entities.NPCs ~= nil) then
    local npcIndex = #entities.NPCs + 1
    lNpc.id = npcIndex
    entities.NPCs[lNpc.id] = lNpc
  --debugMessage ( "ICI "..#entities["NPCs"], #entities.sprites)

  end

  -- initialisation par défaut
  lNpc.initialize()

  return lNpc
end -- createNPC