-- Entité représentant une animation
-- ****************************************

-- ÉTAT POSSIBLES
-- ATTENTION l'ajout ou la modification de ses valeurs peut entrainer des modifications dans les fonctions spritecheckIfCanXXX
-- étas de base pour le gameplay
ANIM_STATUS = "animStatus"
ANIM_STATUS_NORMAL = ANIM_STATUS..SEP_LAST.."normal"
ANIM_STATUS_ENDED = ANIM_STATUS..SEP_LAST.."ended"
ANIM_STATUS_STOPPED = ANIM_STATUS..SEP_LAST.."stopped"
ANIM_STATUS_DESTROYING = ANIM_STATUS..SEP_LAST.."destroying"
ANIM_STATUS_DESTROYED = ANIM_STATUS..SEP_LAST.."destroyed"

-- NOM DE L'ANIMATION
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
ANIM_NAME = "animName"
ANIM_NAME_NONE = ANIM_NAME..SEP_LAST.."none"
ANIM_NAME_EXT_MOVE = SEP_NEXT.."move" -- en rapport avec un mouvement
ANIM_NAME_EXT_ACT = SEP_NEXT.."act" -- en rapport avec une action
ANIM_NAME_EXT_STATUS = SEP_NEXT.."status" -- en rapport avec un état (ne devant pas être interrompu)

ANIM_NAME_MOVE = ANIM_NAME..ANIM_NAME_EXT_MOVE
ANIM_NAME_MOVE_IDLE = ANIM_NAME_MOVE..SEP_LAST.."idle"
ANIM_NAME_MOVE_WALK = ANIM_NAME_MOVE..SEP_LAST.."walk"
ANIM_NAME_MOVE_RUN = ANIM_NAME_MOVE..SEP_LAST.."run"
ANIM_NAME_MOVE_FLY = ANIM_NAME_MOVE..SEP_LAST.."fly"
ANIM_NAME_MOVE_FALL = ANIM_NAME_MOVE..SEP_LAST.."fall"
ANIM_NAME_MOVE_JUMP = ANIM_NAME_MOVE..SEP_LAST.."jump"
ANIM_NAME_MOVE_CLIMB = ANIM_NAME_MOVE..SEP_LAST.."climb"
ANIM_NAME_MOVE_CLIMB_IDLE = ANIM_NAME_MOVE..SEP_LAST.."climbIdle"

ANIM_NAME_ACT = ANIM_NAME..ANIM_NAME_EXT_ACT
ANIM_NAME_ACT_ACTION1 = ANIM_NAME_ACT..LAST_EXT_ACTION1
ANIM_NAME_ACT_ACTION2 = ANIM_NAME_ACT..LAST_EXT_ACTION2

ANIM_NAME_STATUS = ANIM_NAME..ANIM_NAME_EXT_STATUS
ANIM_NAME_STATUS_OPEN = ANIM_NAME_STATUS..SEP_LAST.."open"
ANIM_NAME_STATUS_CLOSE = ANIM_NAME_STATUS..SEP_LAST.."close"
ANIM_NAME_STATUS_EXPLODE = ANIM_NAME_STATUS..SEP_LAST.."explode"
ANIM_NAME_STATUS_LOSE_ENERGY = ANIM_NAME_STATUS..SEP_LAST.."loseEnergy"
ANIM_NAME_STATUS_LOSE_LIFE = ANIM_NAME_STATUS..SEP_LAST.."loseLife"
ANIM_NAME_STATUS_DESTROY = ANIM_NAME_STATUS..SEP_LAST.."destroy" -- valable aussi pour les animation de type explosion
ANIM_NAME_STATUS_SPAWN = ANIM_NAME_STATUS..SEP_LAST.."spawn"

--- Crée un pseudo objet de type animation
-- @param pName Nom de l'animation.
-- @param pFolder (OPTIONNEL) le chemin du sous répertoire contenant les images de l'animation
-- @param pExt (OPTIONNEL) extension des fichiers à utiliser. Si absent, ".png" sera utilisée.
-- @param pEntity entité à laquelle est liée l'animation.
-- @param pSpeed (OPTIONNEL) la durée d'une frame en secondes. Si absent, ANIMATION_DEFAULT_SPEED sera utilisée.
-- @param pIsLooping (OPTIONNEL) true pour jouer l'animation en boucle. Si absent, true sera utilisée.
-- @param pNextAnimationName (OPTIONNEL) nom de l'animation suivante. Si absent, ANIM_NAME_NONE sera utilisée et aucune animation ne sera jouée.
-- @param pSound (OPTIONNEL) son pour le lancement de l'animation. Si absent, aucun son ne sera joué.
-- @param pMustPlaySound (OPTIONNEL) faut il jouer le son pour le lancement de l'animation. Si absent, true sera utilisée.
-- @param pSoundIsLooping (OPTIONNEL) faut il jouer le son en boucle. Si absent, false sera utilisée.
-- @param pSoundEnd (OPTIONNEL) son pour la fin de l'animation. Si absent, Si absent, aucun son ne sera joué.
-- @param pMustPlaySoundEnd (OPTIONNEL) faut il jouer le son pour la fin de l'animation. Si absent, true sera utilisée.
-- @param pEntityStatusWhenRunning (OPTIONNEL) status de l'entité pendant l'animation.
-- @param pEntityStatusWhenFinished (OPTIONNEL) status de l'entité à la fin de l'animation.
-- @param pCallBackWhenFinished (OPTIONNEL) fonction à appeler à la fin de l'animation.
-- @return un pseudo objet animation
function createAnimation (pName, pFolder, pExt, pEntity, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createAnimation ", pName, pFolder, pExt, pEntity, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
  if (pSpeed == nil) then pSpeed = ANIMATION_DEFAULT_SPEED end
  if (pExt == nil) then pExt = ".png" end
  if (pIsLooping == nil) then pIsLooping = true end
  if (pMustPlaySound == nil) then pMustPlaySound = true end
  if (pMustPlaySoundEnd == nil) then pMustPlaySoundEnd = true end
  if (pSoundIsLooping == nil) then pSoundIsLooping = false end
  if (pNextAnimationName == nil) then pNextAnimationName = ANIM_NAME_NONE end

  local lAnimation = {}

  -- index de l'animation dans la table entities.animations (utile pour sa suppression)
  lAnimation.listIndex = 0 -- mis à jour lors de l'ajout entities.animations, ne pas remettre à 0 dans l'initialize
  -- nom
  lAnimation.name = pName
  -- extension du nom de fichier
  lAnimation.ext = pExt
  -- status
  lAnimation.status = ANIM_STATUS_NORMAL
  -- dossier contenant les images ET les sons par défaut
  lAnimation.folder = pFolder
  -- la durée d'une frame en secondes
  lAnimation.animationSpeed = pSpeed
  -- faut il jouer l'animation en boucle
  lAnimation.isLooping = pIsLooping
  -- son pour lancement
  lAnimation.sound = pSound
  -- faut il jouer le son pour le lancement
  lAnimation.mustPlaySound = pMustPlaySound
  -- faut il jouer le son en boucle
  lAnimation.soundIsLooping = pSoundIsLooping
  -- son pour la fin
  lAnimation.soundEnd = pSoundEnd
  -- faut il jouer le son son pour la fin de l'animation
  lAnimation.mustPlaySoundEnd = pMustPlaySoundEnd
  -- nom de l'animation suivante
  lAnimation.nextAnimationName = pNextAnimationName
  -- Ressources (images) composant l'animation
  lAnimation.resources = {}
  -- nombre de frames composant l'animation
  lAnimation.frameCount = 0 -- mis a jour lors du chargement des images
  -- index de la frame en cours (commence à 1)
  lAnimation.frame = 0
  -- timer pour la durée restante de la frame courante
  lAnimation.animationTimer = pSpeed
  -- l'animation courante doit elle être inversée ?
  lAnimation.flipedToRight = false
  -- entité à laquelle est liée l'animation
  lAnimation.entity = pEntity
  -- status de l'entité pendant l'animation
  lAnimation.entityStatusWhenRunning = pEntityStatusWhenRunning
  -- status de l'entité à la fin de l'animation
  lAnimation.entityStatusWhenFinished = pEntityStatusWhenFinished
  -- fonction à appeler à la fin de l'animation
  lAnimation.callBackWhenFinished = pCallBackWhenFinished
  -- couleur de l'animation. Si présente elle sera appliquée sur le sprite pendant l'animation
  lAnimation.color = nil

  --- Initialise l'objet
  -- @param pFolder le chemin du sous répertoire contenant les images de l'animation
  function lAnimation.initialize (pFolder)
    debugFunctionEnter("animation.initialize ", pFolder)
    lAnimation.loadResource(pFolder, lAnimation.ext)
    lAnimation.loadDefaultSounds()
  end -- animation.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lAnimation.destroy ()
    -- debugFunctionEnter("animation.destroy")
    lAnimation.status = ANIM_STATUS_DESTROYING
  end -- animation.destroy

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function lAnimation.clean ()
    ---- debugFunctionEnterNL("animation.clean")
    -- la suppression sera effective dans la boucle de animation.update
    lAnimation.status = ANIM_STATUS_DESTROYED
    -- suppression de l'animation dans le parent
    if (lAnimation.entity ~= nil and lAnimation.entity.animations ~= nil and lAnimation.entity.animations[lAnimation.name] ~= nil) then
      lAnimation.entity.animations[lAnimation.name] = nil
    end
  end -- animation.clean

  --- Joue une (nouvelle) animation
  -- @param pMustPlaySound (OPTIONNEL) true pour jouer le son lié à l'animation. Si absent la valeur true sera utilisée.
  function lAnimation.play (pPlaySound)
    -- debugFunctionEnter("animation.play ", pName)
    if (lAnimation.status == ANIM_STATUS_DESTROYING or lAnimation.status == ANIM_STATUS_DESTROYED) then return end
    if (pMustPlaySound == nil) then pMustPlaySound = true end
    lAnimation.animationTimer = lAnimation.animationSpeed
    lAnimation.frame = 1
    lAnimation.status = ANIM_STATUS_NORMAL
    if (lAnimation.sound ~= nil and lAnimation.mustPlaySound and pMustPlaySound) then
      lAnimation.sound:stop()
      lAnimation.sound:play()
    end
  end -- animation.play

  --- Joue une (nouvelle) animation
  -- @param  le nom de l'animation
  function lAnimation.stop ()
    -- debugFunctionEnter("animation.play ", pName)
    if (lAnimation.status == ANIM_STATUS_DESTROYING or lAnimation.status == ANIM_STATUS_DESTROYED) then return end
    -- on stop son son
    if (lAnimation.sound ~= nil) then lAnimation.sound:stop() end
    -- si elle a un son de fin, on le joue
    if (lAnimation.soundEnd ~= nil and lAnimation.mustPlaySoundEnd) then lAnimation.soundEnd:play() end
    lAnimation.status = ANIM_STATUS_STOPPED
  end -- animation.play

  --- actualise l'animation en cours
  -- @param pDt delta time
  -- @return le status de l'animation.
  function lAnimation.update (pDt)
    -- debugFunctionEnter("animation.update ", pDt) -- ATTENTION cet appel peut remplir le log
    if (lAnimation.status == ANIM_STATUS_DESTROYING or lAnimation.status == ANIM_STATUS_DESTROYED or lAnimation.status == ANIM_STATUS_STOPPED or lAnimation.status == ANIM_STATUS_ENDED) then return end
    lAnimation.animationTimer = lAnimation.animationTimer - pDt
    -- la durée de l'animation (vitesse) est elle écoulée ?
    if (lAnimation.animationTimer <= 0) then
      -- oui, on passe à la frame suivante
      lAnimation.frame = lAnimation.frame + 1
      lAnimation.animationTimer = lAnimation.animationSpeed
      if (lAnimation.entity ~= nil and lAnimation.entityStatusWhenRunning ~= nil) then lAnimation.entity.status = lAnimation.entityStatusWhenRunning end
      -- est ce la dernière ?
      if (lAnimation.frame > lAnimation.frameCount) then
        -- oui, l'animation est terminée
        -- on re passe à la 1ere frame par défaut (pour pouvoir la rejouer plus tard ou immédiatement)
        lAnimation.frame = 1
        if (lAnimation.soundEnd ~= nil and lAnimation.mustPlaySoundEnd) then
          lAnimation.sound:stop()
          lAnimation.soundEnd:play()
        end
        if (lAnimation.entity ~= nil and lAnimation.entityStatusWhenFinished ~= nil) then lAnimation.entity.status = lAnimation.entityStatusWhenFinished end
        if (lAnimation.entity ~= nil and lAnimation.callBackWhenFinished ~= nil) then lAnimation.callBackWhenFinished() end
        -- le callback a pu modifier l'animation courante
        if (lAnimation.status ~= ANIM_STATUS_NORMAL) then return lAnimation.status end
        -- doit on la rejouer ?
        if (lAnimation.isLooping) then
          -- oui, on la passe en normal
          lAnimation.status = ANIM_STATUS_NORMAL
        else
          -- non, on la termine
          lAnimation.status = ANIM_STATUS_ENDED
          -- l'animation suivante est elle définie ?

          if (lAnimation.nextAnimationName ~= nil and lAnimation.nextAnimationName ~= ANIM_NAME_NONE and lAnimation.entity.animations[lAnimation.nextAnimationName] ~= nil) then
            -- oui, on la joue
            lAnimation.entity.playAnimation(lAnimation.nextAnimationName)
          else
            -- non, on revient en IDLE
            lAnimation.entity.playAnimation(ANIM_NAME_MOVE_IDLE)
          end
        end -- if (not lAnimation.isLooping) then
      end -- if (lAnimation.frame > lAnimation.frameCount) then
    end -- if (lAnimation.animationTimer <= 0)
    return lAnimation.status
  end -- animation.updateAnimation

  -- Charge les sons par défaut liés à l'animation
  -- @param pExt (OPTIONNEL) extension des fichier à prendre en compte. Si absent, ".mp3" sera utilisé
  -- note: utilise le nom de l'animation en le décomposant pour retrouver le nom du fichier à associer
  function lAnimation.loadDefaultSounds ()
    -- debugFunctionEnter("animation.loadDefaultSounds")
    if (pExt == nil) then pExt = ".mp3" end
    local pos, sepLen, nameExt, filename
    -- recherche du séparateur indiquant le nom de l'animation
    nameExt = lAnimation.name
    pos = nameExt:find(SEP_LAST)
    if (pos ~= nil) then
      sepLen = #SEP_LAST
      nameExt = string.sub(nameExt, pos + 1)
    end
    nameExt = "/anim"..string.titleCase(nameExt)

    filename = lAnimation.folder..nameExt..pExt
    if (love.filesystem.isFile(filename)) then lAnimation.sound = resourceManager.getSound(filename, "static") else lAnimation.mustPlaySound = false end
    filename = lAnimation.folder..nameExt.."End"..pExt
    if (love.filesystem.isFile(filename)) then lAnimation.soundEnd = resourceManager.getSound(filename, "static") else lAnimation.mustPlaySoundEnd = false end
    if (lAnimation.sound ~= nil) then
      lAnimation.sound:setLooping(lAnimation.soundIsLooping)
    end
  end -- animation.loadDefaultSounds

  --- Charge les ressources (images) associées à une animation à partir d'un dossier
  -- @param pFolder le chemin du sous répertoire contenant les images de l'animation
  -- @param pExt (OPTIONNEL) extension des fichier à prendre en compte. Si absent, ".png" sera utilisé
  function lAnimation.loadResource (pFolder, pExt)
    debugFunctionEnter("animation.loadResource ", pFolder, pExt)
    if (pFolder == nil or pFolder =="") then return end
    if (not love.filesystem.isDirectory(pFolder)) then
      logError("l'animation "..lAnimation.name.." contenue dans "..pFolder.." n'a pas pu être ajoutée")
      return
    end
    lAnimation.resources = addResources(pFolder, {pExt})
    lAnimation.frameCount = #lAnimation.resources
  end -- animation.loadResource

  -- initialisation par défaut
  lAnimation.initialize(pFolder)

    -- Ajout de l'animation à la liste
  if (entities.animations ~= nil) then
    -- index de l'animation dans la table (utile pour sa suppression)
    lAnimation.listIndex = #entities.animations + 1
    entities.animations[lAnimation.listIndex] = lAnimation
  end

  return lAnimation, lAnimation.listIndex
end -- createAnimation