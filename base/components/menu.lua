-- entité représentant un menu
-- ****************************************

-- états possible pour un menu
MENU_STATUS = "menuStatus"
MENU_STATUS_UNDEFINED = MENU_STATUS..SEP_LAST.."undefined"
MENU_STATUS_HIDDEN = MENU_STATUS..SEP_LAST.."hidden"
MENU_STATUS_VISIBLE = MENU_STATUS..SEP_LAST.."visible"
MENU_STATUS_ANIMATION_SHOWING = MENU_STATUS..SEP_LAST.."animationShowing"
MENU_STATUS_ANIMATION_HIDING = MENU_STATUS..SEP_LAST.."animationHiding"
MENU_STATUS_ANIMATION_ENDED = MENU_STATUS..SEP_LAST.."animtationEnded"
MENU_ENTRY_NO_ANIMATION = "menuEntry"..SEP_LAST.."noAnimation"

--[[rappel: une entrée de menu
entry = {
  status
  text
  action
  actionParams OPTIONNEL
  tweening OPTIONNEL
  tweeningShow OPTIONNEL
  tweeningHide OPTIONNEL
}
]]

--- Crée un pseudo objet de type menu
-- @param pEntries entrées à ajouter au menu.
-- @param pEntity entité à laquelle est liée l'animation.
-- @param pUseTweening (OPTIONNEL) true pour utiliser.
-- @return un pseudo objet menu
function createMenu (pEntries, pEntity, pUseTweening)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createMenu ", pEntries, pEntity, pUseTweening)

  local lMenu = {}

  -- entité à laquelle est lié le menu
  lMenu.entity = nil
  -- liste des entrées de menu figurant sur l'écran
  lMenu.entries = {}
  -- liste des entrées de menu ayant terminé son animation
  lMenu.entriesEnded = 0
  -- entrée de menu sélectionnée
  lMenu.selection = 1
  -- état du menu (utile pour d'éventuelles animations)
  lMenu.status = MENU_STATUS_UNDEFINED
  -- le menu utilise t-il le tweening pour ces entrées ?
  lMenu.useTweening = false

  --- Initialise l'objet
  -- @param pEntries entrées à ajouter au menu.
  -- @param pEntity entité à laquelle est liée l'animation.
  -- @param pUseTweening (OPTIONNEL) true pour utiliser. Si absent, false sera utilisé.
  function lMenu.initialize (pEntries, pEntity, pUseTweening)
    debugFunctionEnter("menu.initialize ", pEntries, pEntity, pUseTweening)
    lMenu.entity = pEntity
    lMenu.entriesEnded = 0
    lMenu.status = MENU_STATUS_VISIBLE
    lMenu.useTweening = pUseTweening
    -- on ajoute les entrées 1 à 1 afin de les initialiser
    local i
    for i = 1, #lMenu.entries do
      lMenu.addEntry(pEntries[i])
    end -- for
  end -- menu.initialize

  --- Actualise l'objet
  -- @param pDt delta time
  function lMenu.update (pDt)
    ---- debugFunctionEnterNL("menu.update ", pDt) -- ATTENTION cet appel peut remplir le log
    -- le menu utilise il du tweening et les animations sont elles encore en cours ?
    if (lMenu.useTweening and lMenu.status ~= MENU_STATUS_ANIMATION_ENDED) then
      -- oui, actualise l'animation (tweening) de chaque entrée du menu
      local i, entry, result
      for i = 1, #lMenu.entries do
        entry = lMenu.entries[i]
        -- l'entrée de menu utilise t-elle un tweening ?
        if (entry.tweening ~= nil) then
          -- oui, on met à jour le tweening
          result = entry.tweening.update(pDt)
          -- est il terminé ?
          if (result == TWEENING_STATUS_ENDED) then
            -- oui, on compte le nombre d'animation restant à terminer
            lMenu.entriesEnded = lMenu.entriesEnded + 1
            -- tous les animations sont elles terminées ?
            if (lMenu.entriesEnded >= #lMenu.entries) then
              -- oui, est-ce l'animation de fin ?
              if (lMenu.status == MENU_STATUS_ANIMATION_HIDING) then
                -- oui, on valide le menu
                lMenu.valid()
              else
                -- non, on indique que les animations sont terminées
                lMenu.status = MENU_STATUS_ANIMATION_ENDED
              end
            end
          end
        end
      end -- for
    end -- if (lMenu.status ~= MENU_STATUS_ANIMATION_SHOWING) then
  end -- menu.update

 --- Dessine le menu
  -- @param pOffsetX (OPTIONNEL) décalage en X du menu par rapport au centre de l'écran. Si absent, 0 sera utilisée.
  -- @param pOffsetY (OPTIONNEL) décalage en Y du menu par rapport au centre de l'écran. Si absent, 0 sera utilisée.
  -- @param pFont (OPTIONNEL) police d'affichage. Si absent, la police courante sera utilisée.
  -- @param pColor (OPTIONNEL) couleur d'affichage.
  -- @return pOffsetX, pOffsetY mis à jour
  function lMenu.draw (pOffsetX, pOffsetY, pFont, pColor)
    ---- debugFunctionEnterNL("menu.draw ", pOffsetX, pOffsetY, pFont, pColor) -- ATTENTION cet appel peut remplir le log

    -- si le menu est caché, on ne fait rien
    if (lMenu.status == MENU_STATUS_HIDDEN) then return end

    local i, font, h, content, entry, tweenX, tweenY, tweening, position
    if (pFont == nil) then font = love.graphics.getFont() else font = pFont end

    h = font:getHeight(" ") -- alt+255 et non espace
    for i = 1, #lMenu.entries do
      entry = lMenu.entries[i]
      content = entry.text
      tweenX = 0
      tweenY = 0
      position = POS_CENTER
      if (i == lMenu.selection) then
        -- ajout des marqueurs du menu courant
        content = MENU_MARKER_LEFT..content..MENU_MARKER_RIGHT
      else
        -- ajout de marqueurs vide, nécessaire pour que le tweening soit centré
        content = "  "..content.."  "
      end
      -- l'entrée de menu utilise t-elle un tweening ?
      if (lMenu.useTweening and entry.tweening ~= nil) then
        -- oui, on récupère la valeur X actualisée par le tweening
        tweening = entry.tweening
        if (tweening.execute ~= nil and tweening.execute ~= "") then
          tweenX = tweening.execute(tweening.time, tweening.value, tweening.distance, tweening.duration)
          position = POS_CENTER_LEFT -- on se positionne à gauche pour le tweening
        end
      end -- if (entry.tweening ~= nil) then

      -- rappel: lHUD.displayText (pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)
      HUD.displayText(content, font, position, pOffsetX + tweenX, pOffsetY + tweenY, pColor)
      pOffsetY = pOffsetY + h
    end -- for

    -- ajoute une ligne vide en dessous du menu
    pOffsetY = pOffsetY + h
    return pOffsetX, pOffsetY
  end -- menu.draw

  --- Ajoute une entrée au menu
  -- @param pEntry entrée à ajouter.
  function lMenu.addEntry (pEntry)
    debugFunctionEnter("menu.addEntry ", pEntries)
    if (pEntry == nil) then return end
    lMenu.entries[#lMenu.entries + 1] = pEntry

    if (pEntry.tweeningShow ~= nil or pEntry.tweening ~= nil) then
      lMenu.status = MENU_STATUS_ANIMATION_SHOWING
      pEntry.status = TWEENING_STATUS_STARTED
    else
      pEntry.status = MENU_ENTRY_NO_ANIMATION
    end
    if (pEntry.tweening ~= nil) then pEntry.tweening.entity = pEntry end
    if (pEntry.tweeningShow ~= nil) then pEntry.tweeningShow.entity = pEntry end
    if (pEntry.tweeningHide ~= nil) then pEntry.tweeningHide.entity = pEntry end
  end -- menu.addEntry

  --- Supprime une entrée du menu
  -- @param pIndex index de l'entrée à supprimer.
  function lMenu.removeEntry (pIndex)
    debugFunctionEnter("menu.removeEntry ", pIndex)
    if (lMenu.entries[pIndex].tweening ~= nil and lMenu.entries[pIndex].tweening.status == MENU_STATUS_ENTRY_ENDED) then  lMenu.entriesEnded = lMenu.entriesEnded - 1 end
    lMenu.entries[pIndex] = nil
  end -- menu.removeEntry

  --- Navigation dans le menu
  -- @param pKey: touche du clavier enfoncée. Si absent, DUMMY_VALUE sera utilisée.
  function lMenu.navigate (pKey)
    ---- debugFunctionEnterNL("menu.navigate ", pKey) -- ATTENTION cet appel peut remplir le log
    if (pKey == nil) then pKey = DUMMY_VALUE end
    if (lMenu.entries == nil or lMenu.entries == {}) then return end

    if (pKey == settings.appKeys.menuUp) then
      if (sndMenuChange ~= nil) then sndMenuChange:stop(); sndMenuChange:play() end
      lMenu.selection = (lMenu.selection - 2) % (#lMenu.entries) + 1
    elseif (pKey == settings.appKeys.menuDown) then
      if (sndMenuChange ~= nil) then sndMenuChange:stop(); sndMenuChange:play() end
      lMenu.selection = (lMenu.selection) % (#lMenu.entries) + 1
    elseif (pKey == settings.appKeys.menuValid) then
      if (sndMenuValid ~= nil) then sndMenuValid:play() end
      lMenu.hide()
    end
  end -- menu.navigate

  --- Valide le menu
  function lMenu.valid ()
    -- non, on masque le menu et on lance l'action associée à l'entrée validée
    lMenu.status = MENU_STATUS_HIDDEN
    local entry = lMenu.entries[lMenu.selection]
    entry.action(entry.actionParams)
  end

  --- Affiche le menu (avec tweening ou pas)
  function lMenu.show ()
    ---- debugFunctionEnterNL("menu.show") -- ATTENTION cet appel peut remplir le log

    -- si le menu est en cours d'animation d'affichage, on ne fait rien
    if (lMenu.status == MENU_STATUS_ANIMATION_HIDING) then return end

    -- utilise t-il du tweening ?
    if (lMenu.useTweening) then
      -- oui, on initialise le tweening d'affichage pour chaque entrée du menu
      local i, entry
      for i = 1, #lMenu.entries do
        entry = lMenu.entries[i]
        -- si un tweening d'affichage existe, on l'utilise comme tweening courant
        if (entry.tweeningShow ~= nil) then entry.tweening = entry.tweeningShow end
      end -- for
      lMenu.status = MENU_STATUS_ANIMATION_SHOWING
    else
      -- non, on affiche juste le menu
      lMenu.status = MENU_STATUS_VISIBLE
    end -- if (lMenu.useTweening) then
  end -- menu.show

  --- Masque le menu (avec tweening ou pas)
  function lMenu.hide ()
    ---- debugFunctionEnterNL("menu.hide") -- ATTENTION cet appel peut remplir le log

    -- si le menu est en cours d'animation de masquage ou déjà masqué, on ne fait rien
    if (lMenu.status == MENU_STATUS_ANIMATION_HIDING or lMenu.status == MENU_STATUS_HIDDEN) then return end

    -- utilise t-il du tweening ?
    if (lMenu.useTweening) then
      -- oui, on initialise le tweening de masquage pour chaque entrée du menu
      local i, entry
      for i = 1, #lMenu.entries do
        entry = lMenu.entries[i]
        -- si un tweening de masquage existe, on l'utilise comme tweening courant
        if (entry.tweeningHide ~= nil) then entry.tweening = entry.tweeningHide end
      end -- for
      lMenu.status = MENU_STATUS_ANIMATION_HIDING
    else
      -- non, on valide le menu
      lMenu.valid()
    end -- if (lMenu.useTweening) then
  end -- menu.hide

  -- l'initialisation par défaut
  lMenu.initialize(pEntries, pEntity, pUseTweening)

  return lMenu
end -- createMenu