-- entité représentant un tweening
-- NOTE: nécessite le module interpolate qui contient les fonction d'interpolation
-- ****************************************
-- états possible pour un tweening
TWEENING_STATUS = "tweeningStatus"
TWEENING_STATUS_RUNNING = TWEENING_STATUS..SEP_LAST.."running"
TWEENING_STATUS_ENDED = TWEENING_STATUS..SEP_LAST.."ended"

--- Crée un pseudo objet de type tweening
-- @param pEntity entité à laquelle est liée le tweening.
-- @param pValue (OPTIONNEL) valeur de départ. Si absent, 0 sera utilisé.
-- @param pDistance (OPTIONNEL) distance à parcourir. Si absent, viewport.getWidth() / 2 sera utilisé.
-- @param pDuration (OPTIONNEL) durée du tweening en secondes. Si absent, 3 sera utilisé.
-- @param pFunctionToCall (OPTIONNEL) fonction de calcul devant être exécutée par défaut. Si absent, "easeInSine" sera utilisé. Permet de définir un appel générique.
-- @return un pseudo objet tweening
function createTweening (pEntity, pValue, pDistance, pDuration, pFunctionToCall)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createTweening ", pEntity, pValue, pDistance, pDuration, pFunctionToCall)

  local lTweening = {}

  -- index du tweening dans la table entities.tweenings (utile pour sa suppression)
  lTweening.listIndex = 0 -- mis à jour lors de l'ajout entities.tweenings, ne pas remettre à 0 dans l'initialize
  -- entité à laquelle est lié le tweening
  lTweening.entity = nil
  -- temps écoulé (t pour "time")
  lTweening.time = 0
  -- valeur de départ (b pour "begin")
  lTweening.value = 0
  -- distance à parcourir (c pour "change")
  lTweening.distance = 0
  -- durée du mouvement (d pour "duration")
  lTweening.duration = 0
  -- fonction de calcul devant être exécutée par défaut
  -- NOTE: c'est le nom d'une des fonction de calcul figurant dans la table
  lTweening.execute = nil

  --- Initialise l'objet
  -- @param pEntity entité à laquelle est liée le tweening.
  -- @param pValue valeur de départ (b pour "begin")
  -- @param pDistance distance à parcourir (c pour "change")
  -- @param pFunctionToCall (OPTIONNEL) fonction de calcul devant être exécutée par défaut. permet de définir un appel générique.
  function lTweening.initialize (pEntity, pValue, pDistance, pDuration, pFunctionToCall)
    debugFunctionEnter("tweening.initialize ", pEntity, pValue, pDistance, pDuration, pFunctionToCall)

    if (pValue == nil) then pValue = 0 end
    if (pDistance == nil) then pDistance = viewport.getWidth() / 2 end
    if (pDuration == nil) then pDuration = 3 end
    if (pFunctionToCall == nil) then pFunctionToCall = "easeInSine" end
    lTweening.time = 0
    lTweening.value = math.floor(pValue)
    lTweening.distance = math.floor(pDistance)
    lTweening.duration = pDuration
    lTweening.entity = pEntity
    lTweening.setExecute (pFunctionToCall)
  end -- tweening.initialize

  --- Actualise l'objet
  -- @param pDt delta time
  function lTweening.update (pDt)
    ---- debugFunctionEnterNL("tweening.update") -- ATTENTION cet appel peut remplir le log
    if (lTweening.time < lTweening.duration) then
      lTweening.time = lTweening.time + pDt
      return TWEENING_STATUS_RUNNING
    else
      return TWEENING_STATUS_ENDED
    end
  end -- tweening.update

  --- Définit la fonction execute (définit la fonction d'interpolation associée)
  -- @param pFunctionToCall fonction de calcul devant être exécutée par défaut. permet de définir un appel générique.
  function lTweening.setExecute (pFunctionToCall)
    debugFunctionEnter("tweening.setExecute ", pFunctionToCall)
    if (pFunctionToCall ~= nil and interpolate[pFunctionToCall] ~= nil and type(interpolate[pFunctionToCall]) == "function") then
      lTweening.execute = interpolate[pFunctionToCall]
    end
  end -- tweening.setExecute

  -- l'initialisation par défaut
  lTweening.initialize (pEntity, pValue, pDistance, pDuration, pFunctionToCall)

  -- Ajout du tweening à la liste
  if (entities.tweenings ~= nil) then
    -- index du tweening dans la table (utile pour sa suppression)
    lTweening.listIndex = #entities.tweenings + 1
    entities.tweenings[lTweening.listIndex] = lTweening
  end

  return lTweening
end -- createTweening