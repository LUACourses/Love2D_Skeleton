-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- AUTOLOAD
-- ****************************************

--- Ajoute les fichiers contenus dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les fichiers
-- @param pExtList (OPTIONNEL) tableau contenant les extensions des fichier à prendre en compte. Si absent, tous les fichiers seront listés.
-- @param pScanSubFolders (OPTIONNEL) true pour faire une recherche dans les sous-dossiers. Si absent, false sera utilisée.
-- @param pSortFiles (OPTIONNEL) true pour trier le résultat par ordre alphabétique croissant. Si absent, true sera utilisée.
-- @param pPrefix (OPTIONNEL) préfixe des fichier à prendre en compte. Si absent, tous les fichiers seront listés.
-- @return une table contenant le nom complet des fichiers trouvés dans le dossier
function listFolderContent (pFolder, pExtList, pScanSubFolders, pSortFiles, pPrefix)
  -- debugFunctionEnter("ListFolderContent ", pFolder, pExt, pScanSubFolders, pSortFiles, pPrefix)
  if (pExtList == nil) then pExtList = {} end
  if (pScanSubFolders == nil) then pScanSubFolders = false end
  if (pSortFiles == nil) then pSortFiles = true end
  if (pPrefix == nil) then pPrefix = "" end

  local key, value, fileNameLen, fileExt, filePrefix, pExtLen, pPrefixLen, pExt, i
  local allFiles = {}
  pPrefix = string.lower(pPrefix)
  pPrefixLen = string.len(pPrefix)

  -- liste tous les fichiers du répertoire
  local files = love.filesystem.getDirectoryItems(pFolder)

  for key, value in pairs(files) do
    local fileName = pFolder.."/"..value
    if (love.filesystem.isFile(fileName)) then
      -- si l'élément courant est un fichier
      -- compare l'extension du fichier et celles passées en paramètre
      for i = 1, #pExtList do
        pExt = string.lower(pExtList[i])
        pExtLen = string.len(pExt)
        fileNameLen = string.len(fileName)
        fileExt = string.sub(string.lower(fileName), -pExtLen)
        -- les extensions de fichier correspondent-elles ?
        if (pExt == nil or pExt == "" or fileExt == pExt) then
          --oui, on compare le préfixe du fichier et celui recherché
          filePrefix = string.sub(string.lower(value), 1, pPrefixLen)
          if (pPrefix == "" or filePrefix == pPrefix) then
            allFiles[#allFiles + 1] = fileName
          end
        end
      end -- for
    elseif (love.filesystem.isDirectory(fileName)) then
      -- si l'élément courant est un dossier
      -- on liste son contenu avec les même paramètres
      local folderContent = listFolderContent(fileName, pExtList , pScanSubFolders, pSortFiles, pPrefix)
      -- et on ajoute les fichiers qu'il contient à la liste
      local i
      for i = 1, #folderContent do
        allFiles[#allFiles + 1] = folderContent[i]
      end -- for
    end -- if (love.filesystem.isFile(fileName)) then
  end -- for
  if (pSortFiles) then
    -- trie les nom de fichiers (pour ordonner les images de l'animation)
    table.sort(allFiles)
  end
  return allFiles
end -- ListFolderContent

--- Charge automatiquement les fichier lua contenu dans un répertoire donné.
-- @param pFolder Nom du dossier à parcourir.
-- @param pScanSubFolders (OPTIONNEL) true pour faire une recherche dans les sous-dossiers. Si absent, false sera utilisée.
-- @param pPrefix (OPTIONNEL) préfixe des fichier à prendre en compte. Si absent, tous les fichiers seront listés.
function autoLoadRequire (pFolder, pScanSubFolders, pPrefix)
  -- debugFunctionEnter("autoLoadRequire ", pFolder, pScanSubFolders, pPrefix)
  if (pScanSubFolders == nil) then pScanSubFolders = true end
  local ext = ".lua"

  local luaFiles = listFolderContent(pFolder, {ext}, pScanSubFolders, false, pPrefix)
  local key, file, fullFileNameNoExt, i, fileName
  for i = 1, #luaFiles do
    -- on fait un require uniquement des fichier dont le nom ne contient pas la constante NO_AUTOLOAD_STRING
    fileName = luaFiles[i]
    if (fileName:find(NO_AUTOLOAD_STRING) == nil) then
      fullFileNameNoExt = fileName:gsub(ext, "")
      require(fullFileNameNoExt)
    end
  end -- for
end -- autoLoadRequire