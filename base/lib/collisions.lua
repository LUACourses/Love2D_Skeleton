-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- COLLISIONS
-- ****************************************

--- Vérifie si 2 sprites entre en collision (collision rectangulaire de la taille des sprites)
-- @param pSprite 1er sprite à vérifier.
-- @param pSprite 2e sprite à vérifier.
-- @param pStatusToIgnore Status de sprite devant être ignoré.
-- @return true ou false
function boxCollide (pSprite1, pSprite2, pStatusToIgnore)
  ---- debugFunctionEnterNL("boxCollide ", pSprite1, pSprite2, pStatusToIgnore) -- ATTENTION  cet appel peut remplir le log
  if (pStatusToIgnore == nil) then
    pStatusToIgnore = "ZZXXZZ" -- ainsi, la condition sera toujours ignorée si le status à tester est nil
  end
  if (
    (pSprite1 == pSprite2)
    or (pSprite1.status == pStatusToIgnore)
    or (pSprite2.status == pStatusToIgnore)
  )
  then
    return false
  end
  if (pSprite1.x < pSprite2.x + pSprite2.width and
    pSprite2.x < pSprite1.x + pSprite1.width and
    pSprite1.y < pSprite2.y + pSprite2.height and
    pSprite2.y < pSprite1.y + pSprite1.height
    ) then
    return true
  end
  return false
end -- boxCollide

--- Détermination des points entourant une série de point d'une liste de points.
-- NOTE: Permet de calculer la forme englobante. Utile pour les collisions de forme complexes
-- @param pVertices Liste des sommets (paires de coordonnées sous la forme {x,y}) du polygone
-- @return Liste des sommets triés pour créer un polygone convexe
function createConvexHull (pVertices)
  -- Andrew's monotone chain convex hull algorithm
  -- https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
  -- Direct port from Javascript version
  local p = #pVertices

  local cross = function(p, q, r)
    return (q[2] - p[2]) * (r[1] - q[1]) - (q[1] - p[1]) * (r[2] - q[2])
  end

  table.sort(pVertices, function(a, b)
    return a[1] == b[1] and a[2] > b[2] or a[1] > b[1]
  end)

  local lower = {}
  for i = 1, p do
    while (#lower >= 2 and cross(lower[#lower - 1], lower[#lower], pVertices[i]) <= 0) do
      table.remove(lower, #lower)
    end

    table.insert(lower, pVertices[i])
  end

  local upper = {}
  for i = p, 1, -1 do
    while (#upper >= 2 and cross(upper[#upper - 1], upper[#upper], pVertices[i]) <= 0) do
      table.remove(upper, #upper)
    end

    table.insert(upper, pVertices[i])
  end

  table.remove(upper, #upper)
  table.remove(lower, #lower)
  for _, point in ipairs(lower) do
    table.insert(upper, point)
  end

  return upper
end