-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- DEBUG et LOG
-- ****************************************

--- Affiche le contenu d'une variable
-- @param pVar variable à afficher
function dump (pVar)
  if (pVar == nil) then
    print "NIL"
  elseif (type(pVar) == "table") then
    for k, v in pairs(pVar) do
      print(tostring(k).." = "..tostring(v))
    end
  else
    print(pVar)
  end
end -- dump

--- Retourne TRUE si une une variable est égale à une valeur donnée, FALSE sinon et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
-- @return true si l'assertion est vraie, false sinon
function assertEqual (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
      print ("assertEqual: ", pMessage, " Value=", tostring(pValue))
    end
    return true
  else
    return false
  end
end -- assertEqual

--- Quitte le jeu si une une variable est égale à une valeur donnée, et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
function assertEqualQuit (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
       print ("assertEqualQuit: ", pMessage, " Value=", tostring(pValue))
    end
    print ("ARRET SUR VALIDATION D'UNE ASSERTION")
    love.event.quit()
  end
end -- assertEqualQuit

--- Logue (Affiche) un message d'erreur
-- @param pMessage texte à loguer
-- @param mustQuit (OPTIONNEL) si vrai, termine le jeu après avoir afficher le message
function logError (pMessage, pMustQuit)
  print (pMessage)
  if (pMustQuit) then love.event.quit() end
end -- logError

--- Logue (Affiche) des contenus pour du debug
-- @param Paramètres dynamiques
function debugMessage (...)
  if (DEBUG_MODE >= 1) then
    print(os.date().."# INFO #", ...)
  end
end -- debugMessage

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction SANS les appels qui peuvent remplir le log
-- @param Paramètres dynamiques
function debugFunctionEnter (...)
  if (DEBUG_MODE >= 4) then
    print(os.date().."# ENTREE FONCTION #", ...)
  end
end -- debugFunctionEnter

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction AVEC les appels qui peuvent remplir le log
-- @param Paramètres dynamiques
function debugFunctionEnterNL (...)
  if (DEBUG_MODE >= 5) then
    print(os.date().."# ENTREE FONCTION #", ...)
  end
end -- debugFunctionEnterNL

---Affiche un point rouge au coordonnées données
-- @param pX position X du point
-- @param pY position Y du point
function debugPoint (pX, pY)
  love.graphics.setColor(255, 0, 0, 255)
  love.graphics.rectangle("fill", pX, pY, 10, 10)
  love.graphics.setColor(255, 255, 255, 255)
end -- debugPoint

--- Logue (Affiche) des contenus d'information
-- @param Paramètres dynamiques
function logMessage (...)
  print(os.date().."# LOG #", ...)
end -- logMessage