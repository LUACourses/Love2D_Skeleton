-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- FICHIERS
-- ****************************************

--- retourne le path d'un nom de fichier complet
-- @param @fileName nom du fichier à analyser
-- @return le path du fichier
function getFilePath (fileName)
    return fileName:match("(.*[/\\])")
end

--- retourne le nom de fichier d'un nom de fichier complet
-- @param @fileName nom du fichier à analyser
-- @return le nom du fichier
function getFileName (fileName)
  return fileName:match("^.+/(.+)$")
end

--- retourne l'extension d'un nom de fichier complet
-- @param @fileName nom du fichier à analyser
-- @return l'extension du fichier
function getFileExtension (fileName)
  local result = fileName:match("^.+(%..+)$")
  result = result:sub(2)
  return result
end

-- lit un fichier texte et retourne son contenu
-- @param @fileName nom du fichier à lire
-- @return le contenu du fichier sous forme d'une table
function readTextFile (pFileName)
  local line
  local fileContent = {}
  for line in love.filesystem.lines(pFileName) do
    fileContent[#fileContent + 1] = line
  end
  return fileContent
end