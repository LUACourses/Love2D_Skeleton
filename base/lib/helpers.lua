-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- HELPERS
-- ****************************************

--- Remplace l'instruction switch
--[[
  USAGE:
  switch(case) {
    [1] = function () print"number 1!" end,
    [2] = math.sin,
    [false] = function (a) return function (b) return (a or b) and not (a and b) end end,
    Default = function (x) print"Look, Mom, I can differentiate types!" end, -- ["Default"] ;)
    [Default] = print,
    [Nil] = function () print"I must've left it in my other jeans." end,
  }
]]
Default, Nil = {}, function () end --- for uniqueness
function switch (pIndex)
  return setmetatable({pIndex}, {
    __call = function (t, cases)
      local item = #t == 0 and Nil or t[1]
      return (cases[item] or cases[Default] or Nil)(item)
    end
  })
end -- switch

--- Itérateur pour une table FINIE
--[[
USAGE 1:
  t = {10, 20, 30}
  iter = iterator(t)    -- creates the iterator
  while true do
    local element = iter()   -- calls the iterator
    if element == nil then break end
    print(element)
  end
USAGE 2:
  t = {10, 20, 30}
  for element in iterator(t) do
    print(element)
  end
]]
function iterator (pTable)
  local i = 0
  local n = table.getn(pTable)
  return function ()
    i = i + 1
    if i <= n then return pTable[i] end
  end
end -- iterator

--- retourne le contenu du dernier suffixe d'une chaîne utilisant un construction composée (comme les types de sprites)
-- @param pType Chaîne à analyser.
-- @return contenu du suffixe (sans le séparateur) ou "" si non trouvé.
function getLastSuffix (pType)
  local pos = pType:find(SEP_LAST)
  local sepLastLen = #SEP_LAST
  local result = ""
  if (pos ~= nil) then
    result = pType:sub(pos + sepLastLen)
  end
  return result
end