  -- Regroupe des fonctions d'interpolation (utilisée dans le tweening)
-- ****************************************
--- plus d'infos et visualisation de formules sur le tweening sur http://gizma.com/easing/

interpolate = {}
  --- Interpolation linéaire: pas d'accélération
  function interpolate.linearTween (t, b, c, d)
    return c * t / d + b
  end

  --- Interpolation Sinusoïdale IN: accélération de une vitesse nulle
  function interpolate.easeInSine (t, b, c, d)
    return -c * math.cos(t / d * (math.pi / 2)) + c + b
  end

  --- Interpolation Sinusoïdale OUT: décélération jusqu'à une vitesse nulle
  function interpolate.easeOutSine (t, b, c, d)
    return c * math.sin(t / d * (math.pi / 2)) + b
  end

  --- Interpolation Sinusoïdale INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutSine (t, b, c, d)
    return -c / 2 * (math.cos(math.pi * t / d) - 1) + b
  end

  --- Interpolation Exponentielle IN: accélération depuis une vitesse nulle
  function interpolate.easeInExpo (t, b, c, d)
    return c * math.pow(2, 10 * (t / d - 1)) + b
  end

  --- Interpolation Exponentielle OUT: décélération jusqu'à une vitesse nulle
  function interpolate.easeOutExpo (t, b, c, d)
    return c * (-math.pow(2, -10 * t / d) + 1) + b
  end

  --- Interpolation Exponentielle INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutExpo (t, b, c, d)
    t = t / 2
    if (t < 1) then return c / 2 * math.pow(2, 10 * (t - 1)) + b end
    t = t - 1
    return c / 2 * (-math.pow(2, -10 * t) + 2) + b
  end

  --- Interpolation Quadratique IN: accélération depuis une vitesse nulle
  function interpolate.easeInQuad (t, b, c, d)
    t = t / d
    return c * t * t + b
  end

  --- Interpolation Quadratique OUT: décélération jusqu'à une vitesse nulle
  function interpolate.easeOutQuad (t, b, c, d)
    t = t / d
    return -c * t * (t - 2) + b
  end

  --- Interpolation Quadratique INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutQuad (t, b, c, d)
    t = t / d / 2
    if (t < 1) then return c / 2 * t * t + b end
    t = t - 1
    return - c / 2 * (t * (t - 2) - 1) + b
  end

  --- Interpolation Cubique IN: accélération depuis une vitesse nulle
  function interpolate.easeInCubic (t, b, c, d)
    t = t / d
    return c * t * t * t + b
  end

  --- Interpolation Cubique OUT: décélération jusqu'à une vitesse nulle
  function interpolate.easeOutCubic (t, b, c, d)
    t = t / d
    t = t - 1
    return c * (t * t * t + 1) + b
  end

  --- Interpolation Cubique INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutCubic (t, b, c, d)
    t = t / d / 2
    if (t < 1) then return c / 2 * t * t * t + b end
    t = t - 2
    return c / 2 * (t * t * t + 2) + b
  end

  --- Interpolation Quartique IN: accélération depuis une vitesse nulle
  function interpolate.easeInQuart (t, b, c, d)
    t = t / d
    return c * t * t * t * t + b
  end

  --- Interpolation Quartique OUT: - décélération jusqu'à une vitesse nulle
  function interpolate.easeOutQuart (t, b, c, d)
    t = t / d
    t = t - 1
    return -c * (t * t * t * t - 1) + b
  end

  --- Interpolation Quartique INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutQuart (t, b, c, d)
    t = t / d / 2
    if (t < 1) then return c / 2 * t * t * t * t + b end
    t = t - 2
    return -c / 2 * (t * t * t * t - 2) + b
  end

  --- Interpolation Quintique IN: accélérer depuis une vitesse zéro
  function interpolate.easeInQuint (t, b, c, d)
    t = t / d
    return c * t * t * t * t * t + b
  end

  --- Interpolation Quintique OUT: décélération à une vitesse nulle
  function interpolate.easeOutQuint (t, b, c, d)
    t = t / d
    t = t - 1
    return c * (t * t * t * t * t + 1) + b
  end

  --- Interpolation Quintique INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutQuint (t, b, c, d)
    t = t / d / 2
    if (t < 1) then return c / 2 * t * t * t * t * t + b end
    t = t - 2
    return c / 2 * (t * t * t * t * t + 2) + b
  end

  --- Interpolation Circulaire IN: accélérer depuis une vitesse nulle
  function interpolate.easeInCirc (t, b, c, d)
    t = t / d
    return -c * (math.sqrt(1 - t * t) - 1) + b
  end

  --- Interpolation Circulaire OUT: décélération jusqu'à une vitesse nulle
  function interpolate.eeOutCirc (t, b, c, d)
    t = t / d
    t = t - 1
    return c * math.sqrt(1 - t * t) + b
  end

  --- Interpolation Circulaire INOUT: accélération jusqu'à mi-course, puis décélération
  function interpolate.easeInOutCirc (t, b, c, d)
    t = t / d 
    if (t < 1) then return -c / 2 * (math.sqrt(1 - t * t) - 1) + b end
    t = t - 2
    return c / 2 * (math.sqrt(1 - t * t) + 1) + b
  end