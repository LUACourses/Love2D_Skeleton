-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- MATHS
-- ****************************************

--- Retourne l'angle en radiant de la droite passant par 2 points (utile pour les déplacements en X et Y)
-- Ajoutée au module math
-- usage:
-- pSprite.angle = math.angle(pSprite.x, pSprite.y, pSprite2.x, pSprite2.y)
-- vX = speed * math.cos(pSprite.angle)
-- vY = speed * math.sin(pSprite.angle)
-- @param pX1 position X du point 1
-- @param pY1 position Y du point 1
-- @param pX2 position X du point 2
-- @param pY2 position Y du point 2
-- @return angle en radiant
function math.angle (pX1, pY1, pX2, pY2)
  return math.atan2(pY2 - pY1, pX2 - pX1)
end -- math.angle

-- Retourne la distance entre 2 points.
-- Ajoutée au module math
-- @param pX1 position X du point 1
-- @param pY1 position Y du point 1
-- @param pX2 position X du point 2
-- @param pY2 position Y du point 2
-- @return distance en pixel
function math.dist (x1, y1, x2, y2)
  return ((x2 - x1)^ 2  + (y2 - y1) ^ 2) ^ 0.5
end
