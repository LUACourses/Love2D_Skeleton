-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- RESSOURCES
-- ****************************************

--- Créer un objet image (love) en vérifiant si le fichier existe
-- @param filename Nom du fichier
-- @return l'objet image ou nil si le fichier n'existe pas
function newImageIfExists (filename)
  if (filename ~= nil and filename ~= "" and love.filesystem.isFile(filename)) then
    return love.graphics.newImage(filename)
  else
    return nil
  end
end -- newImageIfExists

--- Ajoute les images contenues dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les images
-- @param pExtList (OPTIONNEL) tableau contenant les extensions des fichier à prendre en compte. Si absent, {".png"} sera utilisé
-- @return une table contenant les images associées aux fichiers images trouvés dans le dossier
function addImages (pFolder, pExtList)
  -- debugFunctionEnter("addImages ", pFolder, pExtList)
  if (pExtList == nil or pExtList == {}) then pExtList  = {".png"} end
  local imageList = {}
  local imageTypeList = {}
  local allFiles = listFolderContent(pFolder, pExtList, false, true)
  -- ajoute les images à la table des images retournées
  local i
  for i = 1, #allFiles do
    local index = #imageList + 1
    imageList[index] = resourceManager.getImages(allFiles[i])
  end

  return imageList
end -- addImages

--- Ajoute les resources contenues dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les ressources
-- @param pExtList (OPTIONNEL) tableau contenant les extensions des fichier à prendre en compte.
-- @return une table contenant les ressources associées aux fichiers trouvés dans le dossier
function addResources (pFolder, pExtList)
  -- debugFunctionEnter("addResources ", pFolder, pExtList)
  local resourceList = {}
  local allFiles = listFolderContent(pFolder, pExtList, false, true)
  -- ajoute les images à la table des images retournées
  local i
  for i = 1, #allFiles do
    local index = #resourceList + 1
    resourceList[index] = resourceManager.get(allFiles[i]) -- type de ressource déterminé automatiquement par l'extension de fichier
  end

  return resourceList
end -- addResources