-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- CHAINES
-- ****************************************

--- fonction interne
function _titleCase (first, rest)
   return first:upper()..rest:lower()
end
--- met la 1ere lettre des mots d'une chaîne en majuscule
-- @param pString chaîne de caractères à modifier.
-- @return chaîne modifiée
function string.titleCase (pString)
  if (pString == nil or #pString < 1) then return pString end
  return string.gsub(pString, "(%a)([%w_']*)", _titleCase)
end

--- supprime les espaces dans une chaîne
-- @param pString chaîne de caractères à modifier.
-- @return chaîne modifiée
function string.trim (pString)
  if (pString == nil or #pString < 1) then return pString end
  return pString:match"^%s*(.*)":match"(.-)%s*$"
end

--- supprime les caractères invalide d'une chaîne
-- @param pString chaîne de caractères à modifier.
-- @return chaîne modifiée
function string.sanitize (pString)
  if (pString == nil or #pString < 1) then return pString end
  -- supprime tout caractère qui n'est pas alphanumérique
  local result = string.gsub(pString, "[^%w]", "")
  return result
end

--- découpe une chaîne en fonction d'un séparateur
-- @param pString chaîne de caractères à modifier.
-- @param pDelimiter séparateur. Si absent, "," sera utilisée
-- @return table contenant les éléments séparés
--[[
  usage
  t = split("a,b,c,d,e,f,g",",")
  for i,j in pairs(t) do
    print(i,j)
  end
]]
function string.split (pString, pDelimiter)
  local fields = {}
  if (pDelimiter == nil) then pDelimiter = "," end
  local pattern = string.format("([^%s]+)", pDelimiter)
  string.gsub(pString, pattern, function(c) fields[#fields + 1] = c end)
  return fields
end
