-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- TABLES
-- ****************************************

--- Mélange le contenu d'une table
-- @param pTable table à mélanger
-- @return la table mélangée
-- @see https://forums.coronalabs.com/topic/195-random-table-shuffle/
function table.shuffle (pTable)
  local count = #pTable
  for i = 1, (count * 20) do
    local index0 = math.random(1, count)
    local index1 = math.random(1, count)
    local temp = pTable [index0]
    pTable[index0] = pTable [index1]
    pTable[index1] = temp
  end
  return pTable
end -- table.shuffle

--- merge le contenu de 2 tables
-- @param pFirstTable 1ere table. Son contenu sera modifié
-- @param secondTable 2e table
-- @return la 1ere table avec en plus le contenu de la seconde
function table.merge (pFirstTable, pSecondTable)
  for k,v in pairs(pSecondTable) do
    pFirstTable[k] = v
  end
  return pFirstTable
end -- table.merge

--- Met à nil tous les éléments d'une table NON recursivement
-- @param pTable table à effacer.
function table.empty (pTable)
  if (pTable == nil or pTable == {}) then return end
  local index, value
  for index, value in pairs(pTable) do
    value = nil
  end
end -- table.empty

--- récupère les paires variable = valeur contenues dans une table
-- @param pTable table à analyser.
-- @param pDelimiter séparateur entre chaque variable et sa valeur. Si absent, "=" sera utilisée.
-- @return table contenant les éléments sous la forme {variable=valeur,variable=valeur...}
function table.parseValues (pTable, pDelimiter)
  if (pDelimiter == nil) then pDelimiter = "=" end
  local i, var, value
  local result = {}
  -- boucle sur les entrées de la table
  for i = 1, #pTable do
    -- sépare les paires variable = valeur
    local temp = pTable[i]:split("=")
    var = temp[1]
    value = temp[2]
    -- supprime les espaces
    var = var:trim()
    value = value:trim()
    result[var] = value
  end -- for i
  return result
end -- table.parseValues