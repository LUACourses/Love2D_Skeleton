-- Regroupe les constantes nécessaires au framework
-- Elles ne devraient pas être modifiées pendant l'éxécution
-- ****************************************

-- configuration du DEBUG
-- ******************************
-- Définit des niveau d'affichage des information de debug (affiche des infos supplémentaires)
-- 0: aucun
-- 1: messages affichés via la fonction debugMessage
-- 2: comme précédent mais affiche aussi les infos de debug dans le HUD (variable debugInfos)
-- 3: comme précédent mais affiche aussi les identifiants des sprites
-- 4: comme précédent mais affiche aussi les messages affichés via la fonction debugFunctionEnter (SANS ceux qui peuvent remplir le log)
-- 5: comme précédent mais affiche aussi les messages affichés via la fonction debugFunctionEnterNL (AVEC ceux qui peuvent remplir le log)
DEBUG_MODE = 2
-- position X de la fenêtre du jeu en mode DEBUG
DEBUG_WINDOWS_X = 0
-- position Y de la fenêtre du jeu en mode DEBUG
DEBUG_WINDOWS_Y = 0
-- écran sur lequel est positionné la fenêtre du jeu en mode DEBUG
DEBUG_DISPLAY_NUM = 2
-- DEBUG_DISPLAY_NUM = love.window.getDisplayCount() -- le dernier écran
-- permet d'afficher les éléments invisibles de la carte
DEBUG_SHOW_MAP_INVISIBLE = false
-- permet d'afficher les collisions
DEBUG_SHOW_COLLISIONS = true

-- affiche directement l'écran de jeu sans passer par le menu
DEBUG_START_WITH_PLAYSCREEN = true

-- séparateurs utilisés dans la construction des constantes combinées
SEP_FIRST = "@@" -- 1er séparateur: fin du type de base
SEP_NEXT = "##" -- séparateurs intermédiaire (hiérarchie des sous-classes du type de base)
SEP_LAST = "_" -- dernier séparateur: détail de l'asset (numéro ou suffixe spécifique)

-- suffixes utilisés dans la construction des constantes combinées
NEXT_EXT_LIMIT = SEP_NEXT.."limit" -- en rapport avec une limite de la zone de jeu
NEXT_EXT_TILE = SEP_NEXT.."tile" -- en rapport avec une tuile
NEXT_EXT_SOLID = SEP_NEXT.."solid" -- en rapport avec un élément solide
NEXT_EXT_JUMPTHROUGH  = SEP_NEXT.."jumpthrough " -- en rapport avec un élément pouvant être traversé en sautant
NEXT_EXT_WATER = SEP_NEXT.."water" -- en rapport avec un élément liquide
NEXT_EXT_DAMAGE = SEP_NEXT.."damage" -- en rapport avec un élément qui cause des dommages
NEXT_EXT_DEADLY = SEP_NEXT.."deadly" -- en rapport avec un élément mortel

NEXT_EXT_CHARACTER = SEP_NEXT.."character" -- en rapport avec un personnage (joueur)
NEXT_EXT_PLAYER = SEP_NEXT.."player"  -- en rapport avec le joueur (courant)
NEXT_EXT_NPC = SEP_NEXT.."npc" -- en rapport avec un pnj
NEXT_EXT_PROJ = SEP_NEXT.."projectile" -- en rapport avec un projectile
NEXT_EXT_ITEM = SEP_NEXT.."item" -- en rapport avec un  élément de décor
NEXT_EXT_EFFECT = SEP_NEXT.."effect" -- en rapport avec un effect
NEXT_EXT_BOSS = SEP_NEXT.."boss"  -- en rapport avec boss
NEXT_EXT_ELT = SEP_NEXT.."elt" -- en rapport avec élément de décors

NEXT_EXT_EXPLODE = SEP_NEXT.."explosion" -- une explosion
NEXT_EXT_EXIT = SEP_NEXT.."exit" -- sortie, passage au niveau suivant si touché et ouverte
NEXT_EXT_COLLECT = SEP_NEXT.."collect" -- élément de décor collectable
NEXT_EXT_MOVEFIXED = SEP_NEXT.."fixe" -- pas de déplacement
NEXT_EXT_MOVEGUIDED = SEP_NEXT.."moveguided" -- déplacement guidé par des éléments de la carte
NEXT_EXT_MOVEFALL = SEP_NEXT.."movefall" -- déplacement déplacement vers le bas sans rebond
NEXT_EXT_MOVEBOUNCE = SEP_NEXT.."movebounce" -- déplacement avec rebond sur les bords
NEXT_EXT_MOVEIA = SEP_NEXT.."IA" -- déplacements gérés par une IA

LAST_EXT_UNIQ = SEP_LAST.."uniq"
LAST_EXT_NONE = SEP_LAST.."none"
LAST_EXT_UP = SEP_LAST.."up"
LAST_EXT_RIGHT = SEP_LAST.."right"
LAST_EXT_DOWN = SEP_LAST.."down"
LAST_EXT_LEFT = SEP_LAST.."left"
LAST_EXT_TOP = SEP_LAST.."top"
LAST_EXT_BOTTOM = SEP_LAST.."bottom"
LAST_EXT_LEFTDOWN = SEP_LAST.."leftdown"
LAST_EXT_RIGHTDOWN = SEP_LAST.."rightdown"
LAST_EXT_ACTION1 = SEP_LAST.."action1"
LAST_EXT_ACTION2 = SEP_LAST.."action2"

LAST_EXT_1 = SEP_LAST.."1"
LAST_EXT_2 = SEP_LAST.."2"
LAST_EXT_3 = SEP_LAST.."3"
LAST_EXT_4 = SEP_LAST.."4"
LAST_EXT_5 = SEP_LAST.."5"
LAST_EXT_6 = SEP_LAST.."6"
LAST_EXT_7 = SEP_LAST.."7"
LAST_EXT_8 = SEP_LAST.."8"
LAST_EXT_9 = SEP_LAST.."9"
LAST_EXT_10 = SEP_LAST.."10"

-- TYPE DE JEUX POSSIBLES (utilisés pour gérer des gameplay et de map différents)
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
GAME_TYPE = "gameType"
GAME_SIDE = SEP_NEXT.."side"
GAME_TOPDOWN = SEP_NEXT.."topdown"
GAME_TYPE_PLATEFORMER = SEP_NEXT.."plateformer"
GAME_TYPE_SHOOTER = SEP_NEXT.."shooter"

GAME_TYPE_DEFAULT = GAME_TYPE..SEP_NEXT.."default"
GAME_TYPE_SIDE = GAME_TYPE..GAME_SIDE
GAME_TYPE_TOPDOWN = GAME_TYPE..GAME_TOPDOWN

-- tableau contenant le nom associé à chaque jeu présent dans le framework, indexé par l'ordre alphabétique des dossiers des jeux dans le dossier assets. 
-- NOTE: les différents GAME_NAMES sont définies dans le fichier manifest.lua présent dans le dossier associé à un jeu
GAME_NAMES = {}
-- tableau contenant le manifeste associé à chaque jeu présent dans le framework, indexé par NOM DE JEU
-- NOTE: les différents GAME_MANIFESTS sont définies dans le fichier manifest.lua présent dans le dossier associé à un jeu
GAME_MANIFESTS = {}

-- Répertoires utilisés
-- ******************************
-- dossier contenant les fichiers du framework
FOLDER_BASE = "base"
-- dossier contenant les librairies utilisées par le framework
FOLDER_LIB = FOLDER_BASE.."/lib"
-- dossier contenant les fichiers du jeu
FOLDER_GAME = "game"
-- dossier contenant les fichiers pour les écrans du jeu
FOLDER_SCREEN = "screen"
-- dossier contenant les assets et les prototype de jeu
FOLDER_PROTOTYPE = "assets"

--- Initialise les répertoires utilisés par le jeu
-- NOTE: les répertoires sont initialisés dans une fonction car certains dépendent du type de jeu
-- @param pGameIndex index du jeu courant (dans le tableau GAME_NAME).
function defineAssetsFolders (pGameIndex)
  local gameName, gameFolder
  if (pGameIndex == nil) then
    gameName = "DEFAULT"
    gameFolder = "@default"
  else
    gameName = GAME_NAMES[pGameIndex]
    assertEqualQuit(GAME_NAMES[pGameIndex], nil, "defineAssetsFolders:Le jeu ayant pour index "..pGameIndex.." n'est pas défini. Veuillez vérifier les fichiers manifest.lua", true)
    gameFolder = GAME_MANIFESTS[gameName].folder
  end
  assertEqualQuit(gameFolder, nil, "defineAssetsFolders:Le dossier contenant les assets associés au jeu "..gameName.." n'est pas défini. Veuillez vérifier les fichiers manifest.lua", true)
  local folderExists = love.filesystem.isDirectory(gameFolder)
  assertEqualQuit(folderExists, true, "defineAssetsFolders:Le dossier "..gameFolder.." n'a pas été trouvé", true)

  SUB_FOLDER_FONTS = "/fonts"
  SUB_FOLDER_SOUNDS = "/sounds"
  SUB_FOLDER_MUSICS = "/musics"
  SUB_FOLDER_IMAGES = "/images"

  SUB_FOLDER_MAP = "/map"
  SUB_FOLDER_LEVELS = "/levels"
  SUB_FOLDER_TILES = "/tiles"
  SUB_FOLDER_PLAYERS = "/players"
  SUB_FOLDER_NPCS = "/npcs"
  SUB_FOLDER_ITEMS = "/items"
  SUB_FOLDER_PROJECTILES = "/projectiles"
  SUB_FOLDER_EFFECTS = "/effects"
  SUB_FOLDER_UI = "/ui"
  SUB_FOLDER_ANIM = "/anims"

  -- DOSSIERS LIÉS À UN JEU DONNE
  -- les assets 
  FOLDER_ASSETS = FOLDER_PROTOTYPE.."/"..gameFolder
  -- les polices
  FOLDER_FONTS = FOLDER_ASSETS..SUB_FOLDER_FONTS
  -- les sons
  FOLDER_SOUNDS = FOLDER_ASSETS..SUB_FOLDER_SOUNDS
  -- les musiques
  FOLDER_MUSICS = FOLDER_ASSETS..SUB_FOLDER_MUSICS
  -- les images
  FOLDER_IMAGES = FOLDER_ASSETS..SUB_FOLDER_IMAGES
  -- les images du joueur
  FOLDER_PLAYER_IMAGES = FOLDER_IMAGES..SUB_FOLDER_PLAYERS
  -- les animations du joueur
  FOLDER_PLAYER_ANIMS = FOLDER_PLAYER_IMAGES..SUB_FOLDER_ANIM
  -- les images des pnj
  FOLDER_NPCS_IMAGES = FOLDER_IMAGES..SUB_FOLDER_NPCS
  -- les animations des pnj
  FOLDER_NPCS_ANIMS = FOLDER_NPCS_IMAGES..SUB_FOLDER_ANIM
  -- les images des éléments de décors
  FOLDER_ITEMS_IMAGES = FOLDER_IMAGES..SUB_FOLDER_ITEMS
  -- les animations des éléments de décors
  FOLDER_ITEMS_ANIMS = FOLDER_ITEMS_IMAGES..SUB_FOLDER_ANIM
  -- les images des projectiles
  FOLDER_PROJECTILES_IMAGES = FOLDER_IMAGES..SUB_FOLDER_PROJECTILES
  -- les animations des projectiles
  FOLDER_PROJECTILES_ANIMS = FOLDER_PROJECTILES_IMAGES..SUB_FOLDER_ANIM
  -- les images des effects visuels
  FOLDER_EFFECTS_IMAGES = FOLDER_IMAGES..SUB_FOLDER_EFFECTS
  -- les animations des effects visuels
  FOLDER_EFFECTS_ANIMS = FOLDER_EFFECTS_IMAGES..SUB_FOLDER_ANIM
  -- les images des GUI/HUD
  FOLDER_UI_IMAGES = FOLDER_IMAGES..SUB_FOLDER_UI
  -- les animations des GUI/HUD
  FOLDER_UI_ANIMS = FOLDER_UI_IMAGES..SUB_FOLDER_ANIM
  -- les assets et niveaux de la carte
  FOLDER_MAP = FOLDER_ASSETS..SUB_FOLDER_MAP
  -- les fichiers décrivant les niveaux
  FOLDER_MAP_LEVELS = FOLDER_MAP..SUB_FOLDER_LEVELS
  -- les images de la carte
  FOLDER_MAP_IMAGES = FOLDER_MAP..SUB_FOLDER_IMAGES
  -- les tuiles de la carte
  FOLDER_MAP_IMAGES_TILES = FOLDER_MAP_IMAGES..SUB_FOLDER_TILES
end -- defineAssetsFolders

-- AFFICHAGE
-- ******************************

-- Facteur d'échelle pour l'affichage en X
DISPLAY_SCALE_X = 1

-- Facteur d'échelle pour l'affichage en Y
DISPLAY_SCALE_Y  = DISPLAY_SCALE_X

-- ANIMATIONS
-- ******************************
ANIMATION_DEFAULT_SPEED = 1 / 8

-- DIVERS
-- ******************************
-- décalage de coordonnées à appliquer quand le sprite rebondi sur un obstacle ou sur les bords
OFFSET_WHEN_BOUNCE = 5

-- identifiants devant être contenu dans le nom des fichiers lua ne devant pas être chargés automatiquement par la fonction autoLoadRequire()
NO_AUTOLOAD_STRING = "_norequire"

-- pour des tests uniquement
DUMMY_VALUE = "DummyValue"
-- marqueur pour l'entrée de menu sélectionnée.
-- ATTENTION: certains caractères ne sont pas présents dans toutes les fonts, par exemple < et >
MENU_MARKER_LEFT = "= "
MENU_MARKER_RIGHT = " ="

-- touche espace variant selon les versions de love
if (love._version_major < 1 and love._version_minor < 10) then
  KEY_SPACE = " "
else
  KEY_SPACE = "space"
end
