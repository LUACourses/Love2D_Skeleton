-- Regroupe des fonctions spécifiques au jeu
-- ****************************************

--- efface toutes les entités
function resetEntities()
  --[[ NE MARCHE PAS (passage par référence ?)
  table.empty(entities.navPaths)
  table.empty(entities.sounds)
  table.empty(entities.animations)
  table.empty(entities.characters)
  table.empty(entities.sprites)
  table.empty(entities)
  ]]
  local index, value
  for index, value in pairs(entities.navPaths) do
    value = nil
  end
  entities.navPaths = {}
  for index, value in pairs(entities.sounds) do
    value = nil
  end
  entities.sounds = {}
  for index, value in pairs(entities.animations) do
    value = nil
  end
  entities.animations = {}
  for index, value in pairs(entities.characters) do
    value = nil
  end
  entities.characters = {}
  for index, value in pairs(entities.sprites) do
    value = nil
  end
  entities.sprites = {}
end -- resetEntities

--- Calcule une position aléatoire en X et Y, bornée et respectant une distance de sécurité autour d'un point donné. 
-- NOTE: Utile pour les spawning aléatoire évitant les sprites présents
-- @param pMinX Valeur minimale en X.
-- @param pMinY Valeur minimale en Y.
-- @param pMaxX Valeur maximale en X.
-- @param pMaxY Valeur maximale en Y.
-- @param pMinDistance Distance minimal à respecter.
-- @param pObjectsToAvoid Liste des objets devant se trouver à plus de  pMinDistance px. Chaque objet doit avoir un attribut x et y.
-- @param pMaxTries (OPTIONNEL) Nombre maximal d'essais à faire pour trouver la position (permet d'éviter les boucles infines). Si absent, 100 sera utilisée.
-- @return les positions X et Y
function findSecurePosition (pMinX, pMinY, pMaxX, pMaxY, pMinDistance, pObjectsToAvoid, pMaxTries)
  if (pMaxTries == nil) then pMaxTries = 100 end
  -- alias pour accélérer les traitements
  local mathRandom = math.random
  local mathAbs = math.abs

  local newPosX, newPosY
  local i, object
  local distanceX = 0 -- nécessaire pour rentrer dans la boucle au départ
  local distanceY = 0
  local xIsValid = false
  local yIsValid = false

  -- boucle tant que la position du spawn est trop proche de celle d'au moins un objet
  while (not xIsValid or yIsValid) do
    -- nombre de tests à valider égal au nombre d'objet au départ
    local validationsFailed = #pObjectsToAvoid

    -- tire une position X au hasard dans la zone bornée si la position en X est invalide
    if (not xIsValid) then newPosX = mathRandom(pMinX, pMaxX) end
    -- tire une position Y au hasard dans la zone bornée si la position en Y est invalide
    if (not yIsValid) then newPosY = mathRandom(pMinY, pMaxY) end

    -- boucle sur tous les objects à vérifier
    for i = 1, #pObjectsToAvoid do
      object = pObjectsToAvoid[i]

      -- vérification des coordonnées de l'object
      if (object.x == nil or object.y == nil) then return 0, 0 end

      -- distance avec la position choisie
      distanceX = mathAbs(newPosX - object.x)
      distanceY = mathAbs(newPosY - object.y)

      xIsValid = (distanceX >= pMinDistance)
      yIsValid = (distanceY >= pMinDistance)

      -- si la distance minimale est respectée en X et Y, le test est validé
      if (xIsValid and yIsValid) then
        validationsFailed = validationsFailed - 1
      else
        -- sinon, on sort de la boucle for pour faire un autre tirage de position
        break
      end
    end -- for
    newPosX = math.floor(newPosX)
    newPosY = math.floor(newPosY)
    object.x = math.floor(object.x)
    object.y = math.floor(object.y)

    -- si le nombre de validations restantes est nul (tous les tests ont été validés), alors la position est correcte
    if (validationsFailed <= 0) then return newPosX, newPosY end
    --debugMessage("SPAWNING: invalidation position: new="..newPosX..", "..newPosY.." objet="..object.x..","..object.y.." distanceMin="..pMinDistance)
    -- verification de sécurité contre les boucles infines
    pMaxTries = pMaxTries -1
    if (pMaxTries <= 0) then
      return 0, 0
    end
  end -- while

  return newPosX, newPosY
end

--- Stoppe les musiques associées aux écrans de jeu
-- @param pScreenToKeep (OPTIONNEL) Nom de l'écran dont la musique doit rester en lecture.
function stopAllScreenMusics (pScreenToKeep)
  if (pScreenToKeep == nil) then pScreenToKeep = "" end
  local i, screen
  for i = 1, #entities.screens do
    screen = entities.screens[i]
    if (screen.music ~= nil and screen.name ~= pScreenToKeep) then screen.music:stop() end
  end -- for
end -- stopAllScreenMusics

--- affiche les infos de debug dans la console, dont la liste des sprites correspondant a un status donné
-- @param pStatus (OPTIONNEL) status des sprite à lister. Si absent, tous les sprites seront affichés.
function printDebugInfos (pStatus)
  debugMessage("=====================")
  debugMessage("DEBUGINFOS")
  debugMessage(debugInfos)
  debugMessage("=====================")
  debugMessage("LISTE DES SPRITES")
  local count = 0
  for index = 1, #entities.sprites do
    sprite = entities.sprites[index]
    if (pStatus ~=nil and sprite.status == pStatus) then
      debugMessage(sprite.toString())
      count = count +1
    end
  end
  debugMessage("NOMBRE DE SPRITES:", count)
  if (pStatus ~=nil) then
    debugMessage(" avec le status:"..pStatus)
  end
  debugMessage("=====================")
end -- printDebugInfos
