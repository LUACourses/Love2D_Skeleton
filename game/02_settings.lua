-- Entité regroupant les paramètres du jeu
-- ****************************************

--- Crée un pseudo objet de type settings
-- @param pGameIndex (OPTIONNEL) index du jeu courant (dans le tableau GAME_NAME). Si absent, 1 sera utilisée.
-- @return un pseudo objet settings
function newSettings (pGameIndex)

  local lSettings = {}
  --index du jeu courant (dans le tableau GAME_NAME)
  lSettings.gameIndex = pGameIndex
  -- type de jeu courant (constante GAME_MANIFEST.gametype)
  lSettings.gameType = nil
  -- valeur de la friction (ralentissement du à l'environnement)
  lSettings.friction = pFriction
  -- valeur de la gravité
  lSettings.gravity  = pGravity
  -- mapping clavier pour les touches globales à le jeu
  lSettings.appKeys = {}
  -- mapping clavier pour les touches pouvant être modifiées par chaque joueur
  lSettings.playerKeys = {}
  -- paramètres de la souris
  lSettings.mouse = {}

  --- Initialise l'objet
  -- @param pGameIndex (OPTIONNEL) index du jeu courant (dans le tableau GAME_NAME). Si absent, 1 sera utilisée.
  -- @param pSensibilityX (OPTIONNEL) coefficient de sensibilité de la souris en X. Si absent, 1 sera utilisée.
  -- @param pSensibilityX (OPTIONNEL) coefficient de sensibilité de la souris en Y. Si absent, 1 sera utilisée.
  -- @param pFriction (OPTIONNEL) coefficient de friction du mouvement. Si absent, 5 sera utilisée. Mettre à 0 pour ne pas appliquer
  -- @param pGravity (OPTIONNEL) coefficient de gravité. Si absent, 20 sera utilisée. Mettre à 0 pour ne pas appliquer
  function lSettings.initialize (pGameIndex, pSensibilityX, pSensibilityY, pFriction, pGravity)
    debugFunctionEnter("settings.initialize ", pGameIndex, pSensibilityX, pSensibilityY, pFriction, pGravity)
    if (pSensibilityX == nil) then pSensibilityX = 1 end
    if (pSensibilityY == nil) then pSensibilityY = 1 end
    if (pFriction == nil) then pFriction = 5 end --5 pixels par seconde par défaut
    if (pGravity == nil) then pGravity = 20 end --20 pixels par seconde par défaut
    if (pGameIndex == nil) then
      -- à modifier pour choisir le type de jeu
      pGameIndex = nil -- permet de passer par le menu de choix du jeu
      pGameIndex = 1
    end

    -- GAMEPLAY
    -- définit le jeu courant (dans le tableau GAME_NAME).
    lSettings.setGame(pGameIndex)
    lSettings.friction = pFriction
    lSettings.gravity  = pGravity

    lSettings.appKeys = {
      nextPower     = "kp*",
      nextLevel     = "kp+",
      loseLive      = "kp-",
      loseGame      = "end",
      winGame       = "home",
      quitGame      = "escape",
      nextGame      = "f8",
      debugSwitch   = "f9",
      moveWithKeys  = "f10",
      moveWithMouse = "f11",
      grabMouse     = "f12",
      pauseGame     = "p",
      startGame     = "r",
      previousMusic = "f1",
      nextMusic     = "f2",
      menuUp        = "up",
      menuDown      = "down",
      menuLeft      = "left",
      menuRight     = "right",
      menuValid     = KEY_SPACE, -- validation du choix de menu
      menuReturn    = "m", -- retour au menu
      inputValid    = "return", -- validation d'une entrée clavier
      inputChange   = "backspace", -- effacer la dernière entrée clavier
    }

    lSettings.playerKeys = {
      action1      = KEY_SPACE,
      action2      = "lctrl",
      -- touches de déplacement
      moveUp       = "up",
      moveDown     = "down",
      moveLeft     = "left",
      moveRight    = "right",
      -- touches de déplacement alternatives
      moveUpAlt    = "z",
      moveDownAlt  = "s",
      moveLeftAlt  = "q",
      moveRightAlt = "d"
    }

    lSettings.mouse = {
      sensibilityX = pSensibilityX,
      sensibilityY = pSensibilityY
    }
  end

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  -- change le jeu chargé par le framework
  -- @param pGameIndex index du jeu courant (dans le tableau GAME_NAME). Cet index correspond à l'ordre alphabétique des dossiers des jeux dans le dossier assets.
  function lSettings.setGame (pGameIndex)
    debugFunctionEnter("settings.setGame ", pGameIndex)
    if (pGameIndex == nil) then
      lSettings.gameType = GAME_TYPE_DEFAULT
      lSettings.gameIndex = nil
    else
      assertEqualQuit(GAME_NAMES[pGameIndex], nil, "settings.setGame:Le jeu ayant pour index "..pGameIndex.." n'est pas défini. Veuillez vérifier les fichiers manifest.lua", true)
      lSettings.gameIndex = pGameIndex
      local gameName = GAME_NAMES[pGameIndex]
      lSettings.gameType = GAME_MANIFESTS[gameName].gametype
    end
  end

  --- Détruit l'objet
  function lSettings.destroy ()
    debugFunctionEnter("settings.destroy")
    lSettings.clean()
    lSettings = nil
  end --settings.destroy

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function lSettings.clean ()
    debugFunctionEnter("settings.clean")
    -- pour le moment rien à faire
  end --settings.clean

  -- initialisation par défaut
  lSettings.initialize(pGameIndex)

  return lSettings
end --newSettings
