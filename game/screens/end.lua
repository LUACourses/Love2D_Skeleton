-- Entité représentant l'écran de fin
-- Utilise une forme d'héritage simple: screenEnd qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_END = "screenEnd"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @param pHasWon (OPTIONNEL) true si la partie est gagnée. Si absent, player.hasWon ou FALSE sera utilisée.
-- @return un pseudo objet screenEnd
function createScreenEnd (pName, pBgImage, pMusic, pHasWon)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenEnd", pName, pBgImage, pMusic, pHasWon)

  if (pHasWon == nil) then
    if (player == nil) then pHasWon = false else pHasWon = player.hasWon end
  end

  local lScreen = createScreen(SCREEN_END, pBgImage, pMusic)
  
  -- true si la partie est gagnée
  lScreen.hasWon = pHasWon

  -- Override des fonctions de l'écran
  -- ******************************
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize = lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenEnd.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)

    -- liste des entrées de menu figurant sur l'écran
    -- rappel: createMenu (pEntries, pEntity, pUseTweening)
    -- rappel: createTweening (pEntity, pValue, pDistance, pDuration, pFunctionToCall)
    -- rappel: menu.addEntry (pEntry, pTweeningParamShow, pTweeningParamHide)
    local entry, content, w, h, font, halfX, halfY
    halfX = viewport.getWidth() / 2
    halfY = viewport.getHeight() / 2
    font = love.graphics.getFont()
    local duration = 1
    local fontSizeFactor = 2
    font = HUD.get_fontMenuContent()

    lScreen.menu = createMenu(nil, lScreen, true) -- true pour utiliser le tweening
    h = font:getHeight(" ") -- alt+255 et non espace

    content = 'Jouer'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = screenPlay.show,
      tweeningShow = createTweening(nil, - w, halfX, 1, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, 1, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    content = 'Scores'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = screenStats.show,
      tweeningShow = createTweening(nil, - w, halfX, 1, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, 1, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    content = 'Retour au menu'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = screenStart.show,
      tweeningShow = createTweening(nil, - w, halfX, 1, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, 1, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    content = 'Quitter'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = quitGame,
      tweeningShow = createTweening(nil, - w, halfX, 1, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, 1, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    --lScreen.isInitialized = true important: ne pas utiliser cette ligne permet de réinitialiser l'animation du menu à chaque affichage
  end -- screen.initialize

  --[[ à décommenter pour des comportements spécifiques
  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenEnd.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update
  ]]

  --- affiche l'écran
  -- @param pHasWon (OPTIONNEL) true si la partie est gagnée.
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show (pHasWon)
    debugFunctionEnter("screenEnd.show")
    lScreen.screenShow()
    if (pHasWon == nil) then pHasWon = false end
    lScreen.hasWon = pHasWon
  end -- screen.show

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenEnd.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()

    local w, h, font, i
    local color, content, score
    local colorWin = {0, 200, 0, 255} -- vert sans transparence
    local colorLose = {200, 0, 0, 255} -- rouge sans transparence

    font = HUD.get_fontMenuContent()
    w = font:getWidth(" ") -- alt+255 et non espace
    h = font:getHeight(" ") -- alt+255 et non espace
    local offsetX = 0
    local offsetY = -100

    if (lScreen.hasWon) then
      content = "Bravo,partie gagnee !"
      color = colorWin
    else
      content = "GAME OVER :o("
      color = colorLose
    end

    font = HUD.get_fontMenuTitle()
    textWidth = font:getWidth(content)
    -- rappel: lHUD.animText (pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    HUD.animText(content, font, TEXT_ANIM_SCROLL_HORZ, offsetX, offsetY, nil, nil, {color}, 1, 200, - textWidth, nil)

    offsetY = 100
    font = HUD.get_fontMenuContent()
    w = font:getWidth(" ") -- alt+255 et non espace
    h = font:getHeight(" ") -- alt+255 et non espace
    if (player ~= nil) then score = math.floor(player.score) else score = 0 end
    HUD.displayText("Votre score est de "..score, font, POS_CENTER, offsetX, offsetY)

    if (gameStats.isHighscore(score)) then
      local width = viewport.getWidth()
      offsetY = offsetY + h * 2
      HUD.displayText('Vous avez un HIGH SCORE!', font, POS_CENTER, offsetX, offsetY, colorWin)
      offsetY = offsetY + h
      HUD.displayText('Entrer votre nom:', font, POS_CENTER, offsetX, offsetY, {255, 255, 255, 255})
      offsetY = offsetY + h
      HUD.displayText(player.nickName, font, POS_CENTER, offsetX, offsetY, {255, 255, 0, 255}) -- jaune sans transparence
      offsetY = offsetY + h
      HUD.displayText('Appuyer sur "Entrer" pour valider', font, POS_CENTER, offsetX, offsetY, {255, 255, 255, 255} )
    end

    offsetY = offsetY + h * 2
    
    -- affiche le menu de l'écran (s'il est défini)
    offsetX, offsetY = lScreen.menu.draw(offsetX, offsetY, font, color)

    love.graphics.setColor(255, 255, 255, 255)
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenStart.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    local score
    if (player ~= nil) then score = math.floor(player.score) else score = 0 end
    if (gameStats.isHighscore(score)) then
      if (pKey == settings.appKeys.inputChange) then
        if (#player.nickName > 1) then
          player.nickName = player.nickName:sub(1, #player.nickName - 1)
        else
          player.nickName = ""
        end
      elseif (pKey == settings.appKeys.inputValid and #player.nickName > 0) then
        -- enregistre le score avec le nickname
        gameStats.addScore(player.score, player.nickName)
        gameStats.saveScore()
        -- retour à l'écran de départ
        screenStart.show()
        return
      end
    end --  if (gameStats.isHighscore(player.score)) then

    lScreen.menu.navigate(pKey)
  end -- screen.keypressed

  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenEnd.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenEnd.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  --- Gestion de la saisie de texte sur l'écran
  -- @param pText texte
  function lScreen.textinput (pText)
    if (player ~= nil) then
      if (#player.nickName < 20) then
        player.nickName = player.nickName..pText
      end
    end
  end -- screen.textinput

  return lScreen
end -- createScreenEnd