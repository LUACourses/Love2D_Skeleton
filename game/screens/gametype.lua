-- Entité représentant l'écran de choix du jeu
-- Utilise une forme d'héritage simple: screenGametype qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_GAMETYPE = "screenGametype"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screenGametype
function createScreenGametype (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenGametype", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_GAMETYPE, pBgImage, pMusic)

  -- Override des fonctions de l'écran
  -- ******************************
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize = lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenGametype.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)

    -- liste des entrées de menu figurant sur l'écran
    -- rappel: createMenu (pEntries, pEntity, pUseTweening)
    -- rappel: createTweening (pEntity, pValue, pDistance, pDuration, pFunctionToCall)
    -- rappel: menu.addEntry (pEntry, pTweeningParamShow, pTweeningParamHide)
    local i

    lScreen.menu = createMenu(nil, lScreen, false) -- true pour utiliser le tweening

    for i = 1, #GAME_NAMES do
      entry = {
        text = GAME_NAMES[i],
        actionParams = i,
        action = lScreen.selectGameType,
      }
      lScreen.menu.addEntry(entry)
    end --for
    --lScreen.isInitialized = true important: ne pas utiliser cette ligne permet de réinitialiser l'animation du menu à chaque affichage
  end -- screen.initialize

  --[[ à décommenter pour des comportements spécifiques
  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenGametype.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update

  --- affiche l'écran
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show (pHasWon)
    debugFunctionEnter("screenGametype.show")
    lScreen.screenShow()
  end -- screen.show
  ]]

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenGametype.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()

    local w, h, font, i
    local color, content
    font = HUD.get_fontMenuContent()
    w = font:getWidth(" ") -- alt+255 et non espace
    h = font:getHeight(" ") -- alt+255 et non espace
    local offsetX = 0
    local offsetY = -h * 10

    color = {255, 160, 0, 255} -- vert sans transparence
    font = HUD.get_fontMenuTitle()
    -- rappel: lHUD.animText (pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    HUD.animText(love.window.getTitle(), font, TEXT_ANIM_SINUS_CENTER, 0, offsetY, nil, nil, {color}, 1, 20)

    -- affiche le texte d'aide de l'écran (s'il est défini)
    offsetY = 0
    font = HUD.get_fontMenuContent()
    color = {255, 255, 255, 255} -- blanc sans transparence
    offsetX, offsetY = lScreen.drawHelp(game.helpText, offsetX, offsetY, font, color)

    -- affiche le menu de l'écran (s'il est défini)
    offsetY = offsetY + h * 2
    color = {255, 255, 0, 255} -- jaune sans transparence
    offsetX, offsetY = lScreen.menu.draw(offsetX, offsetY, font, color)


    love.graphics.setColor(255, 255, 255, 255)
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenGametype.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    lScreen.menu.navigate(pKey)
  end -- screen.keypressed


  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenGametype.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenGametype.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  function lScreen.selectGameType (pGameIndex)
    initializeGame(pGameIndex)
  end
  return lScreen
end -- createScreenGametype