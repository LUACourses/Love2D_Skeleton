-- Entité représentant l'écran de Démarrage
-- Utilise une forme d'héritage simple: screenStart qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_START = "screenStart"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screenStart
function createScreenStart (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenStart", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_START, pBgImage, pMusic)

  -- Override des fonctions de l'écran
  -- ******************************
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize = lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenStart.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)

    -- liste des entrées de menu figurant sur l'écran
    -- rappel: createMenu (pEntries, pEntity, pUseTweening)
    -- rappel: createTweening (pEntity, pValue, pDistance, pDuration, pFunctionToCall)
    -- rappel: menu.addEntry (pEntry, pTweeningParamShow, pTweeningParamHide)
    local entry, content, w, h, font, halfX, halfY
    halfX = viewport.getWidth() / 2
    halfY = viewport.getHeight() / 2
    font = love.graphics.getFont()
    local duration = 1
    local fontSizeFactor = 2
    font = HUD.get_fontMenuContent()

    lScreen.menu = createMenu(nil, lScreen, true) -- true pour utiliser le tweening
    h = font:getHeight(" ") -- alt+255 et non espace

    content = 'Jouer'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = screenPlay.show,
      tweeningShow = createTweening(nil, - w, halfX, duration, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, duration, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    content = 'Scores'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = screenStats.show,
      tweeningShow = createTweening(nil, - w, halfX, duration, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, duration, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    content = 'Quitter'
    w = font:getWidth(content) / fontSizeFactor
    entry = {
      text = content,
      action = quitGame,
      tweeningShow = createTweening(nil, - w, halfX, duration, "easeOutSine"),
      tweeningHide = createTweening(nil, halfX - w,  halfX, duration, "easeInExpo")
    }
    lScreen.menu.addEntry(entry)

    --lScreen.isInitialized = true important: ne pas utiliser cette ligne permet de réinitialiser l'animation du menu à chaque affichage
  end -- screen.initialize

  --[[ à décommenter pour des comportements spécifiques
  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenStart.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update

  --- affiche l'écran
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show (pHasWon)
    debugFunctionEnter("screenStart.show")
    lScreen.screenShow()
  end -- screen.show
  ]]

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenStart.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()

    local w, h, font, i
    local color, content
    font = HUD.get_fontMenuContent()
    w = font:getWidth(" ") -- alt+255 et non espace
    h = font:getHeight(" ") -- alt+255 et non espace
    local offsetX = 0
    local offsetY = -h * 10

    color = {255, 160, 0, 255} -- vert sans transparence
    font = HUD.get_fontMenuTitle()
    -- rappel: lHUD.animText (pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    HUD.animText(game.title, font, TEXT_ANIM_SINUS_CENTER, 0, offsetY, nil, nil, {color}, 1, 20)

    offsetY = h * 2

    -- affiche le menu de l'écran (s'il est défini)
    color = {255, 255, 0, 255} -- jaune sans transparence
    font = HUD.get_fontMenuContent()
    offsetX, offsetY = lScreen.menu.draw(offsetX, offsetY, font, color)

    -- affiche le texte d'aide de l'écran (s'il est défini)
    color = {255, 255, 255, 255} -- blanc sans transparence
    offsetX, offsetY = lScreen.drawHelp(game.helpText, offsetX, offsetY, font, color)

    love.graphics.setColor(255, 255, 255, 255)
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenStart.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    lScreen.menu.navigate(pKey)
  end -- screen.keypressed


  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenStart.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenStart.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  return lScreen
end -- createScreenStart