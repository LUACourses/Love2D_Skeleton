-- Entité représentant l'écran de stats (scores)
-- Utilise une forme d'héritage simple: screenStats qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_STATS = "screenStats"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screenStats
function createScreenStats (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenStats", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_STATS, pBgImage, pMusic)

  -- Override des fonctions de l'écran
  -- ******************************
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize = lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenStats.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)
    gameStats.readEntries()
    --lScreen.isInitialized = true important: ne pas utiliser cette ligne permet de réinitialiser l'animation du menu à chaque affichage
  end -- screen.initialize

  --[[ à décommenter pour des comportements spécifiques
  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenStats.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update

  --- affiche l'écran
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show ()
    debugFunctionEnter("screenStats.show")
    lScreen.screenShow()
  end -- screen.show
  ]]

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenStats.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()

    local w, h, font, i
    local color, content
    font = HUD.get_fontMenuContent()
    w = font:getWidth(" ") -- alt+255 et non espace
    h = font:getHeight(" ") -- alt+255 et non espace
    local offsetX = 0
    local offsetY = 50

    local highScores = gameStats.getScoreEntries()
    local width = viewport.getWidth()
    offsetX = width / 3

    love.graphics.setFont(HUD.get_fontMenuTitle())
    love.graphics.printf('MEILLEURS SCORES', 0, offsetY, width, 'center')
    offsetY = offsetY + h * 4

    love.graphics.setFont(HUD.get_fontMenuContent())
    if (#highScores > 0) then
      love.graphics.printf('SCORE', 0, offsetY, offsetX, 'right')
      love.graphics.printf('NOM', offsetX - 15, offsetY, offsetX - 20, 'center')
      love.graphics.printf('DATE', offsetX * 2, offsetY, offsetX, 'left')

      offsetY = offsetY + h * 2

      for i = 1, #highScores do
        love.graphics.printf(highScores[i].score, 0, offsetY, offsetX, 'right')
        love.graphics.printf(highScores[i].nickname, offsetX - 15, offsetY, offsetX - 20, 'center')
        love.graphics.printf(highScores[i].date, offsetX * 2, offsetY, offsetX , 'left')
        offsetY = offsetY + h
      end -- for
    else
      offsetY = 0
      content = 'Le tableau des scores est vide pour le moment'
      HUD.displayText(content, font, POS_CENTER, 0, offsetY, color)
    end -- if (#highScores > 0) then

    color = {255, 255, 0, 255}-- jaune
    content = 'Appuyer sur "'..settings.appKeys.menuReturn..'" pour revenir au menu'
    HUD.displayText(content, font, POS_CENTER, 0, offsetY + h * 2, color)
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenStats.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    if (pKey == settings.appKeys.menuReturn) then screenStart.show() end
  end -- screen.keypressed

  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenStats.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param voir fonction love2D
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenStats.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  return lScreen
end -- createScreenStats