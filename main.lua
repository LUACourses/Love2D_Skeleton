-- Cette ligne permet d'afficher des traces dans la console pendant l'éxécution
io.stdout:setvbuf("no")

-- Filtre pour adoucir les contours des images quand elles sont redimentionnées
-- NOTE : à désactiver pour du pixel art ou du style old school ou des facteur d'échelle important
love.graphics.setDefaultFilter("nearest")

-- Cette ligne permet de déboguer pas à pas dans ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end
-- You can turn the debugging off (require("mobdebug").off()) right after starting debugging (using require("mobdebug").start()) and then turn it on and off around the section you are interested in:
-- require("mobdebug").off() -- <-- turn the debugger off car TROP lent sinon WTF ?

-- variables diverses
-- ******************************
-- VARIABLES POUR DEBUG UNIQUEMENT
-- texte de debug affiché à l'écran par le HUD
debugInfos = ""
-- variable globale commune à tous les modules utilisé pour du debug
debugVar = ""
-- permet de définir des éléments de code spécifiques à activer ou pas en live avec la touche settings.appKeys.debugSwitch
debugFlagEnabled = false
-- permet de visualiser la dernière collision pendant une durée donnée
debugLastcollision = {
  s1 = {0, 0, 0, 0},
  s2 = {0, 0, 0, 0},
  timer = 0
}

-- Différents états de powerUp pour le joueur, utile pour le debug
debugPowerUpList = {}

-- index de l'état courant de powerUp pour le joueur
debugPowerUpIndex = 1

-- liste des toutes les listes d'entités présentes dans le jeu
-- NOTE: si une liste est ajoutée au tableau, il faut également l'ajouter dans la fonction resetEntities()
entities = {
  -- liste des sprites présents dans le jeu
  sprites = {},
  -- liste des joueurs (mode multi-joueur)
  -- NOTE: figure aussi dans la liste des sprites mais avec un index différent
  characters = {},
  -- liste des NPCs
  -- NOTE: figure aussi dans la liste des sprites mais avec un index différent
  NPCs = {},
  -- liste des animations présentes dans le jeu
  animations = {},
  -- liste des sons présents dans le jeu
  sounds = {},
  -- liste des trajectoires présentes dans le jeu
  navPaths = {},
  -- liste des écrans présents dans le jeu
  screens = {},
  -- liste des tweening présents dans le jeu
  tweenings = {}
}

--- Love.load pour initialiser l'application
function love.load ()
  -- Initialisation du random avec un temps en ms
  math.randomseed(love.timer.getTime()); math.random(); math.random(); math.random()

  -- La répétition de l'événement love.keypressed est activés lorsqu'une touche est maintenue enfoncée.
  love.keyboard.setKeyRepeat(true)

  -- Active le mode relatif: le curseur est caché et ne se déplace pas lorsque la souris est activée,
  -- mais les événements relatifs du mouvement de la souris sont toujours générés via love.mousemoved
  love.mouse.setRelativeMode(true)

  -- charge les requires nécessaires pour le lancement uniquement.
  -- note: l'ordre est important, tous les autres modules seront chargé par autoload
  require("constants")
  require(FOLDER_LIB.."/autoload")
  require(FOLDER_GAME.."/02_settings")
  require(FOLDER_GAME.."/03_appState")

  -- charge les librairies de base
  autoLoadRequire(FOLDER_LIB, false)

  -- charge les manifestes pour tous les jeux existants dans le dossier assets
  autoLoadRequire(FOLDER_PROTOTYPE, false, "manifest")

  initializeGame()
end -- love.load

--- Love.update est utilisé pour gérer l'état du jeu frame-to-frame
-- @param pDt delta time
function love.update (pDt)
  HUD.update(pDt)

  appState.currentScreen.update(pDt)

  musicManager.update(pDt)
end -- love.update

--- Love.draw est utilisé pour afficher l'état du jeu sur l'écran.
function love.draw ()
  -- si ce n'a pas déjà été fait,on dessine le viewport
  if (not viewport.firstDrawDone) then viewport.draw() end

  appState.currentScreen.draw()
end -- love.draw

--- intercepte l'appui des touches du clavier
function love.keypressed (pKey, pScancode, pIsrepeat)
  -- debugMessage("touche clavier appuyée:", pKey, pScancode, pIsrepeat)

  -- ÉVÈNEMENTS INDÉPENDANTS DE L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************
  -- Quitter l'application
  if (pKey == settings.appKeys.quitGame) then quitGame();return end

  -- Capturer ou non la souris dans la fenêtre
  if (pKey == settings.appKeys.grabMouse) then
    local state = not love.mouse.isGrabbed()
    love.mouse.setGrabbed(state)
    debugMessage("mouse.setGrabbed:", state)
  end

  -- Afficher les infos de debug dans la console
  if (pKey == settings.appKeys.debugSwitch) then
    debugMessage ("INFO PLAYER:", player.toString())
    -- permet de déclencher des évènement de debug en live
    debugFlagEnabled = not debugFlagEnabled
    if (debugFlagEnabled) then
      -- debug.debug()
      --[[NOTE:
      Enters an interactive mode with the user, running each string that the user enters.
      Using simple commands and other debug facilities, the user can inspect global and local variables, change their values, evaluate expressions, and so on.
      A line containing only the word cont finishes this function, so that the caller continues its execution.
      Note that commands for debug.debug are not lexically nested within any function, and so have no direct access to local variables.
      ]]
      require("mobdebug").on() -- <-- turn the debugger on
    else
      require("mobdebug").off() -- <-- turn the debugger off
    end
    printDebugInfos(SPRITE_STATUS_SLEEPING)
  end

  -- on passe au jeu suivant
  if (pKey == settings.appKeys.nextGame) then
    local gameIndex = settings.gameIndex + 1
    if (gameIndex > #GAME_NAMES) then gameIndex = 1 end
    HUD.autoHideMessage.color = {255, 128, 128, 255}
    HUD.autoHideMessage.text = "PASSAGE A"..GAME_NAMES[gameIndex]
    HUD.autoHideMessage.timer = 0
    initializeGame(gameIndex)
  end

  -- Jouer la piste musicale suivante
  if (pKey == settings.appKeys.nextMusic) then musicManager.playNext();return end
  -- Jouer la piste musicale précédente
  if (pKey == settings.appKeys.previousMusic) then musicManager.playPrevious();return end

  -- ÉVÈNEMENTS LIÉS À L'ÉCRAN EN COURS D'AFFICHAGE
  -- ******************************
  if (appState.currentScreen.keypressed ~= nil) then appState.currentScreen.keypressed(pKey, pScancode, pIsrepeat)  end
end -- love.keypressed

--- intercepte le relâchement des touches du clavier
function love.keyreleased (pKey)
  -- si une touche figure dans la liste des touche à relacher, on mémorise que cette touche a été relâchée
  if (player ~= nil and player.get_keyToRelease(pKey) ~= nil ) then player.set_keyToRelease(pKey, false) end
end -- love.keyreleased

--- intercepte le mouvement de la souris
function love.mousemoved (pX, pY, pDX, pDY, pIstouch)
  -- debugMessage("déplacement de la souris:", pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
  if (appState.currentScreen.mousemoved ~= nil) then appState.currentScreen.mousemoved(pX, pY, pDX, pDY, pIstouch)  end
end -- love.mousemoved

--- intercepte le clic de souris
function love.mousepressed (pX, pY, pButton, pIstouch)
  -- debugMessage("boutons de la souris appuyé:", pX, pY, pButton, pIstouch)
  if (appState.currentScreen.mousepressed ~= nil) then appState.currentScreen.mousepressed(pX, pY, pButton, pIstouch) end
end -- love.mousepressed

--- intercepte  la saisie de texte sur l'écran
-- @param pText texte
function love.textinput (pText)
  -- debugMessage("texte saisie:",pText)
  if (appState.currentScreen.textinput ~= nil) then appState.currentScreen.textinput(pText) end
end -- love.textinput

--- Initialiser le jeu
-- @param pGameIndex (OPTIONNEL) index du jeu courant (dans le tableau GAME_NAME). Si absent, 1 sera utilisée.
function initializeGame (pGameIndex)

  -- creation des entités
  -- note: l'ordre est important
  -- ******************************
  -- Entité représentant les paramètres du jeu
  settings = newSettings(pGameIndex)

  -- Entité représentant l'état du jeu à un instant donné
  appState = newAppState()

  -- Actualise les dossiers des assets en fonction du type de jeu
  defineAssetsFolders(settings.gameIndex)

  -- Charge automatiquement tous les fichiers lua contenu dans les dossiers et sous-dossiers
  -- identiques à tous les prototypes
  autoLoadRequire(FOLDER_BASE)
  autoLoadRequire(FOLDER_GAME)
  -- spécifiques au prototype défini dans settings.lua
  autoLoadRequire(FOLDER_ASSETS)
  game = createGame()

  debugPowerUpList = {SPRITE_STATUS_INVISIBLE, SPRITE_STATUS_INVINCIBLE, SPRITE_STATUS_DEADLY, SPRITE_STATUS_FREEZE, SPRITE_STATUS_NORMAL}

  -- Entité représentant le gestionnaire de ressources
  resourceManager = createResourceManager()
  assertEqualQuit(resourceManager, nil, "main:resourceManager", true)

  -- Entité représentant les stats de jeu (scores)
  gameStats = createGameStats(game.name.."_stats.txt")

  -- Entité représentant la zone de jeu
  viewport = createViewPort()

  -- utile en cas de switch de jeu en live
  if (musicManager ~= nil) then musicManager.stop() end

  -- Entité représentant le music manager
  musicManager = createMusicManager()

   -- Entité représentant l'affichage
  HUD = createHUD()

  -- Entité représentant la camera (juste pour le scrolling)
  camera = createCamera()

  -- Entité représentant les écrans
  -- NOTE: les images de fond et les musique associées à chaque écran sont chargées automatiquement si elles existent et portent le même nom que celui de l'écran.
  screenPlay = createScreenPlay()
  screenPause = createScreenPause()
  screenStats = createScreenStats()
  screenStart = createScreenStart() -- important: à mettre après screenPlay et screenStats car il les référence.
  screenGametype = createScreenGametype()  -- important: à mettre après screenStart car il le référence.
  screenEnd = createScreenEnd()

  -- utile en cas de switch de jeu en live
  stopAllScreenMusics()

  -- musique gérées par le musicManager
  musicManager.loadDefaultMusics()

  -- charge les sons du jeu (non liés aux sprites)
  sndNextLevel = resourceManager.getSound(FOLDER_SOUNDS.."/nextLevel.mp3","static")
  sndMenuChange = resourceManager.getSound(FOLDER_SOUNDS.."/menuChange.mp3","static")
  sndMenuValid = resourceManager.getSound(FOLDER_SOUNDS.."/menuValid.mp3","static")

  -- initialisation du message avec timer
  HUD.autoHideMessage.color = {255, 128, 0, 255}
  HUD.autoHideMessage.font = HUD.get_fontMenuTitle()

  -- positionne la fenêtre sur l'écran défini pour le debug
  if (DEBUG_MODE > 0) then love.window.setPosition(DEBUG_WINDOWS_X, DEBUG_WINDOWS_Y, DEBUG_DISPLAY_NUM) end

  -- lancement de l'écran de jeu
  if (DEBUG_START_WITH_PLAYSCREEN) then
    screenPlay.show(true)
  else
    if (settings.gameIndex ~= nil) then screenStart.show() else screenGametype.show() end
    --screenEnd.show() -- debug uniquement
  end
end -- initializeGame

--- le joueur passe au niveau suivant
-- @param pNewLevel (OPTIONNEL) valeur du nouveau niveau. Si absent, le niveau courant sera incrémenté de 1
function nextLevel (pNewLevel)
  debugFunctionEnter("nextLevel ",pNewLevel)

  if (pNewLevel == nil) then
    logMessage("Niveau suivant")
    player.addLevel(1)
  else
    logMessage("Niveau atteint:", pNewLevel)
    player.level = pNewLevel
  end

  if (sndNextLevel ~= nil) then sndNextLevel:play() end

  if (player.level > game.levelToWin) then
    -- l'application est terminée lorsque le niveau max est atteint
    screenEnd.show(true)
  else
    -- mémorise certains attributs du joueur car il va être réinitialisé
    appState.backupPlayer(appState.currentPlayerId)
    -- rappel: function screenPlay.show (pMustReset, pMustRestorePlayer)
    screenPlay.show(true, true)
  end
end -- nextLevel

--- Quitter l'application
function quitGame ()
  debugFunctionEnter("quitGame")
  resetEntities()
  if (player ~= nil) then player.clean() end
  musicManager.clean()
  HUD.clean()
  appState.clean()
  viewport.clean()
  love.event.quit()
end -- quitGame