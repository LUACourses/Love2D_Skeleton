## Love2D_Skeleton

### Description
Un framework qui peut servir de base pour des jeux LUA Love2D.  
Il contient 2 prototypes de jeu : un side plateformer et un topdown shooter. D'autres seront ajoutés régulièrement avec de nouvelles fonctionnalités.  

​Aucune documentation n'est fournie pour l'instant.  
Toutefois, pour faciliter la prise en main du framework, le code est clair et ​très largement  commenté (en français uniquement pour le moment).  

===
### Objectifs
Ils dépendent du prototype de jeu choisi dans les réglages: survivre, tuer tous les ennemis et/ou le boss final, collecter tous les objets du niveau...

### Mouvements et actions du joueur
Déplacer le joueur: mouvements de la souris.  
Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.  
Action (tirer): clic gauche.  
Mettre en pause: touche P.  
Relancer la partie: touche R.  
Musique précédente: touche F1.  
Musique suivante: touche F2.  
Quitter le jeu: clic sur la croix ou touche escape.  

#### en mode debug uniquement 
Basculer sur un autre prototype de jeu et relancer le jeu: touche F8.  
Activer le mode Debug en live (ON/OFF): touche F9.  
Activer les déplacements avec le clavier (ON/OFF): touche F10.  
Activer les déplacements avec la souris (ON/OFF): touche F11.  
Confiner la souris dans la fenêtre (ON/OFF): touche F12.  
Passer au niveau suivant: touche + (pavé numérique).  
Perdre une vie: touche - (pavé numérique).  
Utiliser le powerUp suivant: touche * (pavé numérique). 
Perdre la partie: touche Fin.  
Gagner la partie: touche Debut.  

### Interactions
Elles dépendent du prototype de jeu choisi dans les réglages: collisions avec les pnj, les projectiles et bords de la zone de jeu, collecte d'objets....  

### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
A homemade framework that can be used as a base for LUA and Love2D games.  

It contains 2 game prototypes: a side plateformer and a topdown shooter. Others will be added regularly with new features.  

No documentation is provided at this time.  
However, to facilitate the handling of the framework, the code is clear and widely commented (French only for the moment).  

===
### Goals
They depend on the game prototype chosen in the settings: survive, kill all enemies and/or the final boss, collect all the objects of the level ...

### Movements and actions of the player
Move the player: moves the mouse.  
Move the player: ZQSD keys or the arrows on the keyboard.  
Action (fire): left click
Pause: P key.  
Restart the game: R key.  
Previous music: F1 key.  
Next music: F2 key.  
Exit the game: click on the cross or escape key.  

### debug mode only
Switch to another game prototype et restart the game: F8 key.  
Activate the Live Debug mode (ON/OFF): F9 key.  
Enable the movements with the keyboard (ON/OFF): F10 key.  
Enable the movements with the mouse (ON/OFF): F11 key.  
Confining the mouse in the window (ON/OFF): F12 key.  
Go to the next Level: + key (keypad).  
Lose a life: - key (keypad).  
Use the next powerUp: * key (keypad).  
Lose the game: End key.  
Win the game: Home key.  

### Interactions
They depend on the game prototype chosen in the settings: collisions with pnjs, projectiles and edges of the game area, collect objects ....

### Copyrights
Written in LUA and with the Love2D Framework.  
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)